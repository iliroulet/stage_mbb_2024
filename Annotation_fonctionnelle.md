# Sommaire


- [GFAP](#gfap)

- [EggNOG](#eggnog)
    
- [InterProScan](#interproscan)
    
- [FunAnnotate](#funannotate)
    
- [GOMAP](#gomap)


# Annotation fonctionnelle


Après avoir obtenu nos annotations de gènes et les protéines qui y correspondent, nous pouvons ensuite rechercher ces protéines dans plusieurs bases de données afin de vérifier si nos gènes ont bien l'air fonctionnel et trouver leur fonction.

Les résultats de chaque méthode annotation fonctionnelle pour chaque annotation structurelle se trouvent dans le [document principal](README.md).


## GFAP


##### Documentation
https://github.com/simon19891216/GFAP

https://github.com/simon19891216/GFAP/blob/main/Manual.pdf


##### Commandes


GFAP permet d'obtenir une annotation fonctionnelle très rudimentaire mais ultra-rapide, en proposant 3 bases de données différentes : GO (Gene Ontology Resource : https://geneontology.org/), KEGG (Kyoto Encyclopedia of Genes and Genomes : https://www.genome.jp/kegg/) et Pfam (http://pfam.xfam.org/).

```python
python3 GFAP-linux.py -qp /Results/structural_annotation/augustus_ab_initio/augustus_output_regular.aa \
	-aws Cynara_cardunculus -go -am sensitive -o /Results/functional_annotation/augustus_ab_initio/GFAP/GO
```

```python
python3 GFAP-linux.py -qp /Results/structural_annotation/augustus_ab_initio/augustus_output_regular.aa \
	-aws Cynara_cardunculus -kegg -am sensitive -o /Results/functional_annotation/augustus_ab_initio/GFAP/GO
```

```python
python3 GFAP-linux.py -qp /Results/structural_annotation/augustus_ab_initio/augustus_output_regular.aa \
	-aws Cynara_cardunculus -pfam -am sensitive -o /Results/functional_annotation/augustus_ab_initio/GFAP/GO
```

GFAP permet de produire un histogramme ou une heatmap (accompagnés d'un réseau) du nombre de gènes trouvés par catégories de fonction (fonction moléculaire, composant cellulaire ou processus biologique) pour l'annotation fonctionnelle par GO.

```python
python3 GFAP-linux.py -dn -ds -ar /Results/functional_annotation/augustus_ab_initio/GFAP/GO/GFAP-Cynara_cardunculus-GO_annotate.txt \
	-drawtypes bar_chart -colormodel y2r -st pdf -go -o /Results/functional_annotation/augustus_ab_initio/GFAP/GO/bar_chart \
	-aws Cynara_cardunculus
```

```python
python3 GFAP-linux.py -dn -ds -ar /Results/functional_annotation/augustus_ab_initio/GFAP/GO/GFAP-Cynara_cardunculus-GO_annotate.txt \
	-drawtypes heatmap -colormodel y2r -st pdf -go -o /Results/functional_annotation/augustus_ab_initio/GFAP/GO/heatmap \
	-aws Cynara_cardunculus
```

##### Fichiers obtenus


- detail_information.txt : informations plus détaillées sur l'annotation
- GFAP-Cynara_cardunculus-GO_annotate.txt : annotation fonctionnelle (gène, ID, fonction, catégorie de fonction)
- GFAP-Cynara_cardunculus-GO_annotate.txt_pvalue.txt : annotation avec la p-value pour chaque gène

<!-- #region -->
Dans le dossier de l'annotation par GO, on retrouve en plus deux autres dossiers :

- Le dossier bar_chart, contenant les plots de l'histogramme :
    - detail_information.txt : détails des annotations utilisées pour les graphes
    - draw_detail.svg : graphe de points représentant la quantité de gènes trouvés selon leur fonction
    - draw_detail.svg.pdf : histogramme du nombre de gènes selon leur fonction et leur catégorie de fonction
    - draw_network_detail.svg : réseau reprenant le graphe de points précédent


- Le dossier heatmap, contenant les plots de la heatmap :
    - detail_information.txt : détails des annotations utilisées pour les graphes
    - draw_detail.svg : graphe de points représentant la quantité de gènes trouvés selon leur fonction
    - draw_detail.svg.pdf : heatmap du nombre de gènes selon leur fonction et leur catégorie de fonction
    - draw_network_detail.svg : réseau reprenant le graphe de points précédent
<!-- #endregion -->

## EggNOG


##### Documentation

https://github.com/eggnogdb/eggnog-mapper


##### Commandes

```python
emapper.py  --sensmode more-sensitive --cpu 15 \
	-i /Results/structural_annotation/augustus_ab_initio/augustus_output.aa \
	-o /Results/functional_annotation/augustus_ab_initio/eggNOG/augustus_output
```

```python
# afficher le nombre de gènes ayant reçu une annotation
cut -f1 eggNOG/augustus_output.emapper.annotations | sort | uniq | wc -l
# afficher le nombre de hits dans la base eggNOG
cut -f2 eggNOG/augustus_output.emapper.annotations | sort | uniq | wc -l
```

##### Fichiers obtenus


- augustus_regular.emapper.annotations : résultats de l'annotation fonctionnelle selon toutes les bases de données répertoriées dans eggNOG
- augustus_regular.emapper.hits : liste de tous les hits par protéine d'entrée 
- augustus_regular.emapper.seed_orthologs : sortie des runs DIAMOND sur les bases de données


## InterProScan


Afin de chercher les protéines trouvées pour chaque annotation dans la base InterPro, on utilise l'outil InterProScan :


##### Documentation

https://interproscan-docs.readthedocs.io/en/latest/

https://interpro-documentation.readthedocs.io/en/latest/databases.html


##### Commandes


Le fichier de protéines fourni par les outils contient des astérisques, représentant les codons STOP à la fin des protéines. InterProScan les considère comme des caractères étrangers, il faut donc les retirer grâce à un script python.

```python
cd /Results/functional_annotation/augustus_ab_initio/InterProScan
```

```python
python3 /Data/scripts/remove_astk.py /Results/structural_annotation/augustus_ab_initio/augustus_output.aa proteins.aa
```

On peut ensuite lancer InterProScan. On ne le lance ici que sur les bases de données NCBIfam (https://www.ncbi.nlm.nih.gov/genome/annotation_prok/evidence/), SUPERFAMILY (https://supfam.mrc-lmb.cam.ac.uk/), PANTHER (http://www.pantherdb.org/), ProSite (https://prosite.expasy.org/), PIRSF (https://proteininformationresource.org/pirsf/) et Pfam (https://pfam.xfam.org/) afin de limiter les temps de calcul.

```python
{ time /Data/tools/interproscan/interproscan.sh -i proteins.aa -b augustus_ab_initio -goterms -pa -iprlookup -cpu 40 -appl NCBIFAM,SUPERFAMILY,PANTHER,ProSiteProfiles,PIRSR,ProSitePatterns,Pfam,PIRSF ; } 2> time.txt
```

```python
# afficher le nombre de gènes ayant reçu une annotation
cut -f1 InterProScan/augustus_ab_initio.tsv | sort | uniq | wc -l
# afficher le nombre de hits dans les bases InterPro
cut -f5 InterProScan/augustus_ab_initio.tsv | sort | uniq | wc -l
```

##### Fichiers obtenus


- augustus_ab_initio.gff3 : résultats de l'annotation fonctionnelle au format GFF3
- augustus_ab_initio.json : résultats de l'annotation fonctionnelle au format JSON
- augustus_ab_initio.tsv : résultats de l'annotation fonctionnelle au format TSV
- augustus_ab_initio.xml : résultats de l'annotation fonctionnelle au format XML
- time.txt : durée du run


## FunAnnotate


L'outil FunAnnotate permet d'effectuer une annotation fonctionnelle en y incorporant les résultats des annotations par eggNOG et InterPro.


##### Documentation

https://funannotate.readthedocs.io/en/latest/

https://funannotate.readthedocs.io/en/latest/annotate.html#annotate


##### Commandes


Pour les fichiers AUGUSTUS et BRAKER/GALBA :

```python
funannotate annotate --gff augustus_output.gff3 --fasta centaurea_genome.fasta.masked -s Centaurea_corymbosa -o augustus_output \
	--iprscan ../InterProScan/augustus_ab_initio.xml --eggnog ../eggNOG/augustus_output.emapper.annotations --busco_db eukaryota --cpus 30
```

```python
cut -f3,9 augustus_output/annotate_results/Centaurea_corymbosa.gff3 | grep "product=hypothetical protein;$" | awk '{split($2,a,";"); split(a[1],b,"="); print b[2]}' > unannotated_genes.txt
u=$(grep -c "g" unannotated_genes.txt)
p=$(cut -f3 augustus_output.gff3 | grep -c "mRNA")
echo "$d : $(($p-$u))" > nb_of_annotated_genes.txt
```

Pour les fichiers LiftOn :

```python
funannotate annotate --gff augustus_output.gff3 --fasta centaurea_genome.fasta.masked -s Centaurea_corymbosa -o augustus_output \
	--iprscan ../InterProScan/augustus_ab_initio.xml --eggnog ../eggNOG/augustus_output.emapper.annotations --busco_db eukaryota --cpus 30
```

```python
# pour Cynara cardunculus
cut -f3,9 augustus_output/annotate_results/Centaurea_corymbosa.gff3 | grep "CHANGER ÇA" | awk '{split($2,a,";"); split(a[1],b,"="); print b[2]}' > unannotated_genes.txt
u=$(grep -c "g" unannotated_genes.txt)
p=$(cut -f3 augustus_output.gff3 | grep -c "mRNA")
echo "$d : $(($p-$u))" > nb_of_annotated_genes.txt
# pour les deux autres
cut -f3,9 augustus_output/annotate_results/Centaurea_corymbosa.gff3 | grep "product=hypothetical protein;$" | awk '{split($2,a,";"); split(a[1],b,"="); print b[2]}' > unannotated_genes.txt
u=$(grep -c "g" unannotated_genes.txt)
p=$(cut -f3 augustus_output.gff3 | grep -c "mRNA")
echo "$d : $(($p-$u))" > nb_of_annotated_genes.txt
```

--> malheureusement ça n'a pas marché SAUF? pour C. card donc on peut skip et revenir au cas augustus/braker/galba


##### Fichiers obtenus


- augustus_output : dossier d'output (nom donné en paramètre -o)
- funannotate-annotate.9809e2de.log : fichier de log
- genome1.fixedproducts
- genome2.fixedproducts
- genome3.fixedproducts
- genome4.fixedproducts
- genome5.fixedproducts
- genome6.fixedproducts
- genome7.fixedproducts
- genome8.fixedproducts
- nb_of_annotated_genes.txt : nombre de gènes annotés
- time.txt : temps du run
- unannotated_genes.txt : liste des gènes non-annotés

Dans le dossier d'ouput :
- annotate_misc : fichiers intermédiaires
- annotate_results : résultats finaux
- logfiles : fichiers de log

Dans le dossier annotate_results :

- Centaurea_corymbosa.agp : fichier AGP (liens entre et emplacement des contigs)
- Centaurea_corymbosa.annotations.txt : fichier TSV des annotations
- Centaurea_corymbosa.cds-transcripts.fa : fichier FASTA des transcrits des séquences codantes
- Centaurea_corymbosa.contigs.fsa : fichier FASTA des contigs séparés au niveau des gaps
- Centaurea_corymbosa.discrepency.report.txt : rapport tbl2asn du génome annoté
- Centaurea_corymbosa.gbk : génome annoté au format GenBank
- Centaurea_corymbosa.gff3 : génome annoté au format GFF3
- Centaurea_corymbosa.mrna-transcripts.fa : fichier FASTA des transcrits (ARNm)
- Centaurea_corymbosa.part_1.sqn
- Centaurea_corymbosa.part_1.tbl
- Centaurea_corymbosa.part_2.sqn
- Centaurea_corymbosa.part_2.tbl
- Centaurea_corymbosa.part_3.sqn
- Centaurea_corymbosa.part_3.tbl
- Centaurea_corymbosa.part_4.sqn
- Centaurea_corymbosa.part_4.tbl
- Centaurea_corymbosa.part_5.sqn
- Centaurea_corymbosa.part_5.tbl
- Centaurea_corymbosa.part_6.sqn
- Centaurea_corymbosa.part_6.tbl
- Centaurea_corymbosa.part_7.sqn
- Centaurea_corymbosa.part_7.tbl
- Centaurea_corymbosa.part_8.sqn
- Centaurea_corymbosa.part_8.tbl
- Centaurea_corymbosa.proteins.fa : fichier FASTA des gènes codant des protéines
- Centaurea_corymbosa.scaffolds.fa : fichier FASTA des scaffolds
- Centaurea_corymbosa.stats.json : statistiques sur le génome annoté
- Centaurea_corymbosa.tbl : fichier TBL d'annotation NCBI
- Gene2Products.must-fix.txt : fichier TSV des gènes qui n'ont pas passé le contrôle tbl2asn and doivent être corrigés
- Gene2Products.need-curating.txt : fichier TSV qui ont besoin d'être curés
- Gene2Products.new-names-passed.txt : fichier TSV des gènes qui ont passé le contrôle tbl2asn mais ne sont pas dans la base de données Gene2Products


## GOMAP


On utilise ici GOMAP-singularity, une version contenairisée du pipeline GOMAP (Gene Ontology Meta Annotator for Plants).

Le pipeline compte 7 étapes différentes :
- seqsim
- domain
- fanngo
- mixmeth-blast
- mixmeth-preproc
- mixmeth
- aggregate


##### Documentation
https://bioinformapping.com/gomap/master/index.html


##### Commandes


Les quatre premières étapes du pipeline peuvent être exécutées en parallèle :

```python
sudo ./run-GOMAP-SINGLE.sh --step=seqsim --config=config.yml
```

```python
sudo ./run-GOMAP-SINGLE.sh --step=domain --config=config.yml
```

```python
sudo ./run-GOMAP-SINGLE.sh --step=fanngo --config=config.yml
```

```python
sudo ./run-GOMAP-SINGLE.sh --step=mixmeth-blast --config=config.yml
```

Les trois dernières étapes, par contre, doivent être exécutées l'une après l'autre car elle prennent en entrée des sorties de l'étape précédente :

```python
sudo ./run-GOMAP-SINGLE.sh --step=mixmeth-preproc --config=config.yml
```

```python
sudo ./run-GOMAP-SINGLE.sh --step=mixmeth --config=config.yml
```

```python
sudo ./run-GOMAP-SINGLE.sh --step=aggregate --config=config.yml
```

##### Fichiers obtenus


--> trouver comment interpréter les fichiers de sortie de GOMAP
