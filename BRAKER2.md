# Sommaire


- [Lancement](#commandes)

- [Résultats](#fichiers-obtenus)

    - [Statistiques basiques](#fichiers-obtenus-1)
    
    - [Statistiques avec gFACs](#fichiers-obtenus-2)
    
    - [Analyse avec BUSCO](#synthèse-des-résultats)
    
    - [Analyse avec OMArk](#synthèse-des-résultats-1)
    
- [Liste des protéines](#protéines-disponibles)


# BRAKER2


BRAKER2 a été lancé 4 fois : d'abord sur toutes les protéines d'espèces proches (publiquement) disponibles, puis sur les protéines du clade des Eudicotylédons de la base OrthoDB 11 (https://bioinf.uni-greifswald.de/bioinf/partitioned_odb11/), puis sur une combinaison des protéines des Eudicotylédons et de <i>Centaurea solstitialis</i> et une combinaison des protéines des Eudicotylédons et de <i>Cynara cardunculus</i>.

La liste des protéines d'espèces proches disponibles selon les espèces et les plateformes peut être trouvée à la fin du document.


##### Documentation
https://github.com/Gaius-Augustus/BRAKER

![Schéma du pipeline BRAKER2](images/figures_pipelines/braker2.png)

![Schéma complet du pipeline BRAKER2](images/figures_pipelines/braker2-full.png)


##### Commandes


Afin d'obtenir le clase des Eudicotylédons depuis la base OrthoDB 11, on utilise le script selectClade de l'outil orthodb-clades (https://github.com/tomasbruna/orthodb-clades), qui permet de diviser toute la base en différents taxons.

```python
cd /Data/dbs/orthodb-clades
mkdir orthoDB
cd orthoDB
```

```python
# On télécharge d'abord les bases nécessaires, puis on les renomme selon les nominations du script
wget https://data.orthodb.org/download/odb11v0_all_fasta.tab.gz
aunpack odb11v0_all_fasta.tab.gz
mv odb11v0_all_fasta.tab raw.fasta
wget https://data.orthodb.org/download/odb11v0_species.tab.gz
aunpack odb11v0_species.tab.gz
mv odb11v0_species.tab species.tab
wget https://data.orthodb.org/download/odb11v0_level2species.tab.gz
aunpack odb11v0_level2species.tab.gz
mv odb11v0_level2species.tab level2species.tab
wget https://data.orthodb.org/download/odb11v0_levels.tab.gz
aunpack odb11v0_levels.tab.gz
mv odb11v0_levels.tab levels.tab
```

```python
# On sélectionne d'abord les eucaryotes, puis les plantes, puis les eudicotylédons
./selectClade.py orthoDB/raw.fasta orthoDB/levels.tab orthoDB/level2species.tab Eukaryota > clades/Eukaryota.fa
./selectClade.py clades/Eukaryota.fa orthoDB/levels.tab orthoDB/level2species.tab Viridiplantae > clades/Viridiplantae.fa
./selectClade.py clades/Eukaryota.fa orthoDB/levels.tab orthoDB/level2species.tab eudicotyledons > clades/Eudicotyledons.fa
```

On peut ensuite lancer BRAKER2 avec le fichier obtenu.

```python
# activer l'environnement contenant les dépendances de braker
mamba activate braker_env
# exporter les outils d'alignement utilisés par GeneMark dans le PATH
export PATH=$PATH:/Data/tools/GeneMark-ETP/tools:/Data/tools/compleasm_kit
# Lancer BRAKER2 sur toutes les protéines trouvées sur le gène masqué
{ time /Data/tools/BRAKER/scripts/braker.pl \
	--workingdir=/Results/structural_annotation/BRAKER2_$proteinSet/ \
	--genome=/Data/genomes/masked_genome/centaurea_genome.fasta.masked \
	--prot_seq=/Data/Centaurea/proteins/$proteins_fasta \
	--busco_lineage=eudicots_odb10 \
	--threads=15 \
	--AUGUSTUS_CONFIG_PATH=/Data/tools/Augustus/config/ \
	--AUGUSTUS_BIN_PATH=/Data/tools/Augustus/bin/ \
	--AUGUSTUS_SCRIPTS_PATH=/Data/tools/Augustus/scripts/ \
	--COMPLEASM_PATH=/Data/tools/compleasm_kit/ \
	--GENEMARK_PATH=/Data/tools/GeneMark-ETP/bin/ \
	--PROTHINT_PATH=/Data/tools/ProtHint/bin/ \
	--TSEBRA_PATH=/Data/tools/TSEBRA/bin/ ; } 2> /Results/times/BRAKER2_$proteinSet_time.txt
```

```python
# on transforme l'annotation obtenue en format GFF3 pour pouvoir l'utiliser plus tard avec funannotate
cat braker.gtf | perl -ne 'if(m/\tAUGUSTUS\t/ or m/\tGeneMark\.hmm\t/ or m/\tGeneMark\.hmm3\t/ or m/\tgmst\t/) {print $_;}' | gtf2gff.pl --gff3 --out=braker.gff3
```

##### Fichiers obtenus


- Augustus : résultats de l'annotation brute par Augustus
- bbc : résultats des comparaisons BUSCO de compleasm
- best_by_compleasm.log : fichier de log de compleasm
- braker.aa : séquences protéiques de l'annotation
- braker.codingseq : séquences codantes de l'annotation
- braker.gtf : annotation
- braker.log : fichier de log
- errors : sorties d'erreur
- GeneMark-EP : résultats de l'annotation brute par GeneMark-EP
- GeneMark-ES : résultats de l'annotation brute par GeneMark-ES
- genome_header.map : liste des headers du génome
- hintsfile.gff : fichier GFF contenant les hints
- prothint.gff
- species : paramètres d'espèces (personnalisés grâce à l'annotation par GeneMark) utilisés par Augustus
- what-to-cite.txt : citations du pipeline et de ses différents outils


##### Hints

<!-- #region -->
Après avoir fait un alignement de protéines pour faire une première annotation avec GeneMark, des hints sont extraits de cette annotation.


| | Sur toutes les espèces proches | Sur le clade Eudicotylédons (OrthoDB 11) | Clade + <i>Cynara cardunculus</i> | Clade + <i>Centaurea solstitialis</i> |
| :- | :-: | :-: | :-: | :-: |
Nombre de hints | 825 558 | 718 325 | 697 612 | 780 952
<!-- #endregion -->

## Analyse


### Statistiques de base


##### Commandes

```python
grep -c ">" braker.aa > nb_of_prot_seq.txt
```

```python
# Ploter l'histogramme du nombre d'exons trouvés selon leur taille
python3 /Data/scripts/exons_len_plot.py braker.gtf .
# Ploter l'histogramme du nombre de gènes trouvés selon leur taille
python3 /Data/scripts/gene_len_plot.py braker.gtf .
# Ploter l'histogramme du nombre de transcrits trouvés selon leur taille
python3 /Data/scripts/transcripts_len_plot.py braker.gtf .
# Ploter l'histogramme du nombre de gènes selon leur nombre d'exons (moyenne sur tous les transcrits)
python3 /Data/scripts/exons_mean_nbr_plot_stat.py braker.gtf .
# Ploter le scatter plot de la taille de gènes par leur nombre moyen d'exons
python3 /Data/scripts/gene_len_by_mean_exons.py braker.gtf .
```

```python
mkdir plots
mv *.png plots
mkdir stats
mv *n*.txt stats
```

##### Fichiers obtenus


- exons_length_histogram.png : histogramme du nombre d'exons trouvés selon leur taille
- exons_mean_nbr_histogram.png : histogramme du nombre de gènes selon leur nombre d'exons (moyenne sur tous les transcrits trouvés pour le gène)
- exons_mean_nbr_stat.txt : statistiques sur les exons par gènes (moyenne sur tous les transcrits)
- gene_len_by_mean_exons_scatterplot.png : scatter plot de la taille de gènes par leur nombre moyen d'exons
- gene_length_histogram.png : histogramme du nombre de gènes trouvés selon leur taille
- nb_of_prot_seq.txt : nombre de séquences protéiques extraites de l'annotation
- transcripts_length_histogram.png : histogramme du nombre de transcrits trouvés selon leur taille


Ces résultats ont été synthétisés en tableaux :

| | Sur toutes les espèces proches | Sur le clade des Eudicotylédons (issu d'OrthoDB 11) | Eudicotylédons + <i>Cynara cardunculus</i> | Eudicotylédons + <i>Centaurea solstitialis</i> |
| :- | :-: | :-: | :-: | :-: |
Nombre de protéines trouvées | 66 028 | 53 454 | 53 833 | 57 084 |

<br>

| | Sur toutes les espèces proches | Sur le clade des Eudicotylédons (issu d'OrthoDB 11) | Eudicotylédons + <i>Cynara cardunculus</i> | Eudicotylédons + <i>Centaurea solstitialis</i> |
| :- | :-: | :-: | :-: | :-: |
Nombre de gènes | 58 073 | 49 699 | 49 991 | 51 630 
Nombre de gènes mono-exoniques | 19 114 | 14 710 | 14 986 | 15 732 
Nombre de gènes multi-exoniques | 38 959 | 34 989 | 35 005 | 35 898 
Ratio mono/multi | 0.49 | 0.42 | 0.43 | 0.44 
Nombre médian d’exons | 2 | 2 | 2 | 2 
Nombre maximum d’exons | 307 | 151 | 76 | 399


### gFACs


##### Commandes


gFACs a une option format (-f) pour renseigner le format du fichier GTF donné en entrée. Pour savoir le format à renseigner, on peut utiliser le script de support format_diagnosis.pl (https://gfacs.readthedocs.io/en/latest/Bonus/format_diagnosis.html). Ce script renvoie une suite de valeurs que l'on peut ensuite aller comparer au tableau de formats de la documentation gFACs (https://gfacs.readthedocs.io/en/latest/_images/Table.png) pour déterminer quel format gFACs correspond au nôtre, ou au moins lequel en est le plus proche.

```python
cd /Results/structural_annotation/BRAKER2_$proteinSet
perl /opt/biotools/conda/envs/gFACs/opt/perl-gfacs-1.1.1/support_scripts/format_diagnosis.pl braker.gtf
```

Le format correspondant au fichier gtf obtenu est braker_2.0_gtf.

```python
mkdir gFACs
mamba activate gFACs
gFACs.pl -f braker_2.0_gtf --statistics --fasta /Data/genomes/original_genome/centaurea_genome.fasta --splice-table --nt-content \
	--get-fasta --get-protein-fasta --create-gff3 --distributions exons_lengths intron_lengths CDS_lengths gene_lengths \
	exon_position exon_position_data intron_position intron_position_data -O gFACs braker.gtf
```

##### Fichiers obtenus


- CDS_lengths_distributions.tsv : distribution des longueurs des séquences codantes
- data_exon_position_distributions.tsv : liste des positions des exons ?
- data_intron_position_distributions.tsv : liste des positions des exons ?
- exon_position_distributions.tsv : distribution de la position des exons
- gene_lengths_distributions.tsv : distribution des longueurs des gènes
- genes.fasta : liste des gènes utilisés par gFACs pour ses statistiques
- genes.fasta.faa : liste des gènes utilisés par gFACs pour ses statistiques accompagnés de certaines séquences protéiques
- gene_table.txt : table de gènes crées par gFACs pour ses statistiques
- gFACs_log.txt : fichier de log
- intron_lengths_distributions.tsv : distribution des longueurs des introns
- intron_position_distributions.tsv : distribution de la position des introns
- out.gff3 : fichier contenant les éléments étudiés par gFACs (mRNA, exons, ...)
- statistics.txt : statistiques produites par gFACs


Le résumé des statistiques obtenues se trouve dans deux tableaux de synthèse :

![1er tableau de synthèse de gFACs](images/gFACs/BRAKER2_gFACs_1.svg)

![2ème tableau de synthèse de gFACs](images/gFACs/BRAKER2_gFACs_2.svg)


### BUSCO


L'outil BUSCO permet de faire une recherche des séquences codantes trouvées dans la base de données orthoDB (https://www.orthodb.org/) d'une lignée afin de voir si les gènes prédits y sont retrouvés. La lignée sélectionnée ici est la lignée eudicots_odb10.


##### Documentation
https://busco.ezlab.org/busco_userguide.html


##### Commandes


Pour lancer BUSCO en mode transcriptome, il faut d'abord faire en sorte de ne sélectionner qu'un isoforme (= transcrit) par gène. On utilise donc AGAT pour ne garder que les plus isoformes aux plus longs CDS.

```python
cd /Results/structural_annotation/BRAKER2_$proteinSet
mamba activate agat
mkdir without_isoforms
agat_sp_keep_longest_isoform.pl -gff braker.gtf -o without_isoforms/agat_without_isoforms.gtf
```

Il faut ensuite reformater le fichier produit par AGAT pour qu'il corresponde à un format Augustus, ce qui nous permettra de lancer ensuite le script d'Augustus permettat d'obtenir les séquences codantes et protéiques d'une annotation.

```python
cd without_isoforms
python3 /Data/scripts/reformat_agat_gtf.py agat_without_isoforms.gtf without_isoforms.gtf
/Data/tools/Augustus/scripts/getAnnoFastaFromJoingenes.py -g /Data/genomes/masked_genome/centaurea_genome.fasta.masked \
	-o without_isoforms -f without_isoforms.gtf
```

Le script produit deux fichiers : without_isoforms.aa et without_isoforms.codingseq.

On peut enfin lancer BUSCO sur le fichier .codingseq obtenu.

```python
cd ..
mamba activate busco
busco -m transcriptome -i without_isoforms/without_isoforms.codingseq -o busco_results -l eudicots_odb10 -c 8
```

##### Fichiers obtenus


- busco.log : log de la commande busco
- logs : dossier des logs de tous les processus de busco
- run_eudicots_odb10 : dossier contenant les séquences trouvées par busco en format FASTA et GFF, les identifiants des séquences manquantes, et l'output de hmmer et metaeuk
- short_summary.specific.eudicots_odb10.busco_results.json : résumé du résultat busco
- short_summary.specific.eudicots_odb10.busco_results.txt : résumé du résultat busco


##### Synthèse des résultats


| | Sur toutes les espèces proches | Sur le clade des Eudicotylédons (issu d'OrthoDB 11) | Eudicotylédons + <i>Cynara cardunculus</i> | Eudicotylédons + <i>Centaurea solstitialis</i> |
| :- | :-: | :-: | :-: | :-: |
Complet | 2215 (95.2%) | 2218 (95.3%) | 2220 (95.5%) | 2220 (95.5%)
Complet unique | 2070 (89%) | 2073 (89.1%) | 2074 (89.2%) | 2073 (89.1%)
Complet dupliqué | 145 (6.2%) | 145 (6.2%) | 146 (6.3%) | 147 (6.4%)
Fragmenté | 26 (1.1%) | 23 (1%) | 21 (0.9%) | 21 (0.9%)
Manquant | 85 (3.6%) | 85 (3.6%) | 85 (3.6%) | 85 (3.6%)
Total / 2326 | 2241 (96.4%) | 2241 (96.4%) | 2241 (96.3%) | 2241 (96.3%)


### OMArk


L'outil OMArk permet de faire une recherche des séquences protéiques trouvées dans la base de données OMA (https://omabrowser.org/oma/home/) d'une lignée afin de voir si les gènes prédits y sont retrouvés. La lignée sélectionnée ici est la lignée Asteranae (clade). Cette lignée comprend des protéines de 11 espèces :
- <i>Actinidia chinensis var. chinensis</i>
- <i>Capsicum annuum</i>
- <i>Coffea canephora</i>
- <i>Cynara cardunculus var. scolymus</i>
- <i>Daucus carota subsp. sativus</i>
- <i>Erythranthe guttata</i>
- <i>Helianthus annuus</i>
- <i>Nicotiana attenuata</i>
- <i>Nicotiana tabacum</i>
- <i>Solanum lycopersicum</i>
- <i>Solanum tuberosum</i>


##### Documentation
https://github.com/DessimozLab/OMArk/tree/main

https://github.com/DessimozLab/omamer


##### Commandes


Il faut d'abord faire une recherche dans la base OMA (ici <i>Viridiplantae</i>) avec l'outil OMAmer, puis soumettre le résultats de cette recherche à OMArk.

```python
cd /Results/structural_annotation/BRAKER2_$proteinSet
mamba activate OMArk
mkdir OMAmer-OMArk
omamer search -d /Data/dbs/OMA/Viridiplantae.h5 -q braker.aa -o OMAmer-OMArk/omamer.out -t 8
```

OMArk utilise les protéines générées depuis l'annotation et n'en sélectionne qu'une par gène, il faut donc lui soumettre la liste des transcrits pour chaque gène pour qu'il choisisse l'un de ces isoformes.

```python
# On crée un fichier .splice contenant les isoformes
python3 /Data/scripts/make_splice_file.py braker.aa
```

```python
cd OMAmer-OMArk
# On donne à omark le rang taxonomique le plus proche (parmi la liste des rangs acceptés) avec l'option -r,
# et l'identifiant taxonomique de ce rang pour notre espèce avec l'option -t
omark -f omamer.out -d /Data/dbs/OMA/Viridiplantae.h5 -o omark -r family -t 4210 -of ../braker.aa --isoform_file ../braker.aa.splice
```

##### Fichiers obtenus


- omamer_Cynara_cardunculus_var_scolymus.fasta : liste des protéines ayant été retrouvées dans la base de <i>Cynara cardunculus</i>
- omamer_detailed_summary.txt : résumé détaillé des résultats
- omamer_mapped.fasta : liste des protéines retrouvées dans la lignée donnée
- omamer_misplaced.fasta : liste des protéines retrouvées dans d'autres lignées que la lignée donnée
- omamer_no_hits.fasta :  : liste des protéines non-retrouvées dans la base de données
- omamer.omq : liste des protéines uniques retrouvées
- omamer.pdf : histogramme des pourcentages de protéines selon leur catégorie
- omamer.png : histogramme des pourcentages de protéines selon leur catégorie
- omamer_selected_isoforms.txt : liste des isoformes (= transcrits) choisis pour chaque gène par omark
- omamer.sum : résumé simplifié des résultats
- omamer.tax : rappel de la lignée et clade sélectionnés
- omamer.ump : liste des protéines complètes retrouvés dans la lignée


##### Synthèse des résultats


Pour rappel, OMArk ne sélectionne qu'un seul isoforme par gène : il ne prend donc pas en compte toutes les protéines fournies par nos annotations.

Le nombre de protéines prises en compte dans nos annotations par OMArk est :

| | Sur toutes les espèces proches | Sur le clade des Eudicotylédons (issu d'OrthoDB 11) | Eudicotylédons + <i>Cynara cardunculus</i> | Eudicotylédons + <i>Centaurea solstitialis</i> |
| :- | :-: | :-: | :-: | :-: |
Nombre total de protéines | 58 073 | 49 699 | 49 991 | 51 630


Le premier tableau permet de voir les gènes trouvés dans la lignée sélectionnée, et en combien de copies, similairement à BUSCO :

| | Sur toutes les espèces proches | Sur le clade des Eudicotylédons (issu d'OrthoDB 11) | Eudicotylédons + <i>Cynara cardunculus</i> | Eudicotylédons + <i>Centaurea solstitialis</i> |
| :- | :-: | :-: | :-: | :-: |
Unique | 7 543 (67.36%) | 7 569 (67.59%) | 7 574 (67.64%) | 7 553 (67.45%)
Dupliqué | 3 246 (28.99%) | 3 214 (28.70%) | 3 209 (28.66%) | 3 220 (28.76%)
Dupliqué attendu | 2 004 (17.90%) | 1 991 (17.78%) | 1 987 (17.74%) | 2 002 (17.88%)
Dupliqué inattendu | 1 242 (11.09%) | 1 223 (10.92%) | 1 222 (10.91%) | 1 218 (10.88%)
Manquant | 409 (3.65%) | 415 (3.71%) | 415 (3.71%) | 425 (3.8%)
Total / 11 198 | 10 789 (96.35%) | 10 783 (96.29%) | 10 783 (96.29%) | 10 773 (96.2%)


Le deuxième tableau obtenu permet de voir les gènes retrouvés dans des espèces appartenant à la lignée sélectionnée (Consistent), ceux retrouvés dans des espèces non incluses dans la lignée (Inconsistent), et enfin ceux n'ayant pas été trouvé dans la base de données OMA (Inconnu) :

| | Sur toutes les espèces proches | Sur le clade des Eudicotylédons (issu d'OrthoDB 11) | Eudicotylédons + <i>Cynara cardunculus</i> | Eudicotylédons + <i>Centaurea solstitialis</i> |
| :- | :-: | :-: | :-: | :-: |
Consistent | 35 922 (61.86%) | 33 021 (66.44%) | 33 176 (66.36%) | 34 400 (66.63%)
Consistent partiel | 6 894 (11.87%) | 5 688 (11.44%) | 5 686 (11.37%) | 6 209 (12.03%)
Consistent fragmenté | 2 954 (5.09%) | 2 266 (4.56%) | 2 457 (4.91%) | 2 657 (5.15%)
Inconsistent | 2 872 (4.95%) | 2 268 (4.56%) | 2 273 (4.55%) | 2 399 (4.65%)
Inconsistent partiel | 1 999 (3.44%) | 1 495 (3.01%) | 1 491 (2.98%) | 1 632 (3.16%)
Inconsistent fragmenté | 268 (0.46%) | 228 (0.46%) | 242 (0.48%) | 214 (0.41%)
Inconnu | 19 279 (33.2%) | 14 410 (28.99%) | 14 542 (29.09%) | 14 831 (28.73%)

<!-- #region -->
L'espèce chez laquelle le plus de gènes ont été retrouvés est <b><i>Cynara cardunculus</i></b> pour toutes les annotations :

| | Sur toutes les espèces proches | Sur le clade des Eudicotylédons (issu d'OrthoDB 11) | Eudicotylédons + <i>Cynara cardunculus</i> | Eudicotylédons + <i>Centaurea solstitialis</i> |
| :- | :-: | :-: | :-: | :-: |
Espèce détectée : <i>Cynara cardunculus</i> | 38 794 (66.8%) | 35 289 (71.01%) | 35 449 (70.91%) | 36 799 (71.27%)


Cela est certainement dû au fait qu'OMA ne contient que 11 espèces dans son clade des Astéridées, et <i>Cynara cardunculus</i> est la seule de ces espèces à être assez proche de notre Centaurée.
<!-- #endregion -->

<br>

# Protéines disponibles


Voici les séquences protéiques trouvées sur différentes plateformes pour toutes les espèces proches de <i>Centaurea corymbosa</i> :
<br>
<br>

<i>Arctium lappa</i> :
- https://www.ncbi.nlm.nih.gov/protein/?term=txid4217[organism:exp] (NCBI : 41 260 séquences protéiques)
- https://www.uniprot.org/uniprotkb?query=%28+Arctium+lappa+%29 (UniProt : 165 séquences)
<br>
<br>

<i>Carthamus tinctorius</i> :
- https://www.ncbi.nlm.nih.gov/protein/?term=txid4222[organism:exp] (NCBI : 1 249 séquences protéiques)
- https://www.uniprot.org/uniprotkb?query=%28+Carthamus+tinctorius+%29 (UniProt : 344 séquences)
<br>
<br>

<i>Cynara cardunculus</i> :
- https://www.ncbi.nlm.nih.gov/protein/?term=txid4265[organism:exp] (NCBI : 66 775 séquences protéiques)
- https://www.uniprot.org/uniprotkb?query=%28+Cynara+cardunculus+%29 (UniProt : 26 850 séquences)
- http://plants.ensembl.org/Multi/Search/Results?q=Cynara%20cardunculus;species=all;collection=all;site=ensemblunit (ensembl : 26 505 séquences)
- https://www.orthodb.org/?level=71240&species=59895_0 (OrthoDB : 14 996 groupes)
<br>
<br>

<i>Saussurea involucrata</i> :
- https://www.ncbi.nlm.nih.gov/protein/?term=txid200489[organism:exp] (NCBI : 400 séquences protéiques)
- https://www.uniprot.org/uniprotkb?query=%28+Saussurea+involucrata+%29 (UniProt : 119 séquences)
<br>
<br>

<i>Cyanus segetum</i> ou <i>Centaurea cyanus</i> :
- https://www.ncbi.nlm.nih.gov/protein/?term=txid41522[organism:exp] (NCBI : 320 séquences protéiques)
- https://www.uniprot.org/uniprotkb?query=%28+Centaurea+cyanus+%29 (UniProt : 114 séquences)
<br>
<br>

<i>Carduncellus pinnatus</i> ou <i>Carthamus pinnatus</i> :
- https://www.ncbi.nlm.nih.gov/protein/?term=txid123806[organism:exp] (NCBI : 7 séquences protéiques)
- https://www.uniprot.org/uniparc?query=%28+Carduncellus+pinnatus+%29 (UniProt : 3 séquences)
<br>
<br>

<i>Centaurea solstitialis</i> :
- https://www.ncbi.nlm.nih.gov/protein/?term=txid347529[organism:exp] (NCBI : 33 540 séquences protéiques)
- https://www.uniprot.org/uniprotkb?query=%28+Centaurea+solstitialis+%29 (UniProt : 33 409 séquences)
<br>
<br>

<i>Centaurea jacea</i> :
- https://www.ncbi.nlm.nih.gov/protein/?term=txid351340[organism:exp] (NCBI : 42 séquences protéiques)
- https://www.uniprot.org/uniprotkb?query=%28+Centaurea+jacea+%29 (UniProt : 44 séquences)
