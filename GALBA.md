# Sommaire


- [Lancement](#commandes)

- [Résultats](#fichiers-obtenus)

    - [Statistiques basiques](#fichiers-obtenus-1)
    
    - [Statistiques avec gFACs](#fichiers-obtenus-2)
    
    - [Analyse avec BUSCO](#synthèse-des-résultats)
    
    - [Analyse avec OMArk](#synthèse-des-résultats-1)


# GALBA


GALBA a été lancé 3 fois : d'abord sur toutes les protéines d'espèces proches trouvées, puis sur les protéines de <i>Cynara cardunculus</i>, puis sur celles de <i>Centaurea solstitialis</i>.

La liste des protéines est disponible à la fin du document [BRAKER2](BRAKER2.md#protéines-disponibles).


##### Documentation
https://github.com/Gaius-Augustus/GALBA


![Schéma du pipeline GALBA](images/figures_pipelines/galba.png)


##### Commandes

```python
# activer l'environnement contenant les dépendances de galba
mamba activate galba_env
# exporter Augustus (erreur s'il n'est pas exporté)
export PATH=$PATH:/Data/tools/Augustus/bin/
#Lancer GALBA sur toutes les protéines trouvées sur le gène masqué
{ time /Data/tools/GALBA/scripts/galba.pl \
	--workingdir=/Results/structural_annotation/GALBA_$proteinSet/ \
	--genome=/Data/genomes/masked_genome/centaurea_genome.fasta.masked \
	--prot_seq=/Data/proteins/$proteins_fasta \
	--threads=15 \
	--SCORER_PATH=/Data/tools/miniprot-boundary-scorer/ \
	--MINIPROTHINT_PATH=/Data/tools/miniprothint/ \
	--MINIPROT_PATH=/Data/tools/miniprot/ \
	--TSEBRA_PATH=/Data/tools/TSEBRA/bin/ \
	--AUGUSTUS_CONFIG_PATH=/Data/tools/Augustus/config/ \
	--AUGUSTUS_BIN_PATH=/Data/tools/Augustus/bin/ \
	--AUGUSTUS_SCRIPTS_PATH=/Data/tools/Augustus/scripts/ \
	--DIAMOND_PATH=/Data/tools/diamond/ ; } 2> /Results/times/GALBA_$proteinSet_time.txt
```

```python
# on transforme l'annotation obtenue en format GFF3 pour pouvoir l'utiliser plus tard avec funannotate
cat galba.gtf | perl -ne 'if(m/\tAUGUSTUS\t/ or m/\tminiprot\t/ or m/\tgmst\t/) {print $_;}' | gtf2gff.pl --gff3 --out=galba.gff3
```

##### Fichiers obtenus


- archive : résultats de l'entraînement itératif et des annotations brutes par Augustus et miniprot
- errors : sorties d'erreur
- galba.aa : séquences protéiques de l'annotation
- galba.codingseq : séquences codantes de l'annotation
- galba.gtf : annotation
- GALBA.log : fichier de log
- genome_header.map : liste des headers du génome
- hintsfile.gff : fichier GFF contenant les hints
- optimize_augustus.stdout
- prot_seq_all.fa : lien symbolique vers le fichier de protéines donné en entrée
- species : paramètres d'espèces (personnalisés grâce à l'annotation par miniprot) utilisés par Augustus
- what-to-cite.txt : citations du pipeline et de ses différents outils


##### Hints

<!-- #region -->
Après avoir fait un alignement de protéines pour faire une première annotation avec GeneMark, des hints sont extraits de cette annotation.


| | Sur toutes les espèces proches | <i>Cynara cardunculus</i> | <i>Centaurea solstitialis</i> |
| :- | :-: | :-: | :-: |
Nombre de hints | 512 713 | 401 758 | 551 411
<!-- #endregion -->

## Analyse


### Statistiques de base


##### Commandes

```python
grep -c ">" galba.aa > nb_of_prot_seq.txt
```

```python
# Ploter l'histogramme du nombre d'exons trouvés selon leur taille
python3 /Data/scripts/exons_len_plot.py galba.gtf .
# Ploter l'histogramme du nombre de gènes trouvés selon leur taille
python3 /Data/scripts/gene_len_plot.py galba.gtf .
# Ploter l'histogramme du nombre de transcrits trouvés selon leur taille
python3 /Data/scripts/transcripts_len_plot.py galba.gtf .
# Ploter l'histogramme du nombre de gènes selon leur nombre d'exons (moyenne sur tous les transcrits)
python3 /Data/scripts/exons_mean_nbr_plot_stat.py galba.gtf .
# Ploter le scatter plot de la taille de gènes par leur nombre moyen d'exons
python3 /Data/scripts/gene_len_by_mean_exons.py galba.gtf .
```

```python
mkdir plots
mv *.png plots
mkdir stats
mv *n*.txt stats
```

##### Fichiers obtenus


- exons_length_histogram.png : histogramme du nombre d'exons trouvés selon leur taille
- exons_mean_nbr_histogram.png : histogramme du nombre de gènes selon leur nombre d'exons (moyenne sur tous les transcrits trouvés pour le gène)
- exons_mean_nbr_stat.txt : statistiques sur les exons par gènes (moyenne sur tous les transcrits)
- gene_len_by_mean_exons_scatterplot.png : scatter plot de la taille de gènes par leur nombre moyen d'exons
- gene_length_histogram.png : histogramme du nombre de gènes trouvés selon leur taille
- nb_of_prot_seq.txt : nombre de séquences protéiques extraites de l'annotation
- transcripts_length_histogram.png : histogramme du nombre de transcrits trouvés selon leur taille


Ces résultats ont été synthétisés en tableaux :

| | Sur toutes les espèces proches | <i>Cynara cardunculus</i> | <i>Centaurea solstitialis</i> |
| :- | :-: | :-: | :-: |
Nombre de protéines trouvées | 63 017 | 39 309 | 61 129 |

<br>

| | Sur toutes les espèces proches | <i>Cynara cardunculus</i> | <i>Centaurea solstitialis</i> |
| :- | :-: | :-: | :-: |
Nombre de gènes | 53 229 | 34 042 | 51 519
Nombre de gènes mono-exoniques | 20 158 | 8 842 | 16 979
Nombre de gènes multi-exoniques | 33 071 | 25 200 | 34 540
Ratio mono/multi | 0.61 | 0.35 | 0.49
Nombre médian d’exons | 2 | 3 | 2
Nombre maximum d’exons | 79 | 96 | 77


### gFACs


##### Commandes


gFACs a une option format (-f) pour renseigner le format du fichier GTF donné en entrée. Pour savoir le format à renseigner, on peut utiliser le script de support format_diagnosis.pl (https://gfacs.readthedocs.io/en/latest/Bonus/format_diagnosis.html). Ce script renvoie une suite de valeurs que l'on peut ensuite aller comparer au tableau de formats de la documentation gFACs (https://gfacs.readthedocs.io/en/latest/_images/Table.png) pour déterminer quel format gFACs correspond au nôtre, ou au moins lequel en est le plus proche.

```python
cd /Results/structural_annotation/GALBA_$proteinSet
perl /opt/biotools/conda/envs/gFACs/opt/perl-gfacs-1.1.1/support_scripts/format_diagnosis.pl galba.gtf
```

Le format correspondant au fichier gtf obtenu est braker_2.0_gtf.

```python
mkdir gFACs
mamba activate gFACs
gFACs.pl -f braker_2.0_gtf --statistics --fasta /Data/genomes/original_genome/centaurea_genome.fasta --splice-table --nt-content \
	--get-fasta --get-protein-fasta --create-gff3 --distributions exons_lengths intron_lengths CDS_lengths gene_lengths \
	exon_position exon_position_data intron_position intron_position_data -O gFACs galba.gtf
```

##### Fichiers obtenus


- CDS_lengths_distributions.tsv : distribution des longueurs des séquences codantes
- data_exon_position_distributions.tsv : liste des positions des exons ?
- data_intron_position_distributions.tsv : liste des positions des exons ?
- exon_position_distributions.tsv : distribution de la position des exons
- gene_lengths_distributions.tsv : distribution des longueurs des gènes
- genes.fasta : liste des gènes utilisés par gFACs pour ses statistiques
- genes.fasta.faa : liste des gènes utilisés par gFACs pour ses statistiques accompagnés de certaines séquences protéiques
- gene_table.txt : table de gènes crées par gFACs pour ses statistiques
- gFACs_log.txt : fichier de log
- intron_lengths_distributions.tsv : distribution des longueurs des introns
- intron_position_distributions.tsv : distribution de la position des introns
- out.gff3 : fichier contenant les éléments étudiés par gFACs (mRNA, exons, ...)
- statistics.txt : statistiques produites par gFACs


Le résumé des statistiques obtenues se trouve dans deux tableaux de synthèse :

![1er tableau de synthèse de gFACs](images/gFACs/GALBA_gFACs_1.svg)

![2ème tableau de synthèse de gFACs](images/gFACs/GALBA_gFACs_2.svg)


### BUSCO


L'outil BUSCO permet de faire une recherche des séquences codantes trouvées dans la base de données orthoDB (https://www.orthodb.org/) d'une lignée afin de voir si les gènes prédits y sont retrouvés. La lignée sélectionnée ici est la lignée eudicots_odb10.


##### Documentation
https://busco.ezlab.org/busco_userguide.html


##### Commandes


Pour lancer BUSCO en mode transcriptome, il faut d'abord faire en sorte de ne sélectionner qu'un isoforme (= transcrit) par gène. On utilise donc AGAT pour ne garder que les plus isoformes aux plus longs CDS.

```python
cd /Results/structural_annotation/GALBA_$proteinSet
mamba activate agat
mkdir without_isoforms
agat_sp_keep_longest_isoform.pl -gff galba.gtf -o without_isoforms/agat_without_isoforms.gtf
```

Il faut ensuite reformater le fichier produit par AGAT pour qu'il corresponde à un format Augustus, ce qui nous permettra de lancer ensuite le script d'Augustus permettat d'obtenir les séquences codantes et protéiques d'une annotation.

```python
cd without_isoforms
python3 /Data/scripts/reformat_agat_gtf.py agat_without_isoforms.gtf without_isoforms.gtf
/Data/tools/Augustus/scripts/getAnnoFastaFromJoingenes.py -g /Data/genomes/masked_genome/centaurea_genome.fasta.masked \
	-o without_isoforms -f without_isoforms.gtf
```

Le script produit deux fichiers : without_isoforms.aa et without_isoforms.codingseq.

On peut enfin lancer BUSCO sur le fichier .codingseq obtenu.

```python
cd ..
mamba activate busco
busco -m transcriptome -i without_isoforms/without_isoforms.codingseq -o busco_results -l eudicots_odb10 -c 8
```

##### Fichiers obtenus


- busco.log : log de la commande busco
- logs : dossier des logs de tous les processus de busco
- run_eudicots_odb10 : dossier contenant les séquences trouvées par busco en format FASTA et GFF, les identifiants des séquences manquantes, et l'output de hmmer et metaeuk
- short_summary.specific.eudicots_odb10.busco_results.json : résumé du résultat busco
- short_summary.specific.eudicots_odb10.busco_results.txt : résumé du résultat busco


##### Synthèse des résultats


| | Sur toutes les espèces proches | <i>Cynara cardunculus</i> | <i>Centaurea solstitialis</i> |
| :- | :-: | :-: | :-: |
Complet | 2216 (95.2%) | 2209 (94.9%) | 2197 (94.5%)
Complet unique | 2078 (89.3%) | 2080 (89.4%) | 2062 (88.7%)
Complet dupliqué | 138 (5.9%) | 129 (5.5%) | 135 (5.8%)
Fragmenté | 15 (0.6%) | 20 (0.9%) | 21 (0.9%)
Manquant | 95 (4.2%) | 97 (4.2%) | 108 (4.6%)
Total / 2326 | 2231 (95.8%) | 2229 (95.8%) | 2218 (95.4%)


### OMArk


L'outil OMArk permet de faire une recherche des séquences protéiques trouvées dans la base de données OMA (https://omabrowser.org/oma/home/) d'une lignée afin de voir si les gènes prédits y sont retrouvés. La lignée sélectionnée ici est la lignée Asteranae (clade). Cette lignée comprend 11 espèces :
- <i>Actinidia chinensis var. chinensis</i>
- <i>Capsicum annuum</i>
- <i>Coffea canephora</i>
- <i>Cynara cardunculus var. scolymus</i>
- <i>Daucus carota subsp. sativus</i>
- <i>Erythranthe guttata</i>
- <i>Helianthus annuus</i>
- <i>Nicotiana attenuata</i>
- <i>Nicotiana tabacum</i>
- <i>Solanum lycopersicum</i>
- <i>Solanum tuberosum</i>


##### Documentation
https://github.com/DessimozLab/OMArk/tree/main

https://github.com/DessimozLab/omamer


##### Commandes


Il faut d'abord faire une recherche dans la base OMA (ici <i>Viridiplantae</i>) avec l'outil OMAmer, puis soumettre le résultats de cette recherche à OMArk.

```python
cd /Results/structural_annotation/GALBA_$proteinSet
mamba activate OMArk
mkdir OMAmer-OMArk
omamer search -d /Data/dbs/OMA/Viridiplantae.h5 -q galba.aa -o OMAmer-OMArk/omamer.out -t 8
```

OMArk utilise les protéines générées depuis l'annotation et n'en sélectionne qu'une par gène, il faut donc lui soumettre la liste des transcrits pour chaque gène pour qu'il choisisse l'un de ces isoformes.

```python
# On crée un fichier .splice contenant les isoformes
python3 /Data/scripts/make_splice_file.py galba.aa
```

```python
cd OMAmer-OMArk
# On donne à omark le rang taxonomique le plus proche (parmi la liste des rangs acceptés) avec l'option -r,
# et l'identifiant taxonomique de ce rang pour notre espèce avec l'option -t
omark -f omamer.out -d /Data/dbs/OMA/Viridiplantae.h5 -o omark -r family -t 4210 -of ../galba.aa \
	--isoform_file ../galba.aa.splice
```

##### Fichiers obtenus


- omamer_Cynara_cardunculus_var_scolymus.fasta : liste des protéines ayant été retrouvées dans la base de <i>Cynara cardunculus</i>
- omamer_detailed_summary.txt : résumé détaillé des résultats
- omamer_mapped.fasta : liste des protéines retrouvées dans la lignée donnée
- omamer_misplaced.fasta : liste des protéines retrouvées dans d'autres lignées que la lignée donnée
- omamer_no_hits.fasta :  : liste des protéines non-retrouvées dans la base de données
- omamer.omq : liste des protéines uniques retrouvées
- omamer.pdf : histogramme des pourcentages de protéines selon leur catégorie
- omamer.png : histogramme des pourcentages de protéines selon leur catégorie
- omamer_selected_isoforms.txt : liste des isoformes (= transcrits) choisis pour chaque gène par omark
- omamer.sum : résumé simplifié des résultats
- omamer.tax : rappel de la lignée et clade sélectionnés
- omamer.ump : liste des protéines complètes retrouvés dans la lignée


##### Synthèse des résultats


Pour rappel, OMArk ne sélectionne qu'un seul isoforme par gène : il ne prend donc pas en compte toutes les protéines fournies par nos annotations.

Le nombre de protéines prises en compte dans nos annotations par OMArk est :

| | Sur toutes les espèces proches | <i>Cynara cardunculus</i> | <i>Centaurea solstitialis</i> |
| :- | :-: | :-: | :-: |
Nombre total de protéines | 53 229 | 34 042 | 51 519


Le premier tableau permet de voir les gènes trouvés dans la lignée sélectionnée, et en combien de copies, similairement à BUSCO :

| | Sur toutes les espèces proches | <i>Cynara cardunculus</i> | <i>Centaurea solstitialis</i> |
| :- | :-: | :-: | :-: |
Unique | 7 686 (68.64%) | 7 707 (68.82%) | 7 676 (68.55%)
Dupliqué | 3 096 (27.65%) | 3 090 (27.59%) | 3 080 (27.5%)
Dupliqué attendu | 2 019 (18.03%) | 1 079 (9.64%) | 2 025 (18.08%)
Dupliqué inattendu | 1 077 (9.62%) | 2 011 (17.96%) | 1 055 (9.42%)
Manquant | 416 (3.71%) | 401 (3.58%) | 442 (3.95%)
Total / 11 198 | 10 782 (96.29%) | 10 797 (96.42%) | 10 756 (96.05%)


Le deuxième tableau obtenu permet de voir les gènes retrouvés dans des espèces appartenant à la lignée sélectionnée (Consistent), ceux retrouvés dans des espèces non incluses dans la lignée (Inconsistent), et enfin ceux n'ayant pas été trouvé dans la base de données OMA (Inconnu) :

| | Sur toutes les espèces proches | <i>Cynara cardunculus</i> | <i>Centaurea solstitialis</i> |
| :- | :-: | :-: | :-: |
Consistent | 37 959 (71.31%) | 30 469 (89.5%) | 36 975 (71.77%)
Consistent partiel | 7 493 (14.08%) | 4 469 (13.13%) | 7 312 (14.19%)
Consistent fragmenté | 3 641 (6.84%) | 2 159 (6.34%) | 3 665 (7.11%)
Inconsistent | 3 131 (5.88%) | 1 204 (3.54%) | 3 041 (5.90%)
Inconsistent partiel | 1 911 (3.59%) | 684 (2.01%) | 1 910 (3.71%)
Inconsistent fragmenté | 287 (0.54%) | 158 (0.46%) | 296 (0.57%)
Inconnu | 12 139 (22.81%) | 2 369 (6.96%) | 11 503 (22.33%)

<!-- #region -->
L'espèce chez laquelle le plus de gènes ont été retrouvés est <b><i>Cynara cardunculus</i></b> pour toutes les annotations :

| | Sur toutes les espèces proches | <i>Cynara cardunculus</i> | <i>Centaurea solstitialis</i> |
| :- | :-: | :-: | :-: |
Espèce détectée : <i>Cynara cardunculus</i> | 41 090 (77.19%) | 31 673 (93.04%) | 40 016 (77.67%)


Cela est certainement dû au fait qu'OMA ne contient que 11 espèces dans son clade des Astéridées, et <i>Cynara cardunculus</i> est la seule de ces espèces à être assez proche de notre Centaurée.
<!-- #endregion -->
