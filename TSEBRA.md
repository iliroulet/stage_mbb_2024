# Sommaire


- [Lancement](#commandes)

- [Résultats](#fichiers-obtenus)

    - [Statistiques basiques](#fichiers-obtenus-1)
    
    - [Statistiques avec gFACs](#fichiers-obtenus-2)
    
    - [Analyse avec BUSCO](#fichiers-obtenus-3)
    
    - [Analyse avec OMArk](#fichiers-obtenus-4)


# TSEBRA


TSEBRA a été lancé sur les annotations [BRAKER1](BRAKER1.md) et [BRAKER2](BRAKER2.md) qui paraissaient les plus prometteuses, ici l'annotation BRAKER1 sur <i>Centaurea jacea</i> (alignement customisé) et l'annotation BRAKER2 sur les protéines des Eudicotylédons et de <i>Centaurea solstitialis</i>.


## Annotation


##### Documentation
https://github.com/Gaius-Augustus/TSEBRA


##### Commandes

```python
/Data/tools/TSEBRA/bin/tsebra.py \
	-k /Results/structural_annotation/BRAKER2_orthoDB_C_sol/braker.gtf \
	-g /Results/structural_annotation/BRAKER1_custom_align/BRAKER1_Centaurea_jacea/braker.gtf \
	-e /Results/structural_annotation/BRAKER2_orthoDB_C_sol/hintsfile.gff,/Results/structural_annotation/BRAKER1_custom_align/BRAKER1_Centaurea_jacea/hintsfile.gff \
	-o /Results/structural_annotation/TSEBRA_B1_B2/tsebra_b1_b2.gtf \
	2> /Results/structural_annotation/TSEBRA_B1_B2/tsebra_b1_b2.log
# on fait un fichier où les gènes sont renommés pour pouvoir faire nos analyses dessus
/Data/tools/TSEBRA/bin/rename_gtf.py --gtf tsebra_b1_b2.gtf --out tsebra_b1_b2_renamed.gtf
# obtenir les fichiers .aa et .codingseq grâce au script d'Augustus
/Data/tools/Augustus/scripts/getAnnoFastaFromJoingenes.py -g /Data/genomes/masked_genome/centaurea_genome.fasta.masked \
	-o /Results/structural_annotation/TSEBRA_B1_B2/tsebra_b1_b2_renamed -f /Results/structural_annotation/TSEBRA_B1_B2/tsebra_b1_b2_renamed.gtf
```

```python
# on transforme l'annotation renommée en format GFF3 pour pouvoir l'utiliser plus tard avec funannotate
cat tsebra_b1_b2_renamed.gtf | perl -ne 'if(m/\tAUGUSTUS\t/ or m/\tGeneMark\.hmm\t/ or m/\tGeneMark\.hmm3\t/ or m/\tgmst\t/) {print $_;}' | /Data/tools/Augustus/scripts/gtf2gff.pl --gff3 --out=tsebra_b1_b2_renamed.gff3
```

##### Fichiers obtenus


- tsebra_b1_b2.gtf : annotation originelle
- tsebra_b1_b2.log : log du run TSEBRA
- tsebra_b1_b2_renamed.aa : séquences protéiques de l'annotation (renommée)
- tsebra_b1_b2_renamed.codingseq : séquences codantes de l'annotation (renommée)
- tsebra_b1_b2_renamed.gff3 : annotation (renommée) en format GFF3
- tsebra_b1_b2_renamed.gtf : annotation "renommée" (les gènes ont été renommées pour suivre la convention de sortie BRAKER)


## Analyse


### Statistiques de base


##### Commandes

```python
grep -c ">" tsebra_b1_b2_renamed.aa > nb_of_prot_seq.txt
```

```python
# Ploter l'histogramme du nombre d'exons trouvés selon leur taille
python3 /Data/scripts/exons_len_plot.py tsebra_b1_b2_renamed.gtf .
# Ploter l'histogramme du nombre de gènes trouvés selon leur taille
python3 /Data/scripts/gene_len_plot.py tsebra_b1_b2_renamed.gtf .
# Ploter l'histogramme du nombre de transcrits trouvés selon leur taille
python3 /Data/scripts/transcripts_len_plot.py tsebra_b1_b2_renamed.gtf .
# Ploter l'histogramme du nombre de gènes selon leur nombre d'exons (moyenne sur tous les transcrits)
python3 /Data/scripts/exons_mean_nbr_plot_stat.py tsebra_b1_b2_renamed.gtf .
# Ploter le scatter plot de la taille de gènes par leur nombre moyen d'exons
python3 /Data/scripts/gene_len_by_mean_exons.py tsebra_b1_b2_renamed.gtf .
```

```python
mkdir plots
mv *.png plots
mkdir stats
mv *n*.txt stats
```

##### Fichiers obtenus


- exons_length_histogram.png : histogramme du nombre d'exons trouvés selon leur taille
- exons_mean_nbr_histogram.png : histogramme du nombre de gènes selon leur nombre d'exons (moyenne sur tous les transcrits trouvés pour le gène)
- exons_mean_nbr_stat.txt : statistiques sur les exons par gènes (moyenne sur tous les transcrits)
- gene_len_by_mean_exons_scatterplot.png : scatter plot de la taille de gènes par leur nombre moyen d'exons
- gene_length_histogram.png : histogramme du nombre de gènes trouvés selon leur taille
- nb_of_prot_seq.txt : nombre de séquences protéiques extraites de l'annotation
- transcripts_length_histogram.png : histogramme du nombre de transcrits trouvés selon leur taille


Ces résultats ont été synthétisés en tableaux :

| | BRAKER1 <i>C. jacea</i> et BRAKER2 Eudicotylédons | BRAKER1 <i>C. jacea</i> et BRAKER2 Eudicotylédons + <i>C. solstitialis</i> | BRAKER1 <i>C. jacea</i> et BRAKER2 Eudicotylédons + <i>C. cardunculus</i>
| :- | :-: | :-: | :-: |
Nombre de protéines trouvées | 61 275 | 65 902 | 61 790

<br>

| | BRAKER1 <i>C. jacea</i> et BRAKER2 Eudicotylédons | BRAKER1 <i>C. jacea</i> et BRAKER2 Eudicotylédons + <i>C. solstitialis</i> | BRAKER1 <i>C. jacea</i> et BRAKER2 Eudicotylédons + <i>C. cardunculus</i>
| :- | :-: | :-: | :-: |
Nombre de gènes | 51 823 | 54 296 | 52 061
Nombre de gènes mono-ex. | 16 467 | 17 988 | 16 693
Nombre de gènes multi-ex. | 35 356 | 36 308 | 35 368
Ratio mono/multi | 0.47 | 0.5 | 0.47
Nombre médian d’exons | 2 | 2 | 2
Nombre maximum d’exons | 151 | 399 | 76


### gFACs


L'outil gFACs permet de manipuler des fichiers d'annotation et notamment de produire des statistiques sur le contenu des fichiers.


##### Documentation
https://gfacs.readthedocs.io/en/latest/


##### Commandes


gFACs a une option format (-f) pour renseigner le format du fichier GTF donné en entrée. Pour savoir le format à renseigner, on peut utiliser le script de support format_diagnosis.pl (https://gfacs.readthedocs.io/en/latest/Bonus/format_diagnosis.html). Ce script renvoie une suite de valeurs que l'on peut ensuite aller comparer au tableau de formats de la documentation gFACs (https://gfacs.readthedocs.io/en/latest/_images/Table.png) pour déterminer quel format gFACs correspond au nôtre, ou au moins lequel en est le plus proche.

```python
cd /Results/structural_annotation/TSEBRA_B1_B2
perl /opt/biotools/conda/envs/gFACs/opt/perl-gfacs-1.1.1/support_scripts/format_diagnosis.pl tsebra_b1_b2_renamed.gtf
```

```python
mkdir gFACs
mamba activate gFACs
gFACs.pl -f braker_2.0_gtf --statistics --fasta /Data/genomes/original_genome/centaurea_genome.fasta --splice-table --nt-content \
	--get-fasta --get-protein-fasta --create-gff3 --distributions exons_lengths intron_lengths CDS_lengths gene_lengths \
	exon_position exon_position_data intron_position intron_position_data -O gFACs tsebra_b1_b2_renamed.gtf
```

##### Fichiers obtenus


- CDS_lengths_distributions.tsv : distribution des longueurs des séquences codantes
- data_exon_position_distributions.tsv : liste des positions des exons ?
- data_intron_position_distributions.tsv : liste des positions des exons ?
- exon_position_distributions.tsv : distribution de la position des exons
- gene_lengths_distributions.tsv : distribution des longueurs des gènes
- genes.fasta : liste des gènes utilisés par gFACs pour ses statistiques
- genes.fasta.faa : liste des gènes utilisés par gFACs pour ses statistiques accompagnés de certaines séquences protéiques
- gene_table.txt : table de gènes crées par gFACs pour ses statistiques
- gFACs_log.txt : fichier de log
- intron_lengths_distributions.tsv : distribution des longueurs des introns
- intron_position_distributions.tsv : distribution de la position des introns
- out.gff3 : fichier contenant les éléments étudiés par gFACs (mRNA, exons, ...)
- statistics.txt : statistiques produites par gFACs


Le résumé des statistiques obtenues se trouve dans deux tableaux de synthèse :

![1er tableau de synthèse de gFACs](images/gFACs/TSEBRA_gFACs_1.svg)

![2ème tableau de synthèse de gFACs](images/gFACs/TSEBRA_gFACs_2.svg)


### BUSCO


L'outil BUSCO permet de faire une recherche des séquences codantes trouvées dans la base de données orthoDB (https://www.orthodb.org/) d'une lignée afin de voir si les gènes prédits y sont retrouvés. La lignée sélectionnée ici est la lignée eudicots_odb10.


##### Documentation
https://busco.ezlab.org/busco_userguide.html


##### Commandes


Pour lancer BUSCO en mode transcriptome, il faut d'abord faire en sorte de ne sélectionner qu'un isoforme (= transcrit) par gène. On utilise donc AGAT pour ne garder que les plus isoformes aux plus longs CDS.

```python
cd /Results/structural_annotation/TSEBRA_B1_B2/$combinaison
mamba activate agat
mkdir without_isoforms
agat_sp_keep_longest_isoform.pl -gff tsebra_b1_b2_renamed.gtf -o without_isoforms/agat_without_isoforms.gtf
```

Il faut ensuite reformater le fichier produit par AGAT pour qu'il corresponde à un format Augustus, ce qui nous permettra de lancer ensuite le script d'Augustus permettat d'obtenir les séquences codantes et protéiques d'une annotation.

```python
cd without_isoforms
python3 /Data/scripts/reformat_agat_gtf.py agat_without_isoforms.gtf without_isoforms.gtf
/Data/tools/Augustus/scripts/getAnnoFastaFromJoingenes.py -g /Data/genomes/masked_genome/centaurea_genome.fasta.masked \
	-o without_isoforms -f without_isoforms.gtf
```

Le script produit deux fichiers : without_isoforms.aa et without_isoforms.codingseq.

On peut enfin lancer BUSCO sur le fichier .codingseq obtenu.

```python
cd ..
mamba activate busco
busco -m transcriptome -i without_isoforms/without_isoforms.codingseq -o busco_results -l eudicots_odb10 -c 8
```

##### Fichiers obtenus


- busco.log : log de la commande busco
- logs : dossier des logs de tous les processus de busco
- run_eudicots_odb10 : dossier contenant les séquences trouvées par busco en format FASTA et GFF, les identifiants des séquences manquantes, et l'output de hmmer et metaeuk
- short_summary.specific.eudicots_odb10.busco_results.json : résumé du résultat busco
- short_summary.specific.eudicots_odb10.busco_results.txt : résumé du résultat busco


##### Synthèse des résultats


| | BRAKER1 <i>C. jacea</i> et BRAKER2 Eudicotylédons | BRAKER1 <i>C. jacea</i> et BRAKER2 Eudicotylédons + <i>C. solstitialis</i> | BRAKER1 <i>C. jacea</i> et BRAKER2 Eudicotylédons + <i>C. cardunculus</i>
| :- | :-: | :-: | :-: |
Complet | 2 225 (95.6%) | 2 226 (95.7%) | 2 227 (95.7%)
Complet unique | 2 078 (89.3%) | 2 077 (89.3%) | 2080 (89.4%)
Complet dupliqué | 147 (6.3%) | 149 (6.4%) | 147 (6.3%)
Fragmenté | 18 (0.8%) | 16 (0.7%) | 16 (0.7%)
Manquant | 83 (3.6%) | 84 (3.6%) | 83 (3.6%)
Total / 2326 | 2243 (96.4%) | 2242 (96.4%) | 2243 (96.4%)


### OMArk


L'outil OMArk permet de faire une recherche des séquences protéiques trouvées dans la base de données OMA (https://omabrowser.org/oma/home/) d'une lignée afin de voir si les gènes prédits y sont retrouvés. La lignée sélectionnée ici est la lignée Asteranae (clade). Cette lignée comprend 11 espèces :
- <i>Actinidia chinensis var. chinensis</i>
- <i>Capsicum annuum</i>
- <i>Coffea canephora</i>
- <i>Cynara cardunculus var. scolymus</i>
- <i>Daucus carota subsp. sativus</i>
- <i>Erythranthe guttata</i>
- <i>Helianthus annuus</i>
- <i>Nicotiana attenuata</i>
- <i>Nicotiana tabacum</i>
- <i>Solanum lycopersicum</i>
- <i>Solanum tuberosum</i>


##### Documentation
https://github.com/DessimozLab/OMArk/tree/main

https://github.com/DessimozLab/omamer


##### Commandes


Il faut d'abord faire une recherche dans la base OMA (ici <i>Viridiplantae</i>) avec l'outil OMAmer, puis soumettre le résultats de cette recherche à OMArk.

```python
# Pour télécharger la base de données utilisée ici (si besoin) :
cd /Data/dbs/OMA
wget https://omabrowser.org/All/Viridiplantae.h5
```

```python
cd /Results/structural_annotation/TSEBRA_B1_B2/$combination
mamba activate OMArk
mkdir OMAmer-OMArk
omamer search -d /Data/dbs/OMA/Viridiplantae.h5 -q tsebra_b1_b2_renamed.aa -o OMAmer-OMArk/omamer.out -t 8
```

OMArk utilise les protéines générées depuis l'annotation et n'en sélectionne qu'une par gène, il faut donc lui soumettre la liste des transcrits pour chaque gène pour qu'il choisisse l'un de ces isoformes.

```python
# On crée un fichier .splice contenant les isoformes
python3 /Data/scripts/make_splice_file.py tsebra_b1_b2_renamed.aa
```

```python
cd OMAmer-OMArk
# On donne à omark le rang taxonomique le plus proche (parmi la liste des rangs acceptés) avec l'option -r,
# et l'identifiant taxonomique de ce rang pour notre espèce avec l'option -t
omark -f omamer.out -d /Data/dbs/OMA/Viridiplantae.h5 -o omark -r family -t 4210 -of ../tsebra_b1_b2_renamed.aa --isoform_file ../tsebra_b1_b2_renamed.aa.splice
```

##### Fichiers obtenus


- omamer_Cynara_cardunculus_var_scolymus.fasta : liste des protéines ayant été retrouvés dans la base de <i>Cynara cardunculus</i>
- omamer_detailed_summary.txt : résumé détaillé des résultats
- omamer_mapped.fasta : liste des protéines retrouvées dans la lignée donnée
- omamer_misplaced.fasta : liste des protéines retrouvées dans d'autres lignées que la lignée donnée
- omamer_no_hits.fasta :  : liste des protéines non-retrouvées dans la base de données
- omamer.omq : liste des protéines uniques retrouvées
- omamer.pdf : histogramme des pourcentages de protéines selon leur catégorie
- omamer.png : histogramme des pourcentages de protéines selon leur catégorie
- omamer_selected_isoforms.txt : liste des isoformes (= transcrits) choisis pour chaque gène par omark
- omamer.sum : résumé simplifié des résultats
- omamer.tax : rappel de la lignée et clade sélectionnés
- omamer.ump : liste des protéines complètes retrouvés dans la lignée

```python
less omamer.sum
```

```python
#The selected clade was asterids
#Number of conserved HOGs is: 11198
#Results on conserved HOGs is:
#S:Single:S, D:Duplicated[U:Unexpected,E:Expected],M:Missing
S:6791,D:4007[U:2015,E:1992],M:400
S:60.64%,D:35.78%[U:17.99%,E:17.79%],M:3.57%
#On the whole proteome, there are 60540 proteins
#Of which:
#A:Consistent (taxonomically)[P:Partial hits,F:Fragmented], I: Inconsistent (taxonomically)[P:Partial hits,F:Fragmented], C: Likely Contamination[P:Partial hits,F:Fragmented], U: Unknown 
A:39649[P:7111,F:3243],I:2707[P:1822,F:260],C:0[P:0,F:0],U:18184
A:65.49%[P:11.75%,F:5.36%],I:4.47%[P:3.01%,F:0.43%],C:0.00%[P:0.00%,F:0.00%],U:30.04%
#From HOG placement, the detected species are:
#Clade  NCBI taxid      Number of associated proteins   Percentage of proteome's total
Cynara cardunculus var. scolymus        59895   42356   69.96%
```

##### Synthèse des résultats


Pour rappel, OMArk ne sélectionne qu'un seul isoforme par gène : il ne prend donc pas en compte toutes les protéines fournies par nos annotations.

Le nombre de protéines prises en compte dans nos annotations par OMArk est :

| | BRAKER1 <i>C. jacea</i> et BRAKER2 Eudicotylédons | BRAKER1 <i>C. jacea</i> et BRAKER2 Eudicotylédons + <i>C. solstitialis</i> | BRAKER1 <i>C. jacea</i> et BRAKER2 Eudicotylédons + <i>C. cardunculus</i>
| :- | :-: | :-: | :-: |
Nombre total de protéines | 51 823 | 54 296 | 52 061


Le premier tableau permet de voir les gènes trouvés dans la lignée sélectionnée, et en combien de copies, similairement à BUSCO :

| | BRAKER1 <i>C. jacea</i> et BRAKER2 Eudicotylédons | BRAKER1 <i>C. jacea</i> et BRAKER2 Eudicotylédons + <i>C. solstitialis</i> | BRAKER1 <i>C. jacea</i> et BRAKER2 Eudicotylédons + <i>C. cardunculus</i>
| :- | :-: | :-: | :-: |
Unique | 7 570 (67.6%) | 7 566 (67.57%) | 7 577 (67.66%)
Dupliqué | 3 233 (28.87%) | 3 234 (28.88%) | 3 230 (28.84%)
Dupliqué attendu | 2 002 (17.88%) | 2 016 (18%) | 2 002 (17.88%)
Dupliqué inattendu | 1 2321 (10.99%) | 1 218 (10.88%) | 1 228 (10.97%)
Manquant | 395 (3.53%) | 398 (3.55%) | 391 (3.49%)
Total / 11 198 | 10 803 (96.47%) | 10 800 (96.45%) | 10 807 (96.51%)

<!-- #region -->
Le deuxième tableau obtenu permet de voir les gènes retrouvés dans des espèces appartenant à la lignée sélectionnée (Consistent), ceux retrouvés dans des espèces non incluses dans la lignée (Inconsistent), et enfin ceux n'ayant pas été trouvé dans la base de données OMA (Inconnu) :


| | BRAKER1 <i>C. jacea</i> et BRAKER2 Eudicotylédons | BRAKER1 <i>C. jacea</i> et BRAKER2 Eudicotylédons + <i>C. solstitialis</i> | BRAKER1 <i>C. jacea</i> et BRAKER2 Eudicotylédons + <i>C. cardunculus</i>
| :- | :-: | :-: | :-: |
Consistent | 33 532 (64.7%) | 35 005 (64.47%) | 33 649 (64.63%)
Consistent partiel | 5 772 (11.14%) | 6 365 (11.72%) | 5 701 (10.95%)
Consistent fragmenté | 2 384 (4.6%) | 2 798 (5.15%) | 2 579 (4.95%)
Inconsistent | 2 364 (4.56%) | 2 536 (4.67%) | 2 365 (4.54%)
Inconsistent partiel | 1 553 (3%) | 1 708 (3.15%) | 1 547 (2.97%)
Inconsistent fragmenté | 245 (0.47%) | 239 (0.44%) | 259 (0.5%)
Inconnu | 15 927 (30.73%) | 16 755 (30.86%) | 16 047 (30.82%)
<!-- #endregion -->

L'espèce chez laquelle le plus de gènes ont été retrouvés est <b><i>Cynara cardunculus</i></b> pour toutes les annotations :

| | BRAKER1 <i>C. jacea</i> et BRAKER2 Eudicotylédons | BRAKER1 <i>C. jacea</i> et BRAKER2 Eudicotylédons + <i>C. solstitialis</i> | BRAKER1 <i>C. jacea</i> et BRAKER2 Eudicotylédons + <i>C. cardunculus</i>
| :- | :-: | :-: | :-: |
Espèce détectée : <i>Cynara cardunculus</i> | 35 896 (69.27%) | 37 541 (69.14%) | 36 014 (69.18%)

Cela est certainement dû au fait qu'OMA ne contient que 11 espèces dans son clade des Astéridées, et <i>Cynara cardunculus</i> est la seule de ces espèces à être assez proche de notre Centaurée.
