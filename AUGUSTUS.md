# Sommaire


- [Lancement](#commandes)

- [Résultats](#fichiers-obtenus)

    - [Statistiques basiques](#fichiers-obtenus-1)
    
    - [Statistiques avec gFACs](#fichiers-obtenus-2)
    
    - [Analyse avec BUSCO](#fichiers-obtenus-3)
    
    - [Analyse avec OMArk](#fichiers-obtenus-4)


# AUGUSTUS


AUGUSTUSa été lancé en mode <i>ab initio</i> (sans données extrinsèques) avec les paramètres statistiques de la tomate (espèce la plus proche de la Centaurée parmi les espèces disponibles dans augustus).


## Annotation


##### Documentation
https://github.com/Gaius-Augustus/Augustus


##### Commandes

```python
cd /Results/structural_annotation/augustus_ab_initio
augustus /Data/genomes/masked_genome/centaurea_genome.fasta.masked --species=tomato -o augustus_output
```

```python
# on transforme l'annotation en format GFF3 pour pouvoir l'utiliser plus tard avec funannotate
cat augustus_output.gff | perl -ne 'if(m/\tAUGUSTUS\t/ or m/\tGeneMark\.hmm\t/ or m/\tGeneMark\.hmm3\t/ or m/\tgmst\t/) {print $_;}' | /Data/tools/Augustus/scripts/gtf2gff.pl --gff3 --out=augustus_output.gff3
```

##### Fichiers obtenus


- augustus_output.aa : séquences protéiques de l'annotation
- augustus_output.codingseq : séquences codantes de l'annotation
- augustus_output.gff : annotation


## Analyse


### Statistiques de base


##### Commandes

```python
grep -c ">" augustus_output.aa > nb_of_prot_seq.txt
```

```python
# Ploter l'histogramme du nombre d'exons trouvés selon leur taille
python3 /Data/scripts/exons_len_plot.py augustus_output.gff .
# Ploter l'histogramme du nombre de gènes trouvés selon leur taille
python3 /Data/scripts/gene_len_plot.py augustus_output.gff .
# Ploter l'histogramme du nombre de transcrits trouvés selon leur taille
python3 /Data/scripts/transcripts_len_plot.py augustus_output.gff .
# Ploter l'histogramme du nombre de gènes selon leur nombre d'exons (moyenne sur tous les transcrits)
python3 /Data/scripts/exons_mean_nbr_plot_stat.py augustus_output.gff .
# Ploter le scatter plot de la taille de gènes par leur nombre moyen d'exons
python3 /Data/scripts/gene_len_by_mean_exons.py augustus_output.gff .
```

```python
mkdir plots
mv *.png plots
mkdir stats
mv *n*.txt stats
```

##### Fichiers obtenus


- exons_length_histogram.png : histogramme du nombre d'exons trouvés selon leur taille
- exons_mean_nbr_histogram.png : histogramme du nombre de gènes selon leur nombre d'exons (moyenne sur tous les transcrits trouvés pour le gène)
- exons_mean_nbr_stat.txt : statistiques sur les exons par gènes (moyenne sur tous les transcrits)
- gene_len_by_mean_exons_scatterplot.png : scatter plot de la taille de gènes par leur nombre moyen d'exons
- gene_length_histogram.png : histogramme du nombre de gènes trouvés selon leur taille
- nb_of_prot_seq.txt : nombre de séquences protéiques extraites de l'annotation
- transcripts_length_histogram.png : histogramme du nombre de transcrits trouvés selon leur taille


Ces résultats ont été regroupés dans un tableau :

| Nombre de protéines | | Nombre de gènes | Nombre de gènes mono-exoniques | Nombre de gènes multi-exoniques | Ratio mono/multi | Nombre médian d’exons | Nombre maximum d’exons
| :-: | :-: | :-: | :-: | :-: | :-: | :-: | :-: |
| 31 349 | | 31 349 | 4 482 | 26 867 | 0.17 | 4 | 418


### gFACs


L'outil gFACs permet de manipuler des fichiers d'annotation et notamment de produire des statistiques sur le contenu des fichiers.


##### Documentation
https://gfacs.readthedocs.io/en/latest/


##### Commandes


gFACs a une option format (-f) pour renseigner le format du fichier GTF donné en entrée. Pour savoir le format à renseigner, on peut utiliser le script de support format_diagnosis.pl (https://gfacs.readthedocs.io/en/latest/Bonus/format_diagnosis.html). Ce script renvoie une suite de valeurs que l'on peut ensuite aller comparer au tableau de formats de la documentation gFACs (https://gfacs.readthedocs.io/en/latest/_images/Table.png) pour déterminer quel format gFACs correspond au nôtre, ou au moins lequel en est le plus proche.

```python
cd /Results/structural_annotation/augustus_ab_initio
perl /opt/biotools/conda/envs/gFACs/opt/perl-gfacs-1.1.1/support_scripts/format_diagnosis.pl augustus_output.gff
```

Attention : le format AUGUSTUSne correspond exactement à aucun format proposé par gFACs, nous avons donc utilisé le format qui semblait le plus similaire : braker_2.0_gtf.

```python
mkdir gFACs
mamba activate gFACs
gFACs.pl -f braker_2.0_gtf --statistics --fasta /Data/genomes/original_genome/centaurea_genome.fasta --splice-table --nt-content \
	--get-fasta --get-protein-fasta --create-gff3 --distributions exons_lengths intron_lengths CDS_lengths gene_lengths \
	exon_position exon_position_data intron_position intron_position_data -O gFACs augustus_output.gff
```

##### Fichiers obtenus


- CDS_lengths_distributions.tsv : distribution des longueurs des séquences codantes
- data_exon_position_distributions.tsv : liste des positions des exons ?
- data_intron_position_distributions.tsv : liste des positions des exons ?
- exon_position_distributions.tsv : distribution de la position des exons
- gene_lengths_distributions.tsv : distribution des longueurs des gènes
- genes.fasta : liste des gènes utilisés par gFACs pour ses statistiques
- genes.fasta.faa : liste des gènes utilisés par gFACs pour ses statistiques accompagnés de certaines séquences protéiques
- gene_table.txt : table de gènes crées par gFACs pour ses statistiques
- gFACs_log.txt : fichier de log
- intron_lengths_distributions.tsv : distribution des longueurs des introns
- intron_position_distributions.tsv : distribution de la position des introns
- out.gff3 : fichier contenant les éléments étudiés par gFACs (mRNA, exons, ...)
- statistics.txt : statistiques produites par gFACs


Le résumé des statistiques obtenues se trouve dans un tableau de synthèse :

![Tableau de synthèse de gFACs](images/gFACs/AUGUSTUS_gFACs.svg)


### BUSCO


L'outil BUSCO permet de faire une recherche des séquences codantes trouvées dans la base de données orthoDB (https://www.orthodb.org/) d'une lignée afin de voir si les gènes prédits y sont retrouvés. La lignée sélectionnée ici est la lignée eudicots_odb10.


##### Documentation
https://busco.ezlab.org/busco_userguide.html


##### Commandes


Pour lancer BUSCO en mode transcriptome, il faut d'abord faire en sorte de ne sélectionner qu'un isoforme (= transcrit) par gène. Le fichier d'annotation produit par augustus ne contenant déjà qu'un isoforme par gène, il n'est pas nécessaire de s'en préoccuper pour l'instant.


On peut donc directement lancer BUSCO sur le fichier d'augustus contenant les séquences codantes.

```python
mamba activate busco
busco -m transcriptome -i augustus_output.codingseq -o busco_results -l eudicots_odb10 -c 8
```

##### Fichiers obtenus


- busco.log : log de la commande busco
- logs : dossier des logs de tous les processus de busco
- run_eudicots_odb10 : dossier contenant les séquences trouvées par busco en format FASTA et GFF, les identifiants des séquences manquantes, et l'output de hmmer et metaeuk
- short_summary.specific.eudicots_odb10.busco_results.json : résumé du résultat busco
- short_summary.specific.eudicots_odb10.busco_results.txt : résumé du résultat busco

```python
less short_summary.specific.eudicots_odb10.busco_results.txt
```

```python
# BUSCO version is: 5.7.1 
# The lineage dataset is: eudicots_odb10 (Creation date: 2024-01-08, number of genomes: 31, number of BUSCOs: 2326)
# Summarized benchmarking in BUSCO notation for file /Results/structural_annotation/augustus_ab_initio/augustus_output.codingseq
# BUSCO was run in mode: euk_tran

        ***** Results: *****

        C:87.3%[S:81.3%,D:6.0%],F:4.8%,M:7.9%,n:2326       
        2030    Complete BUSCOs (C)                        
        1890    Complete and single-copy BUSCOs (S)        
        140     Complete and duplicated BUSCOs (D)         
        111     Fragmented BUSCOs (F)                      
        185     Missing BUSCOs (M)                         
        2326    Total BUSCO groups searched
```

##### Synthèse des résultats


| | Complet | Complet unique | Complet dupliqué | Fragmenté | Manquant | Total
| :- | :-: | :-: | :-: | :-: | :-: | :-: |
Nombre et pourcentage de gènes (sur 2 326 gènes compris dans la lignée) | 2 030 (87.3%) | 1 890 (81.3%) | 140 (6%) | 111 (4.8%) | 185 (7.9%) | 2 141 (92.1%)


### OMArk


L'outil OMArk permet de faire une recherche des séquences protéiques trouvées dans la base de données OMA (https://omabrowser.org/oma/home/) d'une lignée afin de voir si les gènes prédits y sont retrouvés. La lignée sélectionnée ici est la lignée Asteranae (clade). Cette lignée comprend 11 espèces :
- <i>Actinidia chinensis var. chinensis</i>
- <i>Capsicum annuum</i>
- <i>Coffea canephora</i>
- <i>Cynara cardunculus var. scolymus</i>
- <i>Daucus carota subsp. sativus</i>
- <i>Erythranthe guttata</i>
- <i>Helianthus annuus</i>
- <i>Nicotiana attenuata</i>
- <i>Nicotiana tabacum</i>
- <i>Solanum lycopersicum</i>
- <i>Solanum tuberosum</i>


##### Documentation
https://github.com/DessimozLab/OMArk/tree/main

https://github.com/DessimozLab/omamer


##### Commandes


Il faut d'abord faire une recherche dans la base OMA (ici <i>Viridiplantae</i>) avec l'outil OMAmer, puis soumettre le résultats de cette recherche à OMArk.

```python
# Pour télécharger la base de données utilisée ici (si besoin) :
cd /Data/dbs/OMA
wget https://omabrowser.org/All/Viridiplantae.h5
```

```python
cd /Results/structural_annotation/augustus_ab_initio
mamba activate OMArk
mkdir OMAmer-OMArk
omamer search -d /Data/dbs/OMA/Viridiplantae.h5 -q augustus_output.aa -o OMAmer-OMArk/omamer.out -t 8
```

OMArk utilise les protéines générées depuis l'annotation et n'en sélectionne qu'une par gène, il faut donc normalement lui soumettre la liste des transcrits pour chaque gène pour qu'il choisisse l'un de ces isoformes. AUGUSTUSne fournissant qu'un seul isoforme par gène, il n'y a pas besoin de le faire ici.

```python
cd OMAmer-OMArk
# On donne à omark le rang taxonomique le plus proche (parmi la liste des rangs acceptés) avec l'option -r,
# et l'identifiant taxonomique de ce rang pour notre espèce avec l'option -t
omark -f omamer.out -d /Data/dbs/OMA/Viridiplantae.h5 -o omark -r family -t 4210 -of ../augustus_output.aa
```

##### Fichiers obtenus


- omamer_Cynara_cardunculus_var_scolymus.fasta : liste des protéines ayant été retrouvés dans la base de <i>Cynara cardunculus</i>
- omamer_detailed_summary.txt : résumé détaillé des résultats
- omamer_mapped.fasta : liste des protéines retrouvées dans la lignée donnée
- omamer_misplaced.fasta : liste des protéines retrouvées dans d'autres lignées que la lignée donnée
- omamer_no_hits.fasta :  : liste des protéines non-retrouvées dans la base de données
- omamer.omq : liste des protéines uniques retrouvées
- omamer.pdf : histogramme des pourcentages de protéines selon leur catégorie
- omamer.png : histogramme des pourcentages de protéines selon leur catégorie
- omamer_selected_isoforms.txt : liste des isoformes (= transcrits) choisis pour chaque gène par omark
- omamer.sum : résumé simplifié des résultats
- omamer.tax : rappel de la lignée et clade sélectionnés
- omamer.ump : liste des protéines complètes retrouvés dans la lignée

```python
less omamer.sum
```

```python
#The selected clade was asterids
#Number of conserved HOGs is: 11198
#Results on conserved HOGs is:
#S:Single:S, D:Duplicated[U:Unexpected,E:Expected],M:Missing
S:7522,D:2314[U:849,E:1465],M:1362
S:67.17%,D:20.66%[U:7.58%,E:13.08%],M:12.16%
#On the whole proteome, there are 31349 proteins
#Of which:
#A:Consistent (taxonomically)[P:Partial hits,F:Fragmented], I: Inconsistent (taxonomically)[P:Partial hits,F:Fragmented], C: Likely Contamination[P:Partial hits,F:Fragmented], U: Unknown 
A:24771[P:6045,F:970],I:1159[P:831,F:54],C:0[P:0,F:0],U:5419
A:79.02%[P:19.28%,F:3.09%],I:3.70%[P:2.65%,F:0.17%],C:0.00%[P:0.00%,F:0.00%],U:17.29%
#From HOG placement, the detected species are:
#Clade  NCBI taxid      Number of associated proteins   Percentage of proteome's total
Cynara cardunculus var. scolymus        59895   25930   82.71%
```

##### Synthèse des résultats


Le premier tableau permet de voir les gènes trouvés dans la lignée sélectionnée, et en combien de copies, similairement à BUSCO :

| | Unique | Dupliqué | Dupliqué attendu | Dupliqué inattendu | Manquant | Total
| :- | :-: | :-: | :-: | :-: | :-: | :-: |
Nombre et pourcentage de gènes (sur 11 198 gènes compris dans la lignée) | 7 522 (67.17%) | 2 314 (20.66%) | 1 465 (13.08%) | 849 (7.58%) | 1 362 (12.16%) | 9 836 (87.84%)


Le deuxième tableau obtenu permet de voir les gènes retrouvés dans des espèces appartenant à la lignée sélectionnée (Consistent), ceux retrouvés dans des espèces non incluses dans la lignée (Inconsistent), et enfin ceux n'ayant pas été trouvé dans la base de données OMA (Inconnu) :

| | Consistent | Consistent partiel | Consistent fragmenté | Inconsistent | Inconsistent partiel | Inconsistent fragmenté | Inconnu
| :- | :-: | :-: | :-: | :-: | :-: | :-: | :-:
Nombre et pourcentage de gènes (sur 31 349 protéines sélectionnées dans l'annotation) | 24 771 (79.02%)| 6 045 (19.28%) | 970 (3.09%) | 1 159 (3.70%) | 831 (2.65%) | 54 (0.17%) | 5 419 (17.29%)


L'espèce chez laquelle le plus de gènes ont été retrouvés est <b><i>Cynara cardunculus</i></b>, avec 25 930 (82.71%) protéines retrouvées.

Cela est certainement dû au fait qu'OMA ne contient que 11 espèces dans son clade des Astéridées, et <i>Cynara cardunculus</i> est la seule de ces espèces à être assez proche de notre Centaurée.
