# Sommaire


- [Visualisation](#visualisation)
- [Recherche des gènes d'intérêt](#recherche-des-gènes-dintérêt)


# Visualisation


Les annotations peuvent être visualisées à l'aide de l'outil JBrowse2 (https://jbrowse.org/jb2/), disponible sur un serveur installé dans un conteneur *docker* sur la machine vengad (adresse IP de vengad : 162.38.181.232 ; adresse du serveur JBrowse : 162.38.181.232:5000).


# Recherche des gènes d'intérêt


Pour rappel, le but du projet est notamment de trouver les gènes impliqués dans les mécanismes de résistance au stress hydrique. 

Ci-dessous des listes de gènes potentiels (source : https://www.ncbi.nlm.nih.gov/pmc/articles/PMC9569452/) :

**Régulés positiviement**

| Gene | Protein | Function
| :- | :-: | :-: |
PUB48 |  Ubiquitin E3 ligase | Protein stability and refolding
CNGC2, CNGC6 | CNGC (cyclic nucleotide gated ion channel) | Ca2+ signal transduction
WRKY39 |  WRKY39 | Salicylic acid (SA) signal regulation
miR156 |  / | Protein stability and refolding
HSP18.0 |  HSP (heat shock protein) | Protein stability and refolding
HIRP1 |  Heat-induced RING finger protein 1 | Protein stability and refolding
DHSRP1 |  Drought, Heat and Salt-induced RING finger protein 1 | Protein stability and refolding
eIF4A1 |  DEAD-box RNA helicase | RNA stability and refolding
TOGR1 |  DEAD-box RNA helicase | RNA stability and refolding
CNGC14, CNGC16 | CNGC | Ca2+ signal transduction
MdATG18 |  Autophagy-related proteins | Photosynthesis
RH22 |  DEAD-box RNA helicase | RNA stability and refolding
CaAPX |  APX (ascorbate peroxidase) | Antioxidant defense
HSFA6B |  HSF | Protein stability and refolding
HSP17, HSP18, HSP21, HSP 22, HSP 23, HSP26, HSP70, HSP83, HSP90, HSF30, and HSC-2 | HSP | Protein stability and refolding
CDPK4 CDPK11 |  CDPK (Calcium-dependent protein kinase) | Ca2+ signal transduction
LlWRKY39 |  WRKY39 | Ca2+ signal transduction
Hsf01 |  HSF | Protein stability and refolding
CDPK7 |  CDPK | Ca2+ signal transduction
CaHsfA1d |  HSF (heat shock factor) | Protein stability and refolding
TCONS_00202587, TCONS_00260893 | / | Antioxidant defense
Pip2-1, pip1-2, tip21 | AQP | Substance transport
SbHCI1 |  Heat- and cold-induced RING finger protein 1 | Protein stability and refolding
TaFBA1 |  Ubiquitin E3 ligase | Protein stability and refolding
TaMYB80 |  MYB | Abscisic acid (ABA) signal regulation
HsfB1 |  HSF | Protein stability and refolding
DEAD31 |  DEAD-box RNA helicase | RNA stability and refolding
JA2 |  Solanum lycopersicum jasmonic acid 2 | Salicylic acid (SA) signal regulation

<br>

**Régulés négativement**

| Gene | Protein | Function
| :- | :-: | :-: |
SUT1 |  Rice sucrose transport protein | Substance transport
miR159 |  / | Hormone regulation
miR396 |  / | Leaf development
Unigene21949_All,Unigene3820_All, Unigene8475_All, Unigene13442_All, Unigene23855_All, and Unigene23780_All | Ethylene receptor | Ethylene signal regulation
Unigene6615_All, Unigene12612_All, Unigene21144_All, and Unigene24054_All | Ethylene response factors (ERFs) | Ethylene signal regulation
CaWAKL20 |  WAK (cell wall-associated protein kinase) | ABA signal regulation
miR159 |  / | Hormone regulation
miR396 |  / | Leaf development
DEAD30 |  DEAD-box RNA helicase | RNA stability and refolding

<br>

Ci-dessous, un schéma détaillant la réponse des plantes au stress hydrique et le rôle qu'y jouent les différents gènes listés précédemment :

![Schéma de la réponse au stress hydrique et les gènes impliqués](images/genes_stress_hydrique.jpg)
**Source : Huang, Ling-Zhi et al. “Gene Networks Involved in Plant Heat Stress Response and Tolerance.” International journal of molecular sciences vol. 23,19 11970. 9 Oct. 2022, doi:10.3390/ijms231911970**


##### Résultats


Ci-dessous, un tableau du nombres de gènes listés qui ont été trouvés dans nos annotations en comptant les gènes en double :

**AUGUSTUS**

| Données utilisées | Nombre de gènes de la liste trouvés
| :- | :-: |
| *ab initio* | 24

**BRAKER1**

| Données utilisées | Nombre de gènes de la liste trouvés
| :- | :-: |
| *Centaurea solstitialis* standard | 27
| *Centaurea solstitialis* customisé | 28
| *Centaurea cyanus* standard | 29
| *Centaurea cyanus* customisé | 28
| *Cynara cardunculus* standard | 27
| *Cynara cardunculus* customisé | 28
| *Centaurea jacea* standard | 31
| *Centaurea jacea* customisé | 30

**BRAKER2**

| Données utilisées | Nombre de gènes de la liste trouvés
| :- | :-: |
| Protéines des espèces proches | 35
| Eudicotylédons | 32
| Eudicotylédons + *Cynara cardunculus* | 29
| Eudicotylédons + *Centaurea solstitialis* | 32

**BRAKER3**

| Données utilisées | Nombre de gènes de la liste trouvés
| :- | :-: |
| Protéines des espèces proches + RNA-Seq de *Centaurea jacea* | 37
| Protéines des Eudicotylédons + RNA-Seq de *Centaurea jacea* | 36
| Protéines des Eudicotylédons et *Cynara cardunculus* + RNA-Seq de *Centaurea jacea* | 36
| Protéines des Eudicotylédons et *Centaurea solstitialis* + RNA-Seq de *Centaurea jacea* | 36

**GALBA**

| Données utilisées | Nombre de gènes de la liste trouvés
| :- | :-: |
| Protéines des espèces proches | 57
| *Cynara cardunculus* | 39
| *Centaurea solstitialis* | 46

**TSEBRA**

| Données utilisées | Nombre de gènes de la liste trouvés
| :- | :-: |
| B1 *Centaurea jacea* customisé + B2 Eudicotylédons et *Cynara cardunculus* | 33
| B1 *Centaurea jacea* customisé + B2 Eudicotylédons et *Centaurea solstitialis* | 36

**LiftOn**

| Données utilisées | Nombre de gènes de la liste trouvés
| :- | :-: |
| *Cynara cardunculus* | 83
| *Arctium lappa* | 0
| *Centaurea solstitialis* | 644
| *Cynara cardunculus* + FunAnnotate | 25
| *Arctium lappa* + FunAnnotate | 28
| *Centaurea solstitialis* + FunAnnotate | 2

<br>

Ci-dessous, un tableau du nombres de gènes listés qui ont été trouvés dans nos annotations sans compter les gènes en double :

**AUGUSTUS**

| Données utilisées | Nombre de gènes de la liste trouvés
| :- | :-: |
| *ab initio* | 11

**BRAKER1**

| Données utilisées | Nombre de gènes de la liste trouvés
| :- | :-: |
| *Centaurea solstitialis* standard | 11
| *Centaurea solstitialis* customisé | 11
| *Centaurea cyanus* standard | 11
| *Centaurea cyanus* customisé | 11
| *Cynara cardunculus* standard | 10
| *Cynara cardunculus* customisé | 11
| *Centaurea jacea* standard | 11
| *Centaurea jacea* customisé | 11

**BRAKER2**

| Données utilisées | Nombre de gènes de la liste trouvés
| :- | :-: |
| Protéines des espèces proches | 11
| Eudicotylédons | 11
| Eudicotylédons + *Cynara cardunculus* | 11
| Eudicotylédons + *Centaurea solstitialis* | 11

**BRAKER3**

| Données utilisées | Nombre de gènes de la liste trouvés
| :- | :-: |
| Protéines des espèces proches + RNA-Seq de *Centaurea jacea* | 11
| Protéines des Eudicotylédons + RNA-Seq de *Centaurea jacea* | 11
| Protéines des Eudicotylédons et *Cynara cardunculus* + RNA-Seq de *Centaurea jacea* | 11
| Protéines des Eudicotylédons et *Centaurea solstitialis* + RNA-Seq de *Centaurea jacea* | 11

**GALBA**

| Données utilisées | Nombre de gènes de la liste trouvés
| :- | :-: |
| Protéines des espèces proches | 11
| *Cynara cardunculus* | 11
| *Centaurea solstitialis* | 11

**TSEBRA**

| Données utilisées | Nombre de gènes de la liste trouvés
| :- | :-: |
| B1 *Centaurea jacea* customisé + B2 Eudicotylédons et *Cynara cardunculus* | 11
| B1 *Centaurea jacea* customisé + B2 Eudicotylédons et *Centaurea solstitialis* | 11

**LiftOn**

| Données utilisées | Nombre de gènes de la liste trouvés
| :- | :-: |
| *Cynara cardunculus* | 4
| *Arctium lappa* | 0
| *Centaurea solstitialis* | 15
| *Cynara cardunculus* + FunAnnotate | 9
| *Arctium lappa* + FunAnnotate | 11
| *Centaurea solstitialis* + FunAnnotate | 1
