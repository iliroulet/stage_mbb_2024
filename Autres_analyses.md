# Sommaire


- [Intersections](#intersections)
- [Gènes "suspicieux"](#gènes-suspicieux)


# Intersections


L'outil AGAT permet de calculer le nombre de gènes en commun entre deux annotations avec le script <i>compare</i>.

Dans le futur du projet, on pourrait faire une comparaison des annotations grâce à ces commandes :


##### Documentation
https://agat.readthedocs.io/en/latest/index.html


##### Commandes

```python
mamba activate agat
agat_sp_compare_two_annotations.pl -gff1 annotation_1.gtf -gff2 annotation_2.gtf -o intersect_1_2
```

Un script python a ensuite été réalisé pour faire un diagramme de Venn à partir de ces chiffres afin de faciliter la visualisation.

```python
python3 plot_venn.py $arg1 $arg2 $arg1bis $arg2bis $arg3
```

Où :
- arg1 est composé du nom de la première annotation et de son nombre de gènes séparés par un ":" (exemple : AUGUSTUS:31349)
- arg2 est composé du nom de la seconde annotation et de son nombre de gènes séparés par un ":" (exemple : AUGUSTUS:31349)
- arg1bis est le nombre de gènes contenus uniquement dans la première annotation (donné dans le tableau donné par AGAT)
- arg2bis est le nombre de gènes contenus uniquement dans la seconde annotation (donné dans le tableau donné par AGAT)
- arg3 est le nombre de gènes en commun dans les deux annotations (calculé depuis le tableau donné par AGAT)


# Gènes "suspicieux"


Les gènes ayant plus de 80 exons dans les annotations d'AUGUSTUS, BRAKER1, BRAKER2 et TSEBRA sont considérés comme suspicieux et vont être BLASTés sur les bases de données <i>NT</i> et <i>NR</i> d'<i>NCBI</i>, ainsi que sur la base <i>SwissProt</i>.

Dû aux temps de calcul, seuls les gènes suspicieux les plus longs et les plus courts ont été BLASTés sur <i>NR</i> et <i>NT</i>.


**AUGUSTUS**

| Données utilisées | Nombre de gènes suspicieux
| :- | :-: |
*ab initio* | 95

**BRAKER1**

| Données utilisées | Nombre de gènes suspicieux
| :- | :-: |
*Centaurea solstitialis* standard | 44
*Centaurea solstitialis* customisé | 23
*Centaurea cyanus* standard | 37
*Centaurea cyanus* customisé | 15
*Cynara cardunculus* standard | 38
*Cynara cardunculus* customisé | 15
*Centaurea jacea* standard | 31
*Centaurea jacea* customisé | 19

**BRAKER2**

| Données utilisées | Nombre de gènes suspicieux
| :- | :-: |
Protéines des espèces proches | 31
Eudicotylédons | 6
Eudicotylédons + *Cynara cardunculus* | 0
Eudicotylédons + *Centaurea solstitialis* | 57

**BRAKER3**

| Données utilisées | Nombre de gènes suspicieux
| :- | :-: |
Protéines des espèces proches + RNA-Seq de *Centaurea jacea* | 0
Protéines des Eudicotylédons + RNA-Seq de *Centaurea jacea* | 0
Protéines des Eudicotylédons et *Cynara cardunculus* + RNA-Seq de *Centaurea jacea* | 0
Protéines des Eudicotylédons et *Centaurea solstitialis* + RNA-Seq de *Centaurea jacea* | 0

**GALBA**

| Données utilisées | Nombre de gènes suspicieux
| :- | :-: |
Protéines des espèces proches | 0
*Cynara cardunculus* | 0
*Centaurea solstitialis* | 0

**TSEBRA**

| Données utilisées | Nombre de gènes suspicieux
| :- | :-: |
B1 *Centaurea jacea* customisé + B2 Eudicotylédons | 6
B1 *Centaurea jacea* customisé + B2 Eudicotylédons et *Cynara cardunculus* | 0
B1 *Centaurea jacea* customisé + B2 Eudicotylédons et *Centaurea solstitialis* | 49

**LiftOn**

| Données utilisées | Nombre de gènes suspicieux
| :- | :-: |
*Cynara cardunculus* | 0
*Arctium lappa* | 0
*Centaurea solstitialis* | 0


##### Commandes

```python
cd /Results/structural_annotation/augustus_ab_initio
mkdir blast_results
```

```python
# Lancement d'un script qui extrait les gènes suspicieux et les écrit dans un fichier BED
python3 /Data/scripts/suspicious_genes_to_bed.py braker.gtf .
# Lancement d'un script qui extrait les gènes du fichier BED grâce à bedtools getfasta puis qui ajoutent des
# informations aux headers des séquences (longueur, nombre d'exons, nom du gène dans l'annotation)
python3 /Data/scripts/add_exon_number.py

# Utilisation de seqkit pour ordonner les gènes puis de awk pour extraire les plus long et plus petit gènes
mamba activate seqkit
seqkit sort suspicious_genes.fasta -l > sorted_suspicious_genes.fasta 
seqkit sort suspicious_genes.fasta -l -r > reverse_sorted_suspicious_genes.fasta
awk '/^>/{if(N)exit;++N;} {print;}' sorted_suspicious_genes.fasta > shortest_suspicious_gene.fasta
awk '/^>/{if(N)exit;++N;} {print;}' reverse_sorted_suspicious_genes.fasta > longest_suspicious_gene.fasta
```

```python
# Télécharger la base de donnée de taxonomie d'NCBI pour pouvoir récupérer les espèces des hits
mkdir taxdb
cd taxdb
wget ftp://ftp.ncbi.nlm.nih.gov/blast/db/taxdb.tar.gz
gzip -d taxdb.tar.gz
tar -vxf taxdb.tar
```

```python
# Exporter la base de données de taxonomie
export BLASTDB=./taxdb
# Lancer BLAST sur NT sur le gène le plus court
blastn -db nt -query shortest_suspicious_gene.fasta -out blast_results/nt_shortest.out -remote -max_hsps 1
	-outfmt "7 sscinames sacc stitle slen sstart send qlen qstart qend length pident nident mismatch gapopen evalue bitscore"
# Lancer BLAST sur NT sur le gène le plus long
    blastn -db nt -query longest_suspicious_gene.fasta -out blast_results/nt_longest.out -remote -max_hsps 1
	-outfmt "7 sscinames sacc stitle slen sstart send qlen qstart qend length pident nident mismatch gapopen evalue bitscore"
# Lancer BLAST sur NR sur le gène le plus court
    blastn -db nr -query shortest_suspicious_gene.fasta -out blast_results/nr_shortest.out -remote -max_hsps 1
	-outfmt "7 sscinames sacc stitle slen sstart send qlen qstart qend length pident nident mismatch gapopen evalue bitscore"
# Lancer BLAST sur NR sur le gène le plus long
blastn -db nr -query longest_suspicious_gene.fasta -out blast_results/nr_longest.out -remote -max_hsps 1
	-outfmt "7 sscinames sacc stitle slen sstart send qlen qstart qend length pident nident mismatch gapopen evalue bitscore"
# Lancer BLAST sur SwissProt sur tous les gènes
blastx -db /Data/SwissProt_db/uniprotkb_swissprot -query sorted_suspicious_genes.fasta -max_hsps 1
	-out blast_results/sp_all.out -outfmt "7 sscinames sacc stitle slen sstart send qlen qstart qend length pident nident mismatch gapopen evalue bitscore"
```

##### Fichiers obtenus


- nr_longest.out : résultat du BLAST du plus long sur NR
- nr_shortest.out : résultat du BLAST du plus court sur NR
- nt_longest.out : résultat du BLAST du plus long sur NT
- nt_shortest.out : résultat du BLAST du plus court sur NT
- sp_all.out : résultat du BLAST de tous les gènes sur SwissProt
- sp_longest.out : résultat du BLAST du plus long sur SwissProt
- sp_shortest.out : résultat du BLAST du plus court sur SwissProt
