# cat Dockerfile
# FROM node:lts-alpine3.13
# MAINTAINER Lacey-Anne Sanderson <laceyannesanderson@gmail.com>
# USER root
# WORKDIR /
# RUN npm install -g @jbrowse/cli && jbrowse --version

#RUN jbrowse create jbrowse2
RUN jbrowse create -u https://github.com/GMOD/jbrowse-components/releases/download/v2.11.1/jbrowse-web-v2.11.1.zip jbrowse2

EXPOSE 5000
WORKDIR /jbrowse2
CMD npm_config_yes=true npx serve -l 5000 .

sudo docker build -t jbrowseserver .
sudo docker run -itd -p 5000:5000 --mount type=bind,src=/home/ines/Desktop/Stage_MBB/JBrowse2/data,dst=/Data --name useJBrowse jbrowseserver

sudo docker exec useJBrowse ln -s ../Data

sudo docker exec useJBrowse jbrowse add-assembly Data/centaurea_genome.fasta --load symlink --name "Centaurée de la Clape" --alias centaurea.corymbosa --type indexedFasta

for f in data/*.gff3;
do sed -i "s/Scaffold/Super-Scaffold/g" $f; file_full=$(basename $f); file=${file_full%%.*};
(grep ^"#" data/${file}.gff3; grep -v ^"#" data/${file}.gff3 | grep -v "^$" | grep "\t" | sort -k1,1 -k4,4n) > data/${file}_sorted.gff3;
bgzip data/${file}_sorted.gff3;
tabix -p gff data/${file}_sorted.gff3.gz;
sudo docker exec useJBrowse jbrowse add-track Data/${file}_sorted.gff3.gz --load symlink --name="${file}" --trackId=${file} --category="CanvasFeatures";
sudo docker exec useJBrowse sed -i "s|${file}_sorted\.gff3|../Data/${file}_sorted.gff3|" config.json;
done

sudo docker exec useJBrowse sed -i "s|centaurea_genome\.fasta|../Data/centaurea_genome.fasta|" config.json;

sudo docker exec useJBrowse jbrowse text-index