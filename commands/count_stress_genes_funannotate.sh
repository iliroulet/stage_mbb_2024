clear
for d in augustus*;
do echo "$d :";
grep -i -f /Data/liste_genes_stress_hydrique.txt $d/FunAnnotate/augustus_output/annotate_results/Centaurea_corymbosa.gff3 -c;
echo "";
done
cd BRAKER1_standard_align;
for d in BRAKER1*;
do echo "$d standard :";
grep -i -f /Data/liste_genes_stress_hydrique.txt $d/FunAnnotate/braker/annotate_results/Centaurea_corymbosa.gff3 -c;
echo "";
done
cd ../BRAKER1_custom_align
for d in BRAKER1*;
do echo "$d custom :";
grep -i -f /Data/liste_genes_stress_hydrique.txt $d/FunAnnotate/braker/annotate_results/Centaurea_corymbosa.gff3 -c;
echo "";
done
cd ..
for d in BRAKER2*;
do echo "$d :";
grep -i -f /Data/liste_genes_stress_hydrique.txt $d/FunAnnotate/braker/annotate_results/Centaurea_corymbosa.gff3 -c;
echo "";
done
for d in BRAKER3*;
do echo "$d :";
grep -i -f /Data/liste_genes_stress_hydrique.txt $d/FunAnnotate/braker/annotate_results/Centaurea_corymbosa.gff3 -c;
echo "";
done
cd TSEBRA_B1_B2
for d in C*;
do echo "TSEBRA $d :";
grep -i -f /Data/liste_genes_stress_hydrique.txt $d/FunAnnotate/tsebra_b1_b2_renamed/annotate_results/Centaurea_corymbosa.gff3 -c;
echo "";
done
cd ..
for d in GALBA*;
do echo "$d :";
grep -i -f /Data/liste_genes_stress_hydrique.txt $d/FunAnnotate/galba/annotate_results/Centaurea_corymbosa.gff3 -c;
echo "";
done
for d in LiftOn*;
do echo "$d :";
grep -i -f /Data/liste_genes_stress_hydrique.txt $d/FunAnnotate/lifton/annotate_results/Centaurea_corymbosa.gff3 -c;
echo "";
done
