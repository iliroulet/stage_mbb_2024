wget -O Dfam_curatedonly.h5.gz https://www.dfam.org/releases/Dfam_3.7/families/Dfam_curatedonly.h5.gz
gunzip -c Dfam_curatedonly.h5.gz > /opt/biotools/conda/envs/earlgrey/share/RepeatMasker/Libraries/Dfam.h5
rm Dfam_curatedonly.h5.gz
