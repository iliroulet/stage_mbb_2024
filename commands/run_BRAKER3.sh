# exporter les outils d'alignement utilisés par GeneMark dans le PATH
export PATH=$PATH:$GENEMARK_PATH/tools
# Lancer BRAKER3
braker.pl \
	--workingdir=$WORKING_DIR \
	--genome=$GENOME_PATH \
	--rnaseq_sets_ids=$SRA_LIST \
	--prot_seq=$PROTEINS_PATH \
	--busco_lineage=$BUSCO_LINEAGE \
	--threads=$T

