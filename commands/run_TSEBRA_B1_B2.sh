tsebra.py \
	-k $GTF_TO_KEEP_PATH \
	-g $GTF_TO_ADD_PATH \
	-e $HINTSFILES_PATH \
	-o $OUTPUT_PATH 2> $LOG_PATH
# Get protein and coding sequences with Augustus script
getAnnoFastaFromJoingenes.py -g $GENOME -o $OUTPUT_DIR -f $OUTPUT_PATH
