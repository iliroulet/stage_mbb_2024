# run BUSCO on proteins
echo "Running BUSCO on protein sequences..."
busco -m proteins -i $AA_FILE_PATH -o $BUSCO_OUTPUT_DIR/proteins -l $BUSCO_LINEAGE -c $T > $BUSCO_OUTPUT_DIR/proteins/busco.log
# run BUSCO on cds
echo "Running BUSCO on coding sequences..."
busco -m transcriptome -i $CODINGSEQ_FILE_PATH -o $BUSCO_OUTPUT_DIR/cds -l $BUSCO_LINEAGE -c $T > $BUSCO_OUTPUT_DIR/cds/busco.log
# Count number of proteins
echo "Counting number of protein sequences"
grep -c ">" $AA_FILE_PATH > $WORKING_DIR/nb_of_prot_seq.txt
# Count number of coding sequences
echo "Counting number of coding sequences"
grep -c ">" $CODINGSEQ_FILE_PATH > $WORKING_DIR/nb_of_cod_seq.txt
# Compute basic exons statistics with GALBA script
echo "Computing some descriptive statistics..."
analyze_exons.py -f $GTF_PATH > $WORKING_DIR/exons_stat_from_GALBA.txt
