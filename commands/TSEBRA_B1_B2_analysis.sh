# run BUSCO
echo "Running BUSCO..."
busco -m proteins -i tsebra_b1_b2.aa -o busco_tsebra_b1_b2 -l eudicots_odb10 -c 8 &> busco_tsebra_b1_b2.log

# Count number of transcripts by counting FASTA headers
echo "Counting number of protein sequences (= transcripts)..."
grep -c ">" tsebra_b1_b2.aa > nb_of_prot_seq.txt

# Number of genes, and descriptive statistics
echo "Computing some descriptive statistics..."
/Data/tools/GALBA/scripts/analyze_exons.py -f tsebra_b1_b2.gtf > exons_stat.txt

# run QUAST
echo "Running QUAST..."
quast tsebra_b1_b2.codingseq \
	-r /Data/Tutoriel/genome/genome.fa \
	-g tsebra_b1_b2.gtf \
	-m 0 \
	-o quast_results/
