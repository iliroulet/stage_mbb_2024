BuildDatabase -name $DATABASE_NAME $GENOME_PATH
RepeatModeler -database $DATABASE_PATH -threads $T -LTRStruct
RepeatMasker -pa $P -lib $LIBRARY_DIR -cutoff $CUTOFF -xsmall $GENOME_PATH -a -source -html -gff
