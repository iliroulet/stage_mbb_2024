mamba activate eggNOG-mapper
emapper.py  --sensmode more-sensitive --cpu 30 -i /Results/structural_annotation/$d/original_genome/lifton.aa -o $d/eggNOG/lifton;
for d in LiftOn*o*;
do emapper.py  --sensmode more-sensitive --cpu 30 -i /Results/structural_annotation/$d/lifton_results/lifton.aa -o $d/eggNOG/lifton;
done
for d in LiftOn*;
do { time /Data/tools/interproscan/interproscan.sh -i $d/InterProScan/proteins.aa -b $d/InterProScan/lifton -goterms -pa -iprlookup -cpu 30 -appl NCBIFAM,SUPERFAMILY,PANTHER,ProSiteProfiles,PIRSR,ProSitePatterns,Pfam,PIRSF ; } 2> $d/InterProScan/time.txt;
done
mamba activate funannotate
for d in LiftOn*so*;
do { time funannotate annotate --gff $d/FunAnnotate/agat_lifton.gff3 --fasta centaurea_genome.fasta -s Centaurea_corymbosa -o $d/FunAnnotate/lifton --iprscan $d/InterProScan/lifton.xml --eggnog $d/eggNOG/lifton.emapper.annotations --busco_db eukaryota --cpus 30 ; } 2> $d/FunAnnotate/time.txt; mv genome*.fixedproducts $d/FunAnnotate; mv funannotate-annotate.*.log $d/FunAnnotate;
done


