for d in augustus*;
do cut -f3,9 $d/FunAnnotate/*/annotate_results/Centaurea_corymbosa.gff3 | grep "product=hypothetical protein;$" | awk '{split($2,a,";"); split(a[1],b,"="); print b[2]}' > $d/FunAnnotate/unannotated_genes.txt; u=$(wc -l $d/FunAnnotate/unannotated_genes.txt | awk '{print $1}'); p=$(cut -f3 $d/FunAnnotate/*.gff3 | grep -c "mRNA"); echo "$d : $(($p-$u))";
done
cd BRAKER1_standard_align;
for d in BRAKER1*;
do cut -f3,9 $d/FunAnnotate/*/annotate_results/Centaurea_corymbosa.gff3 | grep "product=hypothetical protein;$" | awk '{split($2,a,";"); split(a[1],b,"="); print b[2]}' > $d/FunAnnotate/unannotated_genes.txt; u=$(wc -l $d/FunAnnotate/unannotated_genes.txt | awk '{print $1}'); p=$(cut -f3 $d/FunAnnotate/*.gff3 | grep -c "mRNA"); echo "$d standard : $(($p-$u))";
done
cd ../BRAKER1_custom_align
for d in BRAKER1*;
do cut -f3,9 $d/FunAnnotate/*/annotate_results/Centaurea_corymbosa.gff3 | grep "product=hypothetical protein;$" | awk '{split($2,a,";"); split(a[1],b,"="); print b[2]}' > $d/FunAnnotate/unannotated_genes.txt; u=$(wc -l $d/FunAnnotate/unannotated_genes.txt | awk '{print $1}'); p=$(cut -f3 $d/FunAnnotate/*.gff3 | grep -c "mRNA"); echo "$d custom : $(($p-$u))";
done
cd ..
for d in BRAKER2*;
do cut -f3,9 $d/FunAnnotate/*/annotate_results/Centaurea_corymbosa.gff3 | grep "product=hypothetical protein;$" | awk '{split($2,a,";"); split(a[1],b,"="); print b[2]}' > $d/FunAnnotate/unannotated_genes.txt; u=$(wc -l $d/FunAnnotate/unannotated_genes.txt | awk '{print $1}'); p=$(cut -f3 $d/FunAnnotate/*.gff3 | grep -c "mRNA"); echo "$d : $(($p-$u))";
done
for d in BRAKER3*;
do cut -f3,9 $d/FunAnnotate/*/annotate_results/Centaurea_corymbosa.gff3 | grep "product=hypothetical protein;$" | awk '{split($2,a,";"); split(a[1],b,"="); print b[2]}' > $d/FunAnnotate/unannotated_genes.txt; u=$(wc -l $d/FunAnnotate/unannotated_genes.txt | awk '{print $1}'); p=$(cut -f3 $d/FunAnnotate/*.gff3 | grep -c "mRNA"); echo "$d : $(($p-$u))";
done
cd TSEBRA_B1_B2
for d in C*;
do cut -f3,9 $d/FunAnnotate/*/annotate_results/Centaurea_corymbosa.gff3 | grep "product=hypothetical protein;$" | awk '{split($2,a,";"); split(a[1],b,"="); print b[2]}' > $d/FunAnnotate/unannotated_genes.txt; u=$(wc -l $d/FunAnnotate/unannotated_genes.txt | awk '{print $1}'); p=$(cut -f3 $d/FunAnnotate/*.gff3 | grep -c "mRNA"); echo "$d : $(($p-$u))";
done
cd ..
for d in GALBA*;
do cut -f3,9 $d/FunAnnotate/*/annotate_results/Centaurea_corymbosa.gff3 | grep "product=hypothetical protein;$" | awk '{split($2,a,";"); split(a[1],b,"="); print b[2]}' > $d/FunAnnotate/unannotated_genes.txt; u=$(wc -l $d/FunAnnotate/unannotated_genes.txt | awk '{print $1}'); p=$(cut -f3 $d/FunAnnotate/*.gff3 | grep -c "mRNA"); echo "$d : $(($p-$u))";
done
