sed 's/^\s*//g' test_mqc.txt.tbl | \
sed 's/ \{2,\}/\t/g'  | \
sed 's/-\{2,\}/<\/tr>/g'  | \
awk 'BEGIN{FS="\t"; print "<table border=1>"}
{
  if ( index($0,"=") == 0 )
  {
    print "<tr>"
    if (NF == 3) print "<td> </td>"
    for (i = 1; i<= NF; i++) if ($i != "") print "<td>"$i"</td>"
    print "</tr>"
  }
}
END{print "</table>"}' > test_mqc.html
