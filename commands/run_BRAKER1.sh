# exporter les outils d'alignement utilisés par GeneMark dans le PATH
export PATH=$PATH:$GENEMARK_PATH/tools
# Lancer BRAKER1 avec une liste de numéros d'accessions de SRA (NCBI)
braker.pl \
	--workingdir=$WORKING_DIR \
	--genome=$GENOME_PATH \
	--rnaseq_sets_ids=$SRA_LIST \
	--threads=$T
