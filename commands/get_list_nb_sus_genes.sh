for d in augustus*;
do nb=$(grep -c ">" $d/suspicious_genes.fasta);
echo "$d : $nb suspicious genes" > /Results/structural_annotation/nb_of_sus_genes.txt;
echo "" >> /Results/structural_annotation/nb_of_sus_genes.txt;
done
cd BRAKER1_standard_align
for d in BRAKER1*;
do nb=$(grep -c ">" $d/suspicious_genes.fasta);
echo "$d standard : $nb suspicious genes" >> /Results/structural_annotation/nb_of_sus_genes.txt;
echo "" >> /Results/structural_annotation/nb_of_sus_genes.txt;
done
cd ../BRAKER1_custom_align
for d in BRAKER1*;
do nb=$(grep -c ">" $d/suspicious_genes.fasta);
echo "$d custom : $nb suspicious genes" >> /Results/structural_annotation/nb_of_sus_genes.txt;
echo "" >> /Results/structural_annotation/nb_of_sus_genes.txt;
done
cd ..
for d in BRAKER2*;
do nb=$(grep -c ">" $d/suspicious_genes.fasta);
echo "$d : $nb suspicious genes" >> /Results/structural_annotation/nb_of_sus_genes.txt;
echo "" >> /Results/structural_annotation/nb_of_sus_genes.txt;
done
for d in BRAKER3*;
do nb=$(grep -c ">" $d/suspicious_genes.fasta);
echo "$d : $nb suspicious genes" >> /Results/structural_annotation/nb_of_sus_genes.txt;
echo "" >> /Results/structural_annotation/nb_of_sus_genes.txt;
done
for d in GALBA*;
do nb=$(grep -c ">" $d/suspicious_genes.fasta);
echo "$d : $nb suspicious genes" >> /Results/structural_annotation/nb_of_sus_genes.txt;
echo "" >> /Results/structural_annotation/nb_of_sus_genes.txt;
done
cd TSEBRA_B1_B2
for d in *;
do nb=$(grep -c ">" $d/suspicious_genes.fasta);
echo "TSEBRA $d : $nb suspicious genes" >> /Results/structural_annotation/nb_of_sus_genes.txt;
echo "" >> /Results/structural_annotation/nb_of_sus_genes.txt;
done
cd ..
for d in LiftOn*;
do nb=$(grep -c ">" $d/lifton_results/suspicious_genes.fasta);
echo "$d : $nb suspicious genes" >> /Results/structural_annotation/nb_of_sus_genes.txt;
echo "" >> /Results/structural_annotation/nb_of_sus_genes.txt;
done
