# Sommaire


- [Lancement](#commandes)

- [Résultats](#fichiers-obtenus)

    - [Statistiques basiques](#fichiers-obtenus-1)
    
    - [Statistiques avec gFACs](#fichiers-obtenus-2)
    
    - [Analyse avec BUSCO](#fichiers-obtenus-3)
    
    - [Analyse avec OMArk](#fichiers-obtenus-4)


# LiftOn


LiftOn permet de faire du transfert d'annotation depuis une espèce de référence vers notre espèce, de la même façon que LiftOff (https://github.com/agshumate/Liftoff), mais en y couplant aussi de l'alignement de protéines afin d'améliorer les résultats par rapport à un simple transfert.

L'outil a été utilisé 3 fois avec chacune des 3 espèces proches annotées comme espèce de référence.


## Annotation


##### Documentation
https://ccb.jhu.edu/lifton/

https://github.com/Kuanhao-Chao/LiftOn


##### Commandes

```python
nohup lifton -g annotation.gff -P proteins.fa -T rna.fa -t 30 /Data/genomes/masked_genome/centaurea_genome.fasta.masked genome.fa
# obtenir les fichiers .aa et .codingseq grâce à un script d'AGAT
mamba activate agat
agat_sp_extract_sequences.pl -g lifton.gff3 -f /Data/genomes/masked_genome/centaurea_genome.fasta.masked -o lifton.codingseq
agat_sp_extract_sequences.pl -g lifton.gff3 -f /Data/genomes/masked_genome/centaurea_genome.fasta.masked -o lifton.aa -p
```

##### Fichiers obtenus


- annotation.gff_db : base de données sur l'annotation de référence
- genome.fa.fai : fichier d'indexation du génome
- lifton.gff3 : annotation
- lifton_output : fichier intermédiaires
- nohup.out : sortie
- proteins.fa.fai : fichier d'indexation des protéines
- rna.fa.fai : fichier d'indexation des transcrits


## Analyse


### Statistiques de base


##### Commandes

```python
grep -c ">" lifton.aa > nb_of_prot_seq.txt
```

```python
# Ploter l'histogramme du nombre d'exons trouvés selon leur taille
python3 /Data/scripts/exons_len_plot.py lifton.gff3 .
# Ploter l'histogramme du nombre de gènes trouvés selon leur taille
python3 /Data/scripts/gene_len_plot.py lifton.gff3 .
# Ploter l'histogramme du nombre de gènes selon leur nombre d'exons
python3 /Data/scripts/exons_mean_nbr_plot_stat_for_NCBI.py lifton.gff3 .
# Ploter le scatter plot de la taille de gènes par leur nombre moyen d'exons
python3 /Data/scripts/gene_len_by_mean_exons_for_NCBI.py lifton.gff3 .
```

```python
mkdir plots
mv *.png plots
mkdir stats
mv *n*.txt stats
```

##### Fichiers obtenus


- exons_length_histogram.png : histogramme du nombre d'exons trouvés selon leur taille
- exons_mean_nbr_histogram.png : histogramme du nombre de gènes selon leur nombre d'exons (moyenne sur tous les transcrits trouvés pour le gène)
- exons_mean_nbr_stat.txt : statistiques sur les exons par gènes (moyenne sur tous les transcrits)
- gene_len_by_mean_exons_scatterplot.png : scatter plot de la taille de gènes par leur nombre moyen d'exons
- gene_length_histogram.png : histogramme du nombre de gènes trouvés selon leur taille
- nb_of_prot_seq.txt : nombre de séquences protéiques extraites de l'annotation
- transcripts_length_histogram.png : histogramme du nombre de transcrits trouvés selon leur taille


Ces résultats ont été synthétisés en tableaux :

| | <i>Centaurea solstitialis</i> | <i>Cynara cardunculus</i> | <i>Arctium lappa</i>
| :- | :-: | :-: | :-: |
Nombre de protéines trouvées | 30 586 | 31 738 | 29 405

<br>

| | <i>Centaurea solstitialis</i> | <i>Cynara cardunculus</i> | <i>Arctium lappa</i>
| :- | :-: | :-: | :-: |
Nombre de gènes | 31 142 | 24 033 | 29 405
Nombre de gènes mono-ex. | 6 249 | 6 693 | 9 099
Nombre de gènes multi-ex. | 24 893 | 17 340 | 20 306
Ratio mono/multi | 0.25 | 0.39 | 0.45
Nombre médian d’exons | 3 | 3 | 3
Nombre maximum d’exons | 78 | 74 | 79


### gFACs


L'outil gFACs permet de manipuler des fichiers d'annotation et notamment de produire des statistiques sur le contenu des fichiers.


##### Documentation
https://gfacs.readthedocs.io/en/latest/


##### Commandes


gFACs a une option format (-f) pour renseigner le format du fichier GTF donné en entrée. Pour savoir le format à renseigner, on peut utiliser le script de support format_diagnosis.pl (https://gfacs.readthedocs.io/en/latest/Bonus/format_diagnosis.html). Ce script renvoie une suite de valeurs que l'on peut ensuite aller comparer au tableau de formats de la documentation gFACs (https://gfacs.readthedocs.io/en/latest/_images/Table.png) pour déterminer quel format gFACs correspond au nôtre, ou au moins lequel en est le plus proche.

```python
cd /Results/structural_annotation/TSEBRA_B1_B2
perl /opt/biotools/conda/envs/gFACs/opt/perl-gfacs-1.1.1/support_scripts/format_diagnosis.pl lifton.gff3
```

```python
mkdir gFACs
mamba activate gFACs
gFACs.pl -f refseq_gff --statistics --fasta /Data/genomes/original_genome/centaurea_genome.fasta --splice-table --nt-content \
	--get-fasta --get-protein-fasta --create-gff3 --distributions exons_lengths intron_lengths CDS_lengths gene_lengths \
	exon_position exon_position_data intron_position intron_position_data -O gFACs lifton.gff3
```

##### Fichiers obtenus


- CDS_lengths_distributions.tsv : distribution des longueurs des séquences codantes
- data_exon_position_distributions.tsv : liste des positions des exons ?
- data_intron_position_distributions.tsv : liste des positions des exons ?
- exon_position_distributions.tsv : distribution de la position des exons
- gene_lengths_distributions.tsv : distribution des longueurs des gènes
- genes.fasta : liste des gènes utilisés par gFACs pour ses statistiques
- genes.fasta.faa : liste des gènes utilisés par gFACs pour ses statistiques accompagnés de certaines séquences protéiques
- gene_table.txt : table de gènes crées par gFACs pour ses statistiques
- gFACs_log.txt : fichier de log
- intron_lengths_distributions.tsv : distribution des longueurs des introns
- intron_position_distributions.tsv : distribution de la position des introns
- out.gff3 : fichier contenant les éléments étudiés par gFACs (mRNA, exons, ...)
- statistics.txt : statistiques produites par gFACs


Le résumé des statistiques obtenues se trouve dans deux tableaux de synthèse :

![1er tableau de synthèse de gFACs](images/gFACs/LiftOn_gFACs_1.svg)

![2ème tableau de synthèse de gFACs](images/gFACs/LiftOn_gFACs_2.svg)


### BUSCO


L'outil BUSCO permet de faire une recherche des séquences codantes trouvées dans la base de données orthoDB (https://www.orthodb.org/) d'une lignée afin de voir si les gènes prédits y sont retrouvés. La lignée sélectionnée ici est la lignée eudicots_odb10.


##### Documentation
https://busco.ezlab.org/busco_userguide.html


##### Commandes


Pour lancer BUSCO en mode transcriptome, il faut d'abord faire en sorte de ne sélectionner qu'un isoforme (= transcrit) par gène. On utilise donc AGAT pour ne garder que les isoformes aux plus longs CDS. L'annotation LiftOn ne comprenant pas d'isoformes, il n'est pas nécessaire de s'en soucier ici. 

```python
cd ..
mamba activate busco
busco -m transcriptome -i lifton.codingseq -o busco_results -l eudicots_odb10 -c 8
```

##### Fichiers obtenus


- busco.log : log de la commande busco
- logs : dossier des logs de tous les processus de busco
- run_eudicots_odb10 : dossier contenant les séquences trouvées par busco en format FASTA et GFF, les identifiants des séquences manquantes, et l'output de hmmer et metaeuk
- short_summary.specific.eudicots_odb10.busco_results.json : résumé du résultat busco
- short_summary.specific.eudicots_odb10.busco_results.txt : résumé du résultat busco


##### Synthèse des résultats


| | <i>Centaurea solstitialis</i> | <i>Cynara cardunculus</i> | <i>Arctium lappa</i>
| :- | :-: | :-: | :-: |
Complet | 1 337 (57.4%) | 1 575 (67.8%) | 1 855 (79.8%)
Complet unique | 1 264 (54.3%) | 1076 (46.3%) | 1 777 (76.4%)
Complet dupliqué | 73 (3.1%) | 499 (21.5%) | 78 (3.4%)
Fragmenté | 186 (8%) | 381 (16.4%) | 57 (2.5%)
Manquant | 803 (34.6%) | 370 (15.8%) | 414 (17.7%)
Total / 2326 | 1 523 (65.4%) | 1 956 (84.2%) | 1 912 (82.3%)


### OMArk


L'outil OMArk permet de faire une recherche des séquences protéiques trouvées dans la base de données OMA (https://omabrowser.org/oma/home/) d'une lignée afin de voir si les gènes prédits y sont retrouvés. La lignée sélectionnée ici est la lignée Asteranae (clade). Cette lignée comprend 11 espèces :
- <i>Actinidia chinensis var. chinensis</i>
- <i>Capsicum annuum</i>
- <i>Coffea canephora</i>
- <i>Cynara cardunculus var. scolymus</i>
- <i>Daucus carota subsp. sativus</i>
- <i>Erythranthe guttata</i>
- <i>Helianthus annuus</i>
- <i>Nicotiana attenuata</i>
- <i>Nicotiana tabacum</i>
- <i>Solanum lycopersicum</i>
- <i>Solanum tuberosum</i>


##### Documentation
https://github.com/DessimozLab/OMArk/tree/main

https://github.com/DessimozLab/omamer


##### Commandes


Il faut d'abord faire une recherche dans la base OMA (ici <i>Viridiplantae</i>) avec l'outil OMAmer, puis soumettre le résultats de cette recherche à OMArk.

```python
# Pour télécharger la base de données utilisée ici (si besoin) :
cd /Data/dbs/OMA
wget https://omabrowser.org/All/Viridiplantae.h5
```

```python
cd /Results/structural_annotation/TSEBRA_B1_B2/$combination
mamba activate OMArk
mkdir OMAmer-OMArk
omamer search -d /Data/dbs/OMA/Viridiplantae.h5 -q tsebra_b1_b2_renamed.aa -o OMAmer-OMArk/omamer.out -t 8
```

```python
cd OMAmer-OMArk
# On donne à omark le rang taxonomique le plus proche (parmi la liste des rangs acceptés) avec l'option -r,
# et l'identifiant taxonomique de ce rang pour notre espèce avec l'option -t
omark -f omamer.out -d /Data/dbs/OMA/Viridiplantae.h5 -o omark -r family -t 4210 -of ../lifton.aa
```

##### Fichiers obtenus


- omamer_Cynara_cardunculus_var_scolymus.fasta : liste des protéines ayant été retrouvés dans la base de <i>Cynara cardunculus</i>
- omamer_detailed_summary.txt : résumé détaillé des résultats
- omamer_mapped.fasta : liste des protéines retrouvées dans la lignée donnée
- omamer_misplaced.fasta : liste des protéines retrouvées dans d'autres lignées que la lignée donnée
- omamer_no_hits.fasta :  : liste des protéines non-retrouvées dans la base de données
- omamer.omq : liste des protéines uniques retrouvées
- omamer.pdf : histogramme des pourcentages de protéines selon leur catégorie
- omamer.png : histogramme des pourcentages de protéines selon leur catégorie
- omamer_selected_isoforms.txt : liste des isoformes (= transcrits) choisis pour chaque gène par omark
- omamer.sum : résumé simplifié des résultats
- omamer.tax : rappel de la lignée et clade sélectionnés
- omamer.ump : liste des protéines complètes retrouvés dans la lignée


##### Synthèse des résultats


Pour rappel, OMArk ne sélectionne qu'un seul isoforme par gène : il ne prend donc pas en compte toutes les protéines fournies par nos annotations.

Le nombre de protéines prises en compte dans nos annotations par OMArk est :

| | <i>Centaurea solstitialis</i> | <i>Cynara cardunculus</i> | <i>Arctium lappa</i>
| :- | :-: | :-: | :-: |
Nombre total de protéines | 30 582 | 31 729 | 29 382


Le premier tableau permet de voir les gènes trouvés dans la lignée sélectionnée, et en combien de copies, similairement à BUSCO :

| | <i>Centaurea solstitialis</i> | <i>Cynara cardunculus</i> | <i>Arctium lappa</i>
| :- | :-: | :-: | :-: |
Unique | 6 622 (59.14%) | 6 368 (56.87%) | 7 540 (67.33%)
Dupliqué | 1 423 (12.71%) | 2 979 (26.60%) | 2 268 (20.25%)
Dupliqué attendu | 986 (8.81%) | 1 421 (12.69%) | 1 762 (15.73%)
Dupliqué inattendu | 437 (3.9%) | 1 558 (13.91%) | 506 (4.52%)
Manquant | 3 153 (28.16%) | 1 851 (16.53%) | 1 390 (12.41%)
Total / 11 198 | 8 045 (71.84%) | 9 347 (83.47%) | 9 808 (87.59%)

<!-- #region -->
Le deuxième tableau obtenu permet de voir les gènes retrouvés dans des espèces appartenant à la lignée sélectionnée (Consistent), ceux retrouvés dans des espèces non incluses dans la lignée (Inconsistent), et enfin ceux n'ayant pas été trouvés dans la base de données OMA (Inconnu) :


| | <i>Centaurea solstitialis</i> | <i>Cynara cardunculus</i> | <i>Arctium lappa</i>
| :- | :-: | :-: | :-: |
Consistent | 22 240 (72.72%) | 25 377 (79.98%) | 23 594 (80.3%)
Consistent partiel | 4 585 (14.99%) | 3 740 (11.79%) | 2 430 (8.27%)
Consistent fragmenté | 2 761 (9.03%) | 1 552 (4.89%) | 2 513 (8.55%)
Inconsistent | 1 833 (5.99%) | 417 (1.31%) | 1 136 (3.87%)
Inconsistent partiel | 1 029 (3.36%) | 215 (0.68%) | 627 (2.13%)
Inconsistent fragmenté | 245 (0.8%) | 68 (0.21%) | 198 (0.67%)
Inconnu | 6 509 (21.28%) | 5 935 (18.71%) | 4 652 (15.83%)
<!-- #endregion -->

L'espèce chez laquelle le plus de gènes ont été retrouvés est <b><i>Cynara cardunculus</i></b> pour toutes les annotations :

| | <i>Centaurea solstitialis</i> | <i>Cynara cardunculus</i> | <i>Arctium lappa</i>
| :- | :-: | :-: | :-: |
Espèce détectée : <i>Cynara cardunculus</i> | 24 073 (78.72%) | 25 794 (81.29%) | 24 730 (84.17%)

Cela est certainement dû au fait qu'OMA ne contient que 11 espèces dans son clade des Astéridées, et <i>Cynara cardunculus</i> est la seule de ces espèces à être assez proche de notre Centaurée.
