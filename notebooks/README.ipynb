{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Sommaire"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "- [Génome](#génome)\n",
    "\n",
    "- [Espèces proches](#espèces-proches)\n",
    "\n",
    "- [Masking](#masking)\n",
    "\n",
    "- [Annotation structurelle](#annotation-structurelle)\n",
    "\n",
    "- [Annotation fonctionnelle](#annotation-fonctionnelle)\n",
    "\n",
    "- [AUGUSTUS](#augustus)\n",
    "    \n",
    "- [BRAKER1](#braker1)\n",
    "    \n",
    "- [BRAKER2](#braker2)\n",
    "    \n",
    "- [BRAKER3](#braker3)\n",
    "    \n",
    "- [GALBA](#galba)\n",
    "    \n",
    "- [TSEBRA](#tsebra)\n",
    "    \n",
    "- [LiftOn](#lifton)\n",
    "    \n",
    "- [Analyses complémentaires](#analyses-complémentaires)\n",
    "    \n",
    "- [Visualisation et recherche des gènes d'intérêt](#visualisation-et-recherche-des-gènes-dintérêt)\n",
    "    \n",
    "- [Emplacement des données](#emplacement-des-données)\n",
    "\n",
    "- [Installation des environnements](#installation-des-environnements)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Génome"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Le but de ce projet est d'annoter le génome de <i>Centaurea corymbosa</i>, et plus particulièrement de trouver les gènes liés à la résistance à la sécheresse. Nous partons donc d'un génome assemblé lors d'un projet de l'équipe EvoDemo de l'ISEM. Le génome a été séquencé avec PacBio et assemblé à partir de reads HiFi avec Hicanu et Hifiasm. Ses estimations génétiques sont :\n",
    "- 2n = 18\n",
    "- 749 799 250 paires de bases\n",
    "- 58 contigs\n",
    "- 8.852% de gaps"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "L'outil BUSCO a été lancé sur le génome assemblé, à titre de comparaison avec les annotations qui vont suivre :"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "cd /Results\n",
    "busco -m genome -i /Data/genomes/original_genome/centaurea_genome.fasta -l eudicots_odb10 -o busco_genome -c 8\n",
    "less busco_genome/short_summary.specific.eudicots_odb10.busco_genome.txt"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# BUSCO version is: 5.6.1 \n",
    "# The lineage dataset is: eudicots_odb10 (Creation date: 2024-01-08, number of genomes: 31, number of BUSCOs: 2326)\n",
    "# Summarized benchmarking in BUSCO notation for file /Data/Centaurea/genomes/regular_masking/centaurea_genome.fasta.masked\n",
    "# BUSCO was run in mode: euk_genome_met\n",
    "# Gene predictor used: metaeuk\n",
    "\n",
    "\t***** Results: *****\n",
    "\n",
    "\tC:95.7%[S:90.3%,D:5.4%],F:0.6%,M:3.7%,n:2326\t   \n",
    "\t2227\tComplete BUSCOs (C)\t\t\t   \n",
    "\t2101\tComplete and single-copy BUSCOs (S)\t   \n",
    "\t126\tComplete and duplicated BUSCOs (D)\t   \n",
    "\t15\tFragmented BUSCOs (F)\t\t\t   \n",
    "\t84\tMissing BUSCOs (M)\t\t\t   \n",
    "\t2326\tTotal BUSCO groups searched\t\t   \n",
    "\n",
    "Assembly Statistics:\n",
    "\t58\tNumber of scaffolds\n",
    "\t10561\tNumber of contigs\n",
    "\t749799250\tTotal length\n",
    "\t8.852%\tPercent gaps\n",
    "\t33 MB\tScaffold N50\n",
    "\t5 MB\tContigs N50\n",
    "\n",
    "\n",
    "Dependencies and versions:\n",
    "\thmmsearch: 3.1\n",
    "\tbbtools: 39.06\n",
    "\tmetaeuk: 6.a5d39d9\n",
    "\tbusco: 5.6.1"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "##### Synthèse des résultats"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "| | Complet | Complet unique | Complet dupliqué | Fragmenté | Manquant | Total\n",
    "| :- | :-: | :-: | :-: | :-: | :-: | :-: |\n",
    "Nombre et pourcentage de gènes | 2227 (95.7%) | 2101 (90.3%) | 126 (5.4%) | 15 (0.6%) | 84 (3.7%) | 2242 (96.3%)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Espèces proches"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Huit espèces plus ou moins proches de <i>Centaurea corymbosa</i> possèdent des données génomiques exploitables sur différentes plateformes :\n",
    "\n",
    "![Phylogénie rudimentaires des espèces proches](images/phylogénie.png)\n",
    "\n",
    "Parmi ces espèces, trois ont été annotées : <i>Arctium lappa</i>, <i>Centaurea solstitialis</i> et <i>Cynara cardunculus</i>. Des informations basiques ont été extraites de ces annotations pour permettre une éventuelle comparaison avec nos futures annotations (l'espèce <i>Solanum lycopersicum</i> a été rajoutée car ce sera l'espèce utilisée en paramètre pour l'annotation par AUGUSTUS en <i>ab initio</i>) :\n",
    "\n",
    "| | Nombre de gènes | Ratio mono/multi | Nombre médian d'exons | Nombre max d'exons | [BUSCO](Espèces_proches.md#synthèse-des-résultats) Complet / 2326 | [BUSCO](Espèces_proches.md#synthèse-des-résultats) Manquant / 2326 | [OMArk](Espèces_proches.md#synthèse-des-résultats-1) Attendus / 11 198 | [OMArk](Espèces_proches.md#synthèse-des-résultats-1) Manquant / 11 198\n",
    "| :- | :-: | :-: | :-: | :-: | :-: | :-: | :-: | :-: |\n",
    "<i>Arctium lappa</i> | 46 935 | 0.18 | 3 | 79 | 2 237 (96.1%) | 68 (3%) | 8 988 (80.27%) | 1 068 (9.54%)\n",
    "<i>Centaurea solstitialis</i> | 34 242 | 0.1 | 4 | 79 | 1 604 (69%) | 644 (27.6%) | 7 578 (67.67%) | 2 888 (25.79%)\n",
    "<i>Cynara cardunculus</i> | 30 275 | 0.29 | 3 | 76 | 2 284 (98.2%) | 29 (1.2%) | 8 163 (72.89%) | 361 (3.22%)\n",
    " | | | | | | | | | | \n",
    "<i>Solanum lycopersicum</i> | 31 221 | 0.22 | 3 | 77 | 2 305 (99.1%) | 16 (0.7%) | 8 212 (73.34%) | 176 (1.57%)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "##### [→ Processus et résultats  détaillés ←](Espèces_proches.md)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Masking"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Le masking consiste à masquer les séquences répétées présentes dans le génome (éléments transposables, etc.), car ces séquences peuvent facilement induire les outils de prédiction de gènes en erreur. Les outils de masking vont donc masquer ces séquences, en les remplançant par des X ou des N ou bien en les passant en lettres minuscules au lieu de majuscules, ce qui permettra aux outils d'annotation de pouvoir détecter ces séquences et les ignorer si besoin.\n",
    "\n",
    "L'outil de masking utilisé ici est RepeatMasker (accompagné de RepeatModeler). Une autre approche avec l'outil earlGrey (https://github.com/TobyBaril/EarlGrey) a aussi été essayée, mais a ensuite été abandonnée car elle ne donnait pas de résultats significativement meilleurs."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "##### [→ Processus et résultats  détaillés ←](Masking.md)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Au final, le nombre de bases masquées est de <b>450301573 bp (60.06%)</b> (contre 62.06% pour l'approche avec earlGrey).\n",
    "\n",
    "À titre de comparaison, les génomes des espèces proches sur NCBI ont comme pourcentages de bases masquées (l'espèce <i>Solanum lycopersicum</i> a été rajoutée car ce sera l'espèce utilisée en paramètre pour l'annotation par AUGUSTUS en <i>ab initio</i>) :\n",
    "\n",
    "| Espèce | <i>Arctium lappa</i> | <i>Centaurea solstitialis</i> | <i>Cynara cardunculus</i> | <i>Saussurea involucrata</i> | <i>Carthamus tinctorius</i> | | <i>Solanum lycopersicum</i>\n",
    "| :- | :-: | :-: | :-: | :-: | :-: | :-: | :-: |\n",
    "Pourcentage masqué | 57.14% | 43.3% | 61.38% | 62% | 33.73% | | 34.42%"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Annotation structurelle"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Notre stratégie principale est d'utiliser une game d'outils proposée provenant d'une même équipe :\n",
    "- AUGUSTUS : outil de prédiction de gènes utilisant un modèle de Markov semi-caché. L'outil propose plusieurs espèces en option, chacune avec un modèle paramétré sur mesure\n",
    "\n",
    "\n",
    "- BRAKER1 : pipeline alignant des données RNA-Seq sur le génome pour faire une première annotation qui sera ensuite utilisée pour adapter les paramètres d'AUGUSTUS, qui fera l'annotation finale\n",
    "\n",
    "\n",
    "- BRAKER2 : pipeline alignant des séquences protéiques sur le génome pour faire une première annotation qui sera ensuite utilisée pour adapter les paramètres d'AUGUSTUS, qui fera l'annotation finale\n",
    "\n",
    "\n",
    "- BRAKER3 : pipeline alignant des données RNA-Seq et des séquences protéiques sur le génome pour faire des premières annotations qui seront ensuite utilisées pour adapter les paramètres d'AUGUSTUS, qui fera l'annotation finale\n",
    "\n",
    "\n",
    "- GALBA : pipeline alignant des séquences protéiques sur le génome pour faire une première annotation qui sera ensuite utilisée pour adapter les paramètres d'AUGUSTUS, qui fera ensuite un entraînement itératif puis l'annotation finale\n",
    "\n",
    "<br>\n",
    "\n",
    "Nous n'avons pas de données RNA-Seq pour notre espèce, et le génome fait 750Mbp : il semble donc, selon le schéma de décision suivant, que notre seule option soit d'utiliser BRAKER2 sur les différents sets de protéines que nous avons trouvé.\n",
    "\n",
    "![Schéma de décision pour le choix d'outil](images/braker-decision-scheme.png)\n",
    "\n",
    "Malgré cela, nous avons quand même essayé d'utiliser les autres outils disponibles. Pour cela, nous avons utilisé les données RNA-Seq de quatre espèces proches de notre Centaurée : *Centaurea jacea*, *Centaurea solstitialis*, *Centaurea cyanus* (désormais *Cyanus segetum*) et *Cynara cardunculus*. Nous les avons alignées sur le génome d'abord avec les mêmes paramètres que dans BRAKER, puis avec des paramètres plus permissifs. Nous avons ensuite utilisé ces alignements pour lancer BRAKER1, ainsi que BRAKER3 que nous n'avons lancé qu'avec le meilleur alignement (alignement permissif de <i>Centaurea jacea</i>). Comme le coverage de nos alignements n'était pas très bon, nous avons aussi suivi le conseil du schéma et avons fait une fusion des annotations de BRAKER1 et BRAKER2 grâce à TSEBRA. <br>\n",
    "En plus des autres BRAKER, nous avons aussi essayé GALBA. En effet, même si notre génome est inférieur à 1 Gbp, il fait tout de même 0,75 Gbp, et il se pourrait donc que nous ayons quand même des résultats assez satisfaisants en utilisant GALBA.\n",
    "\n",
    "<b>(--> ajouter ici un schéma de décision modifié avec notre approche en plus)</b>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Une autre stratégie a également été explorée, sur le principe du transfert d’annotation. Le transfert d'annotation consiste à transposer une annotation déjà existante d’une autre espèce proche sur notre génome. Nous avons utilisé l'outil LiftOn, permettant de faire du transfert d'annotation depuis une espèce de référence vers notre espèce en y couplant aussi de l'alignement de protéines afin d'améliorer les résultats par rapport à un simple transfert."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Annotation fonctionnelle"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Pour tenter de déterminer la fonction des gènes trouvés par les outils d'annotation structurelle, plusieurs différents outils ont été utilisés pour faire une annotation fonctionnelle : \n",
    "- EggNOG\n",
    "- InterProScan\n",
    "- GFAP\n",
    "- GOMAP"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "##### [→ Processus détaillé ←](Annotation_fonctionnelle.md)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# AUGUSTUS"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "AUGUSTUS a été lancé en mode *ab initio* (sans données extrinsèques) avec les paramètres statistiques de la tomate (espèce la plus proche de la Centaurée parmi les espèces disponibles dans augustus)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "##### [→ Processus et résultats détaillés ←](AUGUSTUS.md)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "##### Résumé des résultats"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Les mesures les plus pertinentes ont été répertoriées pour chaque annotation dans ce tableau :\n",
    "\n",
    "| | Nombre de gènes | Longueur des séquences codantes | Ratio mono/multi | Nombre médian d'exons | Nombre max d'exons | [BUSCO](AUGUSTUS.md#synthèse-des-résultats) Complets / 2326 | [BUSCO](AUGUSTUS.md#synthèse-des-résultats) Manquants / 2326 | [OMArk](AUGUSTUS.md#synthèse-des-résultats-1) Attendus / 11 198 | [OMArk](AUGUSTUS.md#synthèse-des-résultats-1) Manquants / 11 198\n",
    "| :- | :-: | :-: | :-: | :-: | :-: | :-: | :-: | :-: | :-: |\n",
    "AUGUSTUS | 31 349 | 48 084 066 | 0.17 | 4 | 418 | 2 030 (87.3%) | 185 (7.9%) | 8 987 (80.25%) | 1 362 (12.16%)\n",
    "\n",
    "Les résultats de l'annotation fonctionnelle qui a suivi se trouvent dans ce tableau :\n",
    "\n",
    "| | Nombre de protéines | Protéines annotées par EggNOG | Nombre de hits dans la base EggNOG | Protéines annotées par InterProScan | Nombre de hits dans les bases InterProScan | Protéines annotées par FunAnnotate\n",
    "| :- | :-: | :-: | :-: | :-: | :-: | :-: |\n",
    "AUGUSTUS | 31 349 | 26 504 (84.54%) | 21 011 | 25 864 (82.5%) | 13 295 | 26 711 (85.21%)\n",
    "\n",
    "On remarque que le nombre de protéines est égal au nombre de gènes, ce qui signifie qu'AUGUSTUS seul ne prédit pas d'isoformes pour les gènes qu'il trouve."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# BRAKER1"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "BRAKER1 a été lancé 4 fois avec différentes données RNA-Seq de 4 espèces proches de la Centaurée : <i>Centaurea jacea</i>, <i>Centaurea solstitialis</i>, <i>Centaurea cyanus</i> (qui a été récemment reclassifiée dans un autre genre et renommée en <i>Cyanus segetum</i>) et <i>Cynara cardunculus</i>.\n",
    "\n",
    "Le pipeline BRAKER est initialement fait pour faire de l'annotation par alignement ARN avec des données provenant de la même espèce que le génome, il a donc fallu ici modifier les paramètres de l'aligneur de BRAKER, hisat2 (https://github.com/DaehwanKimLab/hisat2), pour autoriser plus de mismatchs. Il a donc fallu aligner les SRA manuellement au lieu de laisser BRAKER le faire, car il nétait pas possible de modifier les paramètres d'hisat2 depuis la commande BRAKER.\n",
    "\n",
    "Les paramètres modifiés sont les pénalités de mismatch minimum et maximum (option --mp), c'est-à-dire les pénalités soustraites au score d'alignement pour chaque caractère non-alignable (voir documentation).\n",
    "\n",
    "Il est aussi possible de modifier la fonction calculant le score minimum d'alignement requis pour qu'un alignement soit considéré valide (option --score-min), mais cela n'a pas été fait ici."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "##### [→ Processus et résultats  détaillés ←](BRAKER1.md)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "##### Résumé des résultats"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Les mesures les plus pertinentes ont été répertoriées pour chaque annotation dans ce tableau :\n",
    "\n",
    "| | Nombre de gènes | Longueur totale des séquences codantes | Ratio mono/multi | Nombre médian d'exons | Nombre max d'exons | [BUSCO](BRAKER1.md#synthèse-des-résultats) Complets / 2326 | [BUSCO](BRAKER1.md#synthèse-des-résultats) Manquants / 2326 | [OMArk](BRAKER1.md#synthèse-des-résultats-1) Attendus / 11 198 | [OMArk](BRAKER1.md#synthèse-des-résultats-1) Manquants / 11 198\n",
    "| :- | :-: | :-: | :-: | :-: | :-: | :-: | :-: | :-: | :-: |\n",
    "C. solstitialis (standard) | 44 957 | 47 292 072 | 0.41 | 2 | 455 | 2 160 (92.9%) | 128 (5.5%) | 8 798 (78.57%) | 497 (4.44%)\n",
    "C. solstitialis (customisé) | 44 051 | 46 972 632 | 0.34  | 3 | 305 | 2 167 (93.1%) | 128 (5.5%) | 8 790 (78.5%) | 482 (4.3%)\n",
    "C. cyanus (standard) | 40 318 | 46 599 318 | 0.33 | 3 | 277 | 2 091 (89.8%) | 168 (7.3%) | 8 691 (77.62%) | 734 (6.55%)\n",
    "C. cyanus (customisé) | 43 400 | 46 034 604 | 0.38 | 2 | 192 | 2 146 (92.3%) | 130 (5.6%) | 8 767 (78.29%) | 512 (4.57%)\n",
    "C. cardunculus (standard) | 39 883 | 46 858 485 | 0.31 | 3 | 332 | 2 066 (88.8%) | 186 (8%) | 8 648 (77.23%) | 813 (7.26%)\n",
    "C. cardunculus (customisé) | 40 206 | 45 712 875 | 0.35 | 3 | 209 | 2 105 (90.5%) | 160 (6.9%) | 8 650 (77.24%) | 749 (6.69%)\n",
    "C. jacea (standard) | 45 525 | 48 335 499 | 0.33 | 3 | 333 | 2 174 (93.4%) | 122 (5.3%) | 9 535 (85.11%) | 477 (4.26%)\n",
    "C. jacea (customisé) | 47 046 | 48 714 897 | 0.36 | 3 | 270 | 2 175 (93.5%) | 117 (5%) | 9 533 (85.13%) | 497 (4.44%)\n",
    "\n",
    "Les résultats de l'annotation fonctionnelle qui a suivi se trouvent dans ce tableau :\n",
    "\n",
    "| | Nombre de protéines | Protéines annotées par EggNOG | Nombre de hits dans la base EggNOG | Protéines annotées par InterProScan | Nombre de hits dans les bases InterProScan | Protéines annotées par FunAnnotate\n",
    "| :- | :-: | :-: | :-: | :-: | :-: | :-: |\n",
    "C. solstitialis (standard) | 46 710 | 34 294 (73.42%) | 25 686 | 32 815 (70.25%) | 13 961 | 34 664 (75.08%)\n",
    "C. solstitialis (customisé) | 46 190 | 34 864 (75.48%) | 25 747 | 33 356 (72.21%) | 13 982 | 35 226 (76.26%)\n",
    "C. cyanus (standard) | 40 782 | 30 974 (75.95%) | 24 094 | 29 769 (73%) | 13 735 | 31 267 (76.67%)\n",
    "C. cyanus (customisé) | 44 754 | 33 912 (75.77%) | 25 453 | 32 315 (72.21%) | 13 940 | 34 271 (76.58%)\n",
    "C. cardunculus (standard) | 40 205 | 30 571 (76.04%) | 23 882 | 29 333 (72.96%) | 13 669 | 30 858 (76.75%)\n",
    "C. cardunculus (customisé) | 41 219 | 31 409 (76.20%) | 24 190 | 30 298 (73.5%) | 13 753 | 31 711 (76.93%)\n",
    "C. jacea (standard) | 49 148 | 36 232 (73.72%) | 26 018 | 34 642 (70.49%) | 13 975 | 36 646 (74.56%)\n",
    "C. jacea (customisé) | 51 022 | 36 929 (72.38%) | 26 272 | 35 114 (68.82%) | 13 981 | 37 394 (73.29%)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# BRAKER2"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "BRAKER2 a été lancé 4 fois : d'abord sur toutes les protéines d'espèces proches disponibles sur les principales plateformes publiques de stockage de données biologiques (NCBI, UniProt, OrthoDB 10), puis sur les protéines du clade des Eudicotylédons de la base OrthoDB 11 (https://bioinf.uni-greifswald.de/bioinf/partitioned_odb11/), puis sur une combinaison des protéines des Eudicotylédons et de <i>Centaurea solstitialis</i> et une combinaison des protéines des Eudicotylédons et de <i>Cynara cardunculus</i>.\n",
    "\n",
    "La liste des protéines d'espèces proches disponibles selon les espèces et les plateformes peut être trouvée à la fin du document [BRAKER2](BRAKER2.md#protéines-disponibles)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "##### [→ Processus et résultats  détaillés ←](BRAKER2.md)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "##### Résumé des résultats"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Les mesures les plus pertinentes ont été répertoriées pour chaque annotation dans ce tableau :\n",
    "\n",
    "| | Nombre de gènes | Longueur totale des séquences codantes | Ratio mono/multi | Nombre médian d'exons | Nombre max d'exons | [BUSCO](BRAKER2.md#synthèse-des-résultats) Complets / 2326 | [BUSCO](BRAKER2.md#synthèse-des-résultats) Manquants / 2326 | [OMArk](BRAKER2.md#synthèse-des-résultats-1) Attendus / 11 198 | [OMArk](BRAKER2.md#synthèse-des-résultats-1) Manquants / 11 198\n",
    "| :- | :-: | :-: | :-: | :-: | :-: | :-: | :-: | :-: | :-: |\n",
    "Protéines de toutes espèces proches | 58 073 | 66 399 963 | 0.49 | 2 | 307 | 2 215 (95.2%) | 85 (3.6%) | 9 547 (85.26%) | 409 (3.65%)\n",
    "Protéines du clade des Eudicotylédons | 49 699 | 54 509 502 | 0.42 | 2 | 151 | 2 218 (95.3%) | 85 (3.6%) | 9 560 (85.37%) | 415 (3.71%)\n",
    "Eudicotylédons + <i>Cynara cardunculus</i> | 49 991 | 54 246 681 | 0.43 | 2 | 76 | 2 220 (95.5%) | 85 (3.6%) | 9 561 (85.38%) | 415 (3.71%)\n",
    "Eudicotylédons + <i>Centaurea solstitialis</i> | 51 630 | 60 981 762 | 0.44 | 2 | 399 | 2 220 (95.5%) | 85 (3.6%) | 9 555 (85.33%) | 425 (3.8%)\n",
    "\n",
    "Les résultats de l'annotation fonctionnelle qui a suivi se trouvent dans ce tableau :\n",
    "\n",
    "| | Nombre de protéines | Protéines annotées par EggNOG | Nombre de hits dans la base EggNOG | Protéines annotées par InterProScan | Nombre de hits dans les bases InterProScan | Protéines annotées par FunAnnotate\n",
    "| :- | :-: | :-: | :-: | :-: | :-: | :-: |\n",
    "Protéines de toutes espèces proches | 66 028 | 48 448 (73.37%) | 27 938 | 45 899 (69.51%) | 14 107 | 49 208 (74.53%)\n",
    "Protéines du clade des Eudicotylédons | 53 454 | 39 780 (74.42%) | 26 908 | 37 925 (70.95%) | 14 062 | 40 352 (75.49%)\n",
    "Eudicotylédons + <i>Cynara cardunculus</i> | 53 833 | 40 255 (74.77%) | 27 006 | 38 320 (71.18%) | 14 070 | 40 713 (74.63%)\n",
    "Eudicotylédons + <i>Centaurea solstitialis</i> | 57 084 | 43 898 (76.9%) | 27 100 | 41 635 (72.94%) | 14 073 | 44 549 (78.04%)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# BRAKER3"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "BRAKER3 a été lancé 4 fois sur les mêmes ensembles de protéines que [BRAKER2](BRAKER2.md#braker2) (Protéines d'espèces proches, clade des Eudicotylédons, combinaison des Eudicotylédons et de <i>Centaurea solstitialis</i> et combinaison des Eudicotylédons et de <i>Cynara cardunculus</i>) avec le meilleur alignement de RNA-Seq obtenu précédemment pour [BRAKER1](BRAKER1.md#alignements) (alignement customisé de <i>Centaurea jacea</i>).\n",
    "\n",
    "La liste des protéines d'espèces proches disponibles selon les espèces et les plateformes peut être trouvée à la fin du document [BRAKER2](BRAKER2.md#protéines-disponibles)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "##### [→ Processus et résultats  détaillés ←](BRAKER3.md)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "##### Résumé des résultats"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Les mesures les plus pertinentes ont été répertoriées pour chaque annotation dans ce tableau :\n",
    "\n",
    "| | Nombre de gènes | Longueur totale des séquences codantes | Ratio mono/multi | Nombre médian d'exons | Nombre max d'exons | [BUSCO](BRAKER3.md#synthèse-des-résultats) Complets / 2326 | [BUSCO](BRAKER3.md#synthèse-des-résultats) Manquants / 2326 | [OMArk](BRAKER3.md#synthèse-des-résultats-1) Attendus / 11 198 | [OMArk](BRAKER3.md#synthèse-des-résultats-1) Manquants / 11 198\n",
    "| :- | :-: | :-: | :-: | :-: | :-: | :-: | :-: | :-: | :-: |\n",
    "Protéines des espèces proches + RNA-Seq de <i>C. jacea</i> | 28 354 | 36 279 171 | 0.37 | 3 | 79 | 2 240 (96.3%) | 78 (3.4%) | 9 942 (88.78%) | 513 (4.58%)\n",
    "Protéines des Eudicotylédons + RNA-Seq de <i>C. jacea</i> | 26 715 | 34 484 910 | 0.32 | 3 | 79 | 2 239 (96.2%) | 78 (3.4%) | 9 966 (89%) | 515 (4.6%)\n",
    "Protéines des Eudicotylédons et de <i>C. cardunculus</i> + RNA-Seq de <i>C. jacea</i> | 26 818 | 34 525 785 | 0.32 | 3 | 79 | 2 241 (96.3%) | 77 (3.4%) | 9 966 (89%) | 513 (4.58%)\n",
    "Protéines des Eudicotylédons et de <i>C. solstitialis</i> + RNA-Seq de <i>C. jacea</i> | 28 169 | 36 159 267 | 0.36 | 3 | 79 | 2 241 (96.3%) | 77 (3.4%) | 9 963 (88.97%) | 503 (4.49%)\n",
    "\n",
    "Les résultats de l'annotation fonctionnelle qui a suivi se trouvent dans ce tableau :\n",
    "\n",
    "| | Nombre de protéines | Protéines annotées par EggNOG | Nombre de hits dans la base EggNOG | Protéines annotées par InterProScan | Nombre de hits dans les bases InterProScan | Protéines annotées par FunAnnotate\n",
    "| :- | :-: | :-: | :-: | :-: | :-: | :-: |\n",
    "Protéines des espèces proches + RNA-Seq de <i>C. jacea</i> | 33 211 | 31 464 (94.74%) | 22 790 | 30 927 (93.12%) | 13 959 | 31 609 (95.18%)\n",
    "Protéines des Eudicotylédons + RNA-Seq de <i>C. jacea</i> | 30 390 | 29 712 (97.77%) | 22 293 | 29 265 (96.3%) | 13 975 | 29 804 (98.07%)\n",
    "Protéines des Eudicotylédons et de <i>C. cardunculus</i> + RNA-Seq de <i>C. jacea</i> | 30 748 | 30 016 (97.62%) | 22 416 | 29 576 (96.19%) | 13 969 | 30 121 (97.96%)\n",
    "Protéines des Eudicotylédons et de <i>C. solstitialis</i> + RNA-Seq de <i>C. jacea</i> | 32 130 | 30 623 (95.30%) | 22 684 | 30 125 (93.76%) | 13 984 | 30 752 (95.71%)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# GALBA"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "GALBA a été lancé 3 fois : d'abord sur toutes les protéines d'espèces proches trouvées, puis sur les protéines de <i>Cynara cardunculus</i>, puis sur celles de <i>Centaurea solstitialis</i>.\n",
    "\n",
    "La liste des protéines est disponible à la fin du document [BRAKER2](BRAKER2.md#protéines-disponibles)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "##### [→ Processus et résultats  détaillés ←](GALBA.md)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "##### Résumé des résultats"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Les mesures les plus pertinentes ont été répertoriées pour chaque annotation dans ce tableau :\n",
    "\n",
    "| | Nombre de gènes | Longueur totale des séquences codantes | Ratio mono/multi | Nombre médian d'exons | Nombre max d'exons | [BUSCO](GALBA.md#synthèse-des-résultats) Complets / 2326 | [BUSCO](GALBA.md#synthèse-des-résultats) Manquants / 2326 | [OMArk](GALBA.md#synthèse-des-résultats-1) Attendus / 11 198 | [OMArk](GALBA.md#synthèse-des-résultats-1) Manquants / 11 198\n",
    "| :- | :-: | :-: | :-: | :-: | :-: | :-: | :-: | :-: | :-: |\n",
    "Protéines des espèces proches | 53 229 | 68 186 031 | 0.61 | 2 | 79 | 2 216 (95.2%) | 95 (4.2%) | 9 705 (86.67%) | 416 (3.71%)\n",
    "Protéines de <i>Cynara cardunculus</i> | 34 042 | 42 615 537 | 0.35 | 3 | 96 | 2 209 (94.9%) | 97 (4.2%) | 8 786 (78.46%) | 401 (3.58%)\n",
    "Protéines de <i>Centaurea solstitialis</i> | 51 519 | 65 680 875 | 0.49 | 2 | 77 | 2 197 (94.5%) | 108 (4.6%) | 9 701 (86.63%) | 442 (3.95%)\n",
    "\n",
    "Les résultats de l'annotation fonctionnelle qui a suivi se trouvent dans ce tableau :\n",
    "\n",
    "| | Nombre de protéines | Protéines annotées par EggNOG | Nombre de hits dans la base EggNOG | Protéines annotées par InterProScan | Nombre de hits dans les bases InterProScan | Protéines annotées par FunAnnotate\n",
    "| :- | :-: | :-: | :-: | :-: | :-: | :-: |\n",
    "Protéines des espèces proches | 63 017 | 53 550 (84.98%) | 28 190 | 51 655  (91.97%) | 14 192 | 54 464 (86.43%)\n",
    "Protéines de <i>Cynara cardunculus</i> | 39 309 | 37 997 (96.66%) | 26 429 | 36 772 (93.55%) | 14 098 | 38 198 (97.17%)\n",
    "Protéines de <i>Centaurea solstitialis</i> | 61 129 | 52 082 (85.2%) | 28 241 | 50 078 (81.92%) | 14 160 | 52 935 (86.6%)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# TSEBRA"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "TSEBRA a été lancé sur les annotations [BRAKER1](BRAKER1.md) et [BRAKER2](BRAKER2.md) qui paraissaient les plus prometteuses, ici l'annotation BRAKER1 sur <i>Centaurea jacea</i> (alignement customisé) et l'annotation BRAKER2 sur les protéines des Eudicotylédons et de <i>Centaurea solstitialis</i>."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "##### [→ Processus et résultats  détaillés ←](TSEBRA.md)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "##### Résumé des résultats"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Les mesures les plus pertinentes ont été répertoriées pour chaque annotation dans ce tableau :\n",
    "\n",
    "| | Nombre de gènes | Longueur totale des séquences codantes | Ratio mono/multi | Nombre médian d'exons | Nombre max d'exons | [BUSCO](TSEBRA.md#synthèse-des-résultats) Complets / 2326 | [BUSCO](TSEBRA.md#synthèse-des-résultats) Manquants / 2326 | [OMArk](TSEBRA.md#synthèse-des-résultats-1) Attendus / 11 198 | [OMArk](TSEBRA.md#synthèse-des-résultats-1) Manquants / 11 198\n",
    "| :- | :-: | :-: | :-: | :-: | :-: | :-: | :-: | :-: | :-: |\n",
    "BRAKER1 <i>C. jacea</i> et BRAKER2 Eudicotylédons | 51 823 | 55 684 782 | 0.47 | 2 |  151 | 2 225 (95.6%) | 83 (3.6%) | 9 572 (85.48%) | 395 (3.53%)\n",
    "BRAKER1 <i>C. jacea</i> et BRAKER2 Eudicotylédons + <i>C. solstitialis</i> | 54 296 | 62 358 639 | 0.5 | 2 | 399 | 2 226 (95.7%) | 84 (3.6%) | 9 592 (85.57%) | 398 (3.55%)\n",
    "BRAKER1 <i>C. jacea</i> et BRAKER2 Eudicotylédons + <i>C. cardunculus</i> | 52 061 | 55 436 805 | 0.47 | 2 | 76 | 2 227 (95.7%) | 83 (3.6%) | 9 579 (85.54%) | 391 (3.49%)\n",
    "\n",
    "Les résultats de l'annotation fonctionnelle qui a suivi se trouvent dans ce tableau :\n",
    "\n",
    "| | Nombre de protéines | Protéines annotées par EggNOG | Nombre de hits dans la base EggNOG | Protéines annotées par InterProScan | Nombre de hits dans les bases InterProScan | Protéines annotées par FunAnnotate\n",
    "| :- | :-: | :-: | :-: | :-: | :-: | :-: |\n",
    "BRAKER1 <i>C. jacea</i> et BRAKER2 Eudicotylédons | 61 275 | 45 263 (73.87%) | 27 907 | 43 149 (70.42%) | 14 107 | 45 919 (74.94%)\n",
    "BRAKER1 <i>C. jacea</i> et BRAKER2 Eudicotylédons + <i>C. solstitialis</i> | 65 902 | 49 791 (75.55%) | 28 181 | 43 809 (66.48%) | 14 119 | 50 547 (76.7%)\n",
    "BRAKER1 <i>C. jacea</i> et BRAKER2 Eudicotylédons + <i>C. cardunculus</i> | 61 790 | 46 010 (74.46%) | 28 059 | 47 236 (76.49%) | 14 125 | 46 547 (75.33%)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# LiftOn"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "LiftOn permet de faire du transfert d'annotation depuis une espèce de référence vers notre espèce, mais en y couplant aussi de l'alignement de protéines afin d'améliorer les résultats par rapport à un simple transfert. LiftOn utilise l’outil LiftOff (https://github.com/agshumate/Liftoff) pour la partie transfert d’annotation, et l’outil miniprot (https://github.com/lh3/miniprot) pour la partie annotation par alignement de protéines : les résultats des deux outils seront ensuite combinés en une annotation finale."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "##### [→ Processus et résultats  détaillés ←](LiftOn.md)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "##### Résumé des résultats"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Les mesures les plus pertinentes ont été répertoriées pour chaque annotation dans ce tableau :\n",
    "\n",
    "| | Nombre de gènes | Longueur totale des séquences codantes | Ratio mono/multi | Nombre médian d'exons | Nombre max d'exons | [BUSCO](LiftOn.md#synthèse-des-résultats) Complets / 2326 | [BUSCO](LiftOn.md#synthèse-des-résultats) Manquants / 2326 | [OMArk](LiftOn.md#synthèse-des-résultats-1) Attendus / 11 198 | [OMArk](LiftOn.md#synthèse-des-résultats-1) Manquants / 11 198\n",
    "| :- | :-: | :-: | :-: | :-: | :-: | :-: | :-: | :-: | :-: |\n",
    "<i>Cynara cardunculus</i> | 24 033 | 42 343 205 | 0.39 | 3 | 74 | 1 575 (67.8%) | 370 (15.8%) | 7 789 (69.56%) | 1 851 (16.53%)\n",
    "<i>Arctium lappa</i> | 29 405 | 32 415 852 | 0.45 | 3 | 79 | 1 855 (79.8%) | 414 (17.7%) | 9 302 (83.06%) | 1 390 (12.41%)\n",
    "<i>Centaurea solstitialis</i> | 31 142 | 39 045 100 | 0.25 | 3 | 79 | 1 337 (57.4%) | 803 (34.6%) | 7608 (67.95%) | 3 153 (28.16%)\n",
    "\n",
    "Une particularité du transfert d'annotation est que si nos annotations de référence sont complètes (annotation structurelle ET fonctionnelle), il n'y a pas besoin de faire une annotation fonctionnelle, car les annotations de fonction de la référence ont déjà été transférées. <br>\n",
    "Cela pose cependant un problème : les annotations de référence ne suivent pas toutes les mêmes conventions d'annotation fonctionnelle : l'annotation de *Cynara cardunculus*, par exemple, ne comporte d'annotation que les ARNm (ou transcrits). Celle d'*Arctium lappa*, elle, a ses annotations sur les CDS, tandis que celle de *Centaurea soltitialis* est la seule à comporterdes annotations sur les gènes eux-mêmes.\n",
    "\n",
    "Pour chaque annotation, on a donc :\n",
    "- *Cynara cardunculus* : 28 456 ARNm annotés sur 34 249\n",
    "- *Arctium lappa* : 17 193 CDS annotés sur 125 732\n",
    "- *Centaurea solstitialis* : 24 010 gènes annotés sur 31 142\n",
    "\n",
    "Nous avons donc tenté de lancer des outils d'annotations sur nos fichiers malgré les annotations déjà présentes et les incompatibilités de format. Cela n'a donné de résultats concluants que pour *Cynara cardunculus* :\n",
    "\n",
    "| | Nombre de protéines | Protéines annotées par EggNOG | Nombre de hits dans la base EggNOG | Protéines annotées par InterProScan | Nombre de hits dans les bases InterProScan | Protéines annotées par FunAnnotate\n",
    "| :- | :-: | :-: | :-: | :-: | :-: | :-: |\n",
    "<i>Cynara cardunculus</i> | 31 738 | 25 687 (80.93%) | 17 822 | 24 852 (78.3%) | 12 233 | 31 112 (98.03%)\n",
    "~<i>Arctium lappa</i>~ | ~29 407~ | ~25 351 (86.21%)~ | ~19 556~ | ~24 383 (82.92%)~ | ~13 157~ | ~6 105 (20.76%)~\n",
    "~<i>Centaurea solstitialis</i>~ | ~32 521~ | ~27 153 (83.49%)~ | ~17 564~ | ~25 715 (79.07%)~ | ~11 821~ | ~129 (0.4%)~"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Analyses complémentaires"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "##### [→ Processus et résultats  détaillés ←](Autres_analyses.md)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Visualisation et recherche des gènes d'intérêt"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "##### [→ Processus et résultats  détaillés ←](Visualisation_et_recherche_des_gènes_d_intérêt.md) "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Emplacement des données"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Toutes les données utilisées se trouvent sur la machine bioco30 (adresse IP : 162.38.181.30), dans le dossier /home/iliroulet/Data. Les résultats produits se trouvent eux dans le dossier /home/iliroulet/Results.\n",
    "\n",
    "Les commandes ayant été lancées dans un conteneur <i>docker</i>, ces dossiers y sont représentés par /Data et /Results.\n",
    "\n",
    "<br>\n",
    "\n",
    "Dans le dossier Data, on retrouve :\n",
    "- cur_results_463080 : données de la Centaurée issues du projet de séquençage et assemblage\n",
    "- dbs : bases de donnée téléchargées pour les outils utilisés\n",
    "- envs : environnements conda\n",
    "- genomes : génome de la Centaurée (normal et masqué)\n",
    "- other_species : données d'espèces proches\n",
    "- proteins : protéines utilisées dans l'annotation (voir dans [BRAKER2](BRAKER2.md#protéines-disponibles))\n",
    "- rnaseq : données RNA-Seq utilisées dans l'annotation (voir dans [BRAKER1](BRAKER1.md))\n",
    "- scripts : scripts python de manipulation et analyse de données\n",
    "- tools : outils téléchargés\n",
    "\n",
    "Le génome de la Centaurée se trouve ici dans le chemin /Data/genomes/original_genome/centaurea_genome.fasta.\n",
    "\n",
    "<br>\n",
    "\n",
    "Dans le dossier Results, on retrouvera :\n",
    "- busco_genome : résultats de BUSCO sur le génome de la Centaurée\n",
    "- functional_annotation : résultats de l'annotation fonctionnelle\n",
    "- intersections : nombre de gènes communs entre les annotations\n",
    "- masking : résultats du masking\n",
    "- structural_annotation : résultats de l'annotation structurelle\n",
    "- times : temps de lancement des pipelines"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Installation des environnements"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Une partie des outils sont dans des environnements conda, et l'autre partie est installée directement dans le conteneur. Une partie d'entre eux nécessite d'être dans des environnements précis pour être lancés pour leurs dépendances. Tous les environnements sont contenus dans le dossier /Data/envs.\n",
    "\n",
    "On y retrouve :\n",
    "- agat.yaml : environnement contenant l'outil AGAT\n",
    "- braker_env.yaml : environnement contenant les dépendances nécessaires au lancement de BRAKER.pl\n",
    "- busco.yaml : environnement contenant l'outil BUSCO\n",
    "- earlgrey.yaml : environnement contenant l'outil earlGrey\n",
    "- eggNOG-mapper.yaml : environnement contenant l'outil emapper.py\n",
    "- funannotate.yaml : environnement contenant l'outil funannotate\n",
    "- galba_env.yaml : environnement contenant les dépendances nécessaires au lancement de GALBA.pl\n",
    "- genometools.yaml : environnement contenant l'outil genometools\n",
    "- gFACs.yaml : environnement contenant l'outil gFACs\n",
    "- GFAP_env.yaml : environnement contenant les dépendances nécessaires au lancement de GFAP-linux.py\n",
    "- hisat2.yaml : environnement contenant l'outil hisat2\n",
    "- liftoff.yaml : environnement contenant l'outil liftoff\n",
    "- LiftOn_env.yaml : environnement contenant LiftOn (installé avec pip) et les dépendances nécessaires au lancement de lifton\n",
    "- OMArk.yaml : environnement contenant l'outil OMArk (et OMAmer)\n",
    "- repmod.yaml : environnement contenant l'outil RepeatModeler et RepeatMasker\n",
    "- seqkit.yaml : environnement contenant l'outil seqkit\n",
    "- toga_env.yaml : environnement contenant les dépendances nécessaires au lancement de TOGA"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Pour créer tous ces environnements :"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "for file in /Data/envs/*; do\n",
    "    mamba env create -f $file\n",
    "done"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Pour créer tous ces environnements :"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.10"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
