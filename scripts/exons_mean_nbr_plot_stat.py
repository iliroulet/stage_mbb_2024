import sys
import numpy as np
import matplotlib.pyplot as plt

f = open(sys.argv[1])

# Initialize an empty dictionary to store gene information
genes = {}

for line in f:
    # Skip comments
    if line[0] != "#":
        # Split line into columns
        col = line.split("\t")
        # If the line represents a gene
        if col[2] == "gene":
            # Initialize transcript to empty string to keep track
            transcript = ""
            # Store gene attributes in dictionary key
            gene = col[8].strip()
            # Add gene to the dictionary with a count of 0
            genes[gene] = {"gene": 0}
        # If the line represents an exon
        if col[2] == "exon":
            # If no transcript was previously stored for this gene
            if transcript == "":
                transcript = col[8].strip()
                genes[gene] = {transcript: 1}
                # Add transcript to gene with count of 1 exon
            else:
                # If transcript is the same as previous exon
                if col[8].strip() == transcript:
                    genes[gene][transcript] += 1
                    # Increment the count
                else:
                    # If new transcript for same gene
                    transcript = col[8].strip()
                    genes[gene][transcript] = 1
                    # Add new transcript with count of 1 exon

f.close()

nbr = []
mono, multi = 0, 0

for gene in genes.keys():
    # Calculate the average number of exons per gene
    nbr.append(round(np.mean(list(genes[gene].values()))))

# Configure the plot appearance
with plt.rc_context(
    {
        "axes.titlesize": "xx-large",
        "axes.labelsize": "xx-large",
        "text.color": "white",
        "axes.labelcolor": "white",
        "axes.facecolor": "black",
        "axes.edgecolor": "black",
        "xtick.color": "white",
        "ytick.color": "white",
        "figure.facecolor": "black",
    }
):
    # Set figure size
    plt.rcParams["figure.figsize"] = [15, 10]

    # Plot a histogram of exon lengths with 50 bins
    plt.hist(nbr, bins=50, color="b")
    plt.title(
        "Nombre de gènes présents sur le génome selon leur nombre d'exons "
        "(moyenne sur tous les transcrits)",
        c="white",
    )
    plt.xlabel("Nombre d'exons")
    plt.ylabel("Nombre de gènes")
    plt.xlim(0, max(nbr) + 1)
    # Set y-axis to logarithmic scale for better visualization of the data distribution
    plt.yscale("log")
    # Add vertical line at mean exon length
    plt.axvline(np.mean(nbr), color="r")
    # Save histogram to location specified by user 
    plt.savefig(
        sys.argv[2] + "/exons_mean_nbr_histogram.png",
        format="png",
        bbox_inches="tight",
    )

# Count the number of mono- and multi-exonic genes
for n in nbr:
    if n == 1:
        mono += 1
    if n > 1:
        multi += 1

t = open(sys.argv[2] + "/exons_mean_nbr_stat.txt", "w")

# Write statistics about the distribution of exons across genes
t.write("Number of transcripts: {}\n".format(len(genes.keys())))
t.write("Monoexonic transcripts: {}\n".format(mono))
t.write("Multiexonic transcripts: {}\n".format(multi))
t.write("Mono:Mult Ratio: {}\n".format(round(mono / multi, 2)))
t.write("Boxplot of number of exons per transcript:\n")
t.write("Min: {}\n".format(min(nbr)))
t.write("25%: {}\n".format(int(np.percentile(nbr, 25))))
t.write("50%: {}\n".format(int(np.percentile(nbr, 50))))
t.write("75%: {}\n".format(int(np.percentile(nbr, 75))))
t.write("Max: {}\n".format(max(nbr)))

t.close()
