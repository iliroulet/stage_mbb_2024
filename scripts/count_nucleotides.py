import sys

f = open(sys.argv[1])

c = 0

for line in f:
    # If line is not a header
    if line[0] != ">":
        for car in line:
            if car in ['a', 't', 'g', 'c', 'n', 'A', 'T', 'G', 'C', 'N']:
                c += 1

print(c)

f.close()