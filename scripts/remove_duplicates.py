import sys

f = open(sys.argv[1])
n = open(sys.argv[2], "w")

# Initialize a flag to control writing to the output file
write = True
# Initialize an empty dictionary to store accession numbers
accs = {}

for line in f:
    # If line is not a header and the write flag is on
    if line[0] != ">" and write == True:
        # Write line
        n.write(line)
    # If line is a header
    if line[0] == ">":
        # Extract accession number
        acc = line.split("\t")[0].split(" ")[0][1:]
        # Check if accession number has already occured
        if acc in accs.keys():
            # If yes, set write flag to False to prevent writing of the sequence
            write = False
        else:
            # If not, add the accession number to dictionary
            accs[acc] = None
            # Write header to the output file
            n.write(line)
            # Set write flag to True to write the sequence
            write = True

f.close()
n.close()
