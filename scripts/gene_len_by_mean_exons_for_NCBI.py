import sys
import numpy as np
import matplotlib.pyplot as plt

f = open(sys.argv[1])

# Initialize an empty dictionary to store gene information
genes = {}

for line in f:
    # Skip comments
    if line[0] != "#":
        # Split line into columns
        col = line.split("\t")
        # If the line represents a gene
        if col[2] == "gene":
            # Initialize transcript to empty string to keep track
            transcript = ""
            # Store gene length (substract start from end) as gene
            gene = int(col[4]) - int(col[3])
            # Add the gene to the dictionary with a count of 0
            genes[gene] = {"gene": 0}
        # If the line represents an exon
        if col[2] == "exon":
            # If no transcript was previously stored for this gene
            if transcript == "":
                transcript = col[8].split(";")[1]
                genes[gene] = {transcript: 1}
                # Add transcript to gene with count of 1 exon
            else:
                # If transcript is the same as previous exon
                if col[8].split(";")[1] == transcript:
                    genes[gene][transcript] += 1
                    # Increment the count
                else:
                    # If new transcript for same gene
                    transcript = col[8].split(";")[1]
                    genes[gene][transcript] = 1
                    # Add new transcript with count of 1 exon

f.close()

for gene in genes.keys():
    # Calculate the average number of exons per gene
    genes[gene] = round(np.mean(list(genes[gene].values())))

# Configure the plot appearance
with plt.rc_context(
    {
        "axes.titlesize": "xx-large",
        "axes.labelsize": "xx-large",
        "text.color": "white",
        "axes.labelcolor": "white",
        "axes.facecolor": "black",
        "axes.edgecolor": "black",
        "xtick.color": "white",
        "ytick.color": "white",
        "figure.facecolor": "black",
    }
):
    # Set figure size
    plt.rcParams["figure.figsize"] = [15, 10]

    # Plot a scatter plot of gene size by average number of exons
    plt.scatter(genes.keys(), genes.values(), alpha=0.5, c="b", s=list(genes.values()))
    plt.title(
        "Taille des gènes prédits selon le nombre d'exons présents "
        "(moyenne sur tous les transcrits)",
        c="white",
    )
    plt.xlabel("Taille des gènes")
    plt.ylabel("Nombre moyen d'exons")
    plt.xlim(0, max(genes.keys()) + 1)
    # Add horizontal and vertical lines representing the mean values
    plt.axhline(np.mean(list(genes.values())), color="r")
    plt.axvline(np.mean(list(genes.keys())), color="r")
    # Save scatter plot to location specified by user
    plt.savefig(
        sys.argv[2] + "/gene_len_by_mean_exons_scatterplot.png",
        format="png",
        bbox_inches="tight",
    )
