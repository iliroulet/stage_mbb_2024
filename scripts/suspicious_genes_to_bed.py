import sys
import numpy as np

f = open(sys.argv[1])

# Initialize an empty dictionary to store gene data
genes = {}

for line in f:
    # Skip headers
    if line[0] != "#":
        # Split line into columns
        col = line.split("\t")
        # If the line represents a gene
        if col[2] == "gene":
            # Initialize transcript to empty string to keep track
            transcript = ""
            # Store gene start and end, scaffold and name in dictionary key
            gene = ((int(col[3]), int(col[4])), (col[0], col[8].strip()))
            genes[gene] = {"gene": 0}
        # If the line represents an exon
        if col[2] == "exon":
            # If no transcript was previously stored for this gene
            if transcript == "":
                transcript = col[8].strip()
                genes[gene] = {transcript: 1}
                # Add transcript to gene with count of 1 exon
            else:
                # If transcript is the same as previous exon
                if col[8].strip() == transcript:
                    genes[gene][transcript] += 1
                    # Increment the count
                else:
                    # If new transcript for same gene
                    transcript = col[8].strip()
                    genes[gene][transcript] = 1
                    # Add new transcript with count of 1 exon

f.close()

# Initialize an empty list to store suspicious genes
sus = []

for gene in genes.keys():
    # Calculate the average number of exons for the gene
    nbr_exons = round(np.mean(list(genes[gene].values())))
    # If average number of exons is greater than 80, consider the gene suspicious
    if nbr_exons > 80:
        # Add the gene and average number of exons to the list
        sus.append((gene, nbr_exons))

t = open(sys.argv[2] + "/suspicious_genes.bed", "w")
# Write comment with gene length, number of exons and gene name
# then gene information in BED format
for pair in sus:
    t.write(
        "#{}\n{}\t{}\t{}\n".format(
            str(pair[0][0][1] - pair[0][0][0])
            + "_nucleotides:"
            + str(pair[1])
            + "_exons:"
            + str(pair[0][1][1]),
            pair[0][1][0],
            pair[0][0][0] - 1,
            pair[0][0][1] - 1,
        )
    )
t.close()
