import sys

f = open(sys.argv[1])
n = open("annotations_sources.txt", "w")

# Initialize an empty list to store sources
sources = []

for line in f:
    # Skip comments
    if line[0] != "#":
        # Split line into columns
        cols = line.split("\t")
        # Check if second column (source) is not already in the list
        if cols[1] not in sources:
            # If not, add to list
            sources.append(cols[1])

# Write list to output file
n.write(str(sources))

n.close()
f.close()
