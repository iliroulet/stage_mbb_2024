import sys

r = open(sys.argv[1])
n = open(sys.argv[2], "w")

for line in r:
    # If line is not a header, convert it to uppercase and write it to output file
    if line[0] != ">":
        n.write(line.upper())
    # If line is a header, write it to output file without modification
    if line[0] == ">":
        n.write(line)

r.close()
n.close()
