import sys
import numpy as np
import matplotlib.pyplot as plt

# Initialize an empty list to store gene lengths
lens = []

f = open(sys.argv[1])

for line in f:
    # Skip comments
    if line[0] != "#":
        # Split line into columns
        col = line.split("\t")
        # If the line represents a gene
        if col[2] == "gene":
            # Calculate gene length (subtracting start position from end position)
            len = int(col[4]) - int(col[3])
            # Append length to list of lengths
            lens.append(len)

f.close()

# Configure the plot appearance
with plt.rc_context(
    {
        "axes.titlesize": "xx-large",
        "axes.labelsize": "xx-large",
        "text.color": "white",
        "axes.labelcolor": "white",
        "axes.facecolor": "black",
        "axes.edgecolor": "black",
        "xtick.color": "white",
        "ytick.color": "white",
        "figure.facecolor": "black",
    }
):
    # Set figure size
    plt.rcParams["figure.figsize"] = [15, 10]

    # Plot a histogram of gene lengths with 50 bins
    plt.hist(lens, bins=50, color="mediumseagreen")
    plt.title("Nombre de gènes présents sur le génome selon leur taille", c="white")
    plt.ylabel("Nombre de gènes")
    plt.xlabel("Taille des gènes")
    plt.xlim(0, max(lens) + 1)
    # Set y-axis to logarithmic scale for better visualization of the data distribution
    plt.yscale("log")
    # Add vertical line at mean gene length
    plt.axvline(np.mean(lens), color="r")
    # Save histogram to location specified by user 
    plt.savefig(
        sys.argv[2] + "/gene_length_histogram.png", format="png", bbox_inches="tight"
    )
