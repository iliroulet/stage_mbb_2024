f = open("softmasked.count")

# Initialize counters for number masked, unmasked and total characters
maj, min, full = 0, 0, 0

for line in f:
    for car in line:
        # If the character is not a newline character, increment total counter
        if car != "\n":
            full += 1
        # If the character is an uppercase nucleotide, increment unmasked counter
        if car in ["A", "T", "G", "C", "N"]:
            maj += 1
        # If the character is a lowercase nucleotide, increment masked counter
        if car in ["a", "t", "g", "c", "n"]:
            min += 1

# Print total number of characters
print("Full length:", full)
# Calculate and print number and percentage of masked characters
print("Masked:", min, "pb =", min / full * 100, "%")
# Calculate and print number and percentage of unmasked characters
print("Not masked:", maj, "pb =", maj / full * 100, "%")

f.close()
