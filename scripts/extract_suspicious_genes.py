import sys
import numpy as np

f = open(sys.argv[1])

# Initialize an empty dictionary to store gene information
genes = {}

for line in f:
    # Skip comments
    if line[0] != "#":
        # Split line into columns
        col = line.split("\t")
        # If the line represents a gene
        if col[2] == "gene":
            # Initialize transcript to empty string to keep track
            transcript = ""
            # Store the current line as the gene
            gene = line
            # Add the gene to the dictionary with a count of 0
            genes[gene] = {"gene": 0}
        # If the line represents an exon
        if col[2] == "exon":
            # If no transcript was previously stored for this gene
            if transcript == "":
                transcript = col[8].strip()
                genes[gene] = {transcript: 1}
                # Add transcript to gene with count of 1 exon
            else:
                # If transcript is the same as previous exon
                if col[8].strip() == transcript:
                    genes[gene][transcript] += 1
                    # Increment the count
                else:
                    # If new transcript for same gene
                    transcript = col[8].strip()
                    genes[gene][transcript] = 1
                    # Add new transcript with count of 1 exon

f.close()

n = open(sys.argv[2], "w")

for gene in genes.keys():
    # Calculate the average number of exons for the gene
    nbr_exons = round(np.mean(list(genes[gene].values())))
    # If average number of exons is greater than 80, write the gene in the new file
    if nbr_exons > 80:
        n.write(gene)

n.close()
