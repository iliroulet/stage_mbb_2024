import sys

f = open(sys.argv[1])

genes = {}

for line in f:
    # Skip comments
    if line[0] != "#":
        # Split line into comments
        col = line.split("\t")
        # If line represents a gene
        if col[2] == "gene":
            # Initialize or reset the transcript variable as empty string
            transcript = ""
            # Store gene ID from 9th column as gene
            gene = col[8].strip()
        # If line represents an exon
        if col[2] == "exon":
            # If transcript is empty
            if transcript == "":
                transcript = col[8].split(";")[1]
                genes[gene] = 1
                # Extract transcript ID from 9th column and add gene to dictionary 
                # with a count of 1 transcript
            else:
                # If the transcript ID changes, increment transcript count
                if col[8].split(";")[1] != transcript:
                    transcript = col[8].split(";")[1]
                    genes[gene] += 1

f.close()

# Print values of dictionary : number of transcripts per gene
print(genes.values())
