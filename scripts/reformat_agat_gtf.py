import sys

f = open(sys.argv[1])
n = open(sys.argv[2], "w")

for line in f:
    # Skip comments
    if line[0] != "#":
        # Split line into columns
        cols = line.strip().split("\t")
        # Split the attributes column (9th column) into different attributes
        attributes = cols[8].split(";")
        # If line represents gene or transcript
        if cols[2] == "gene" or cols[2] == "transcript":
            n.write(
                "{}\t{}\n".format("\t".join(cols[0:-1]), attributes[0].split("=")[1])
            )
            # Write first 8 columns of line, followed by first attribute of 9th column
        else:
            n.write(
                '{}\ttranscript_id "{}"; gene_id "{}";\n'.format(
                    "\t".join(cols[0:-1]),
                    attributes[3].split("=")[1],
                    attributes[2].split("=")[1],
                )
            )
            # For other lines, write first 8 columns, followed by two of 9th column's 
            # attributes formatted as "transcript_id" and "gene_id"

n.close()
f.close()
