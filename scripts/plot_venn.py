import sys
import matplotlib.pyplot as plt
from matplotlib_venn import venn2

# Configure the plot appearance
with plt.rc_context({"text.color": "white"}):
    venn2(
        # Define the sizes of the subsets (intersection, set1, set2)
        subsets=(int(sys.argv[2]), int(sys.argv[4]), int(sys.argv[5])),
        # Label sets using first two command-line arguments (name and number of genes)
        set_labels=(
            "{}\n{}".format(sys.argv[1].split(":")[0], sys.argv[1].split(":")[1]),
            "{}\n{}".format(sys.argv[3].split(":")[0], sys.argv[3].split(":")[1]),
        ),
        set_colors=("blue", "red"),
        # Format labels for subsets to include their size and percentage of the total
        subset_label_formatter=lambda x: str(x)
        + "\n("
        + f"{(x/(int(sys.argv[2]) + int(sys.argv[4]) + int(sys.argv[5]))):1.0%}"
        + ")",
    )
    # Save the plot as PNG file with a black background
    plt.savefig("venn_diagram.png", format="png", facecolor="black")
