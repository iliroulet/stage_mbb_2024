import sys

f = open(sys.argv[1])
n = open(sys.argv[2], "w")

for line in f:
    # Skip comments
    if line[0] != "#":
        # Split line into comments
        cols = line.split("\t")
        # If line represents a gene
        if cols[2] in ["gene"]:
            # Write line to output file
            n.write(line)

n.close()
f.close()
