import sys

f = open(sys.argv[1])
n = open(sys.argv[1] + ".splice", "w")

# Initialize an empty list to store headers
headers = []

for line in f:
    # Check if line is header
    if line[0] == ">":
        headers.append(line[1:-1])
        # Add sequence name to list (header without ">" and newline)
f.close()

# Initialize variables to keep track of previous annotation and gene
prev_anno, prev_gene = "", ""

for header in headers:
    # Split header into annotation and gene parts
    anno = header.split(".")[0]
    gene = header.split(".")[1]
    # If current gene and annotation match previous ones
    if gene == prev_gene and anno == prev_anno:
        n.write(";{}".format(header))
        # append gene name to previous line (separate with semicolon)
    # If the gene or annotation changed, start a new line of gene names
    if gene != prev_gene or anno != prev_anno:
        if prev_gene == "":
            n.write(header)
        else:
            n.write("\n{}".format(header))
    # Update previous gene and annotation for next iteration
    prev_gene, prev_anno = gene, anno

n.close()
