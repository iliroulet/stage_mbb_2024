import sys

f = open(sys.argv[1])
n = open(sys.argv[2], "w")

for line in f:
    # If line is not a header
    if line[0] != ">":
        # Write line
        n.write(line)
    # If line is a header
    if line[0] == ">":
        # Extract accession number
        acc = line.split(" ")[0]
        n.write(acc+"\n")

f.close()
n.close()
