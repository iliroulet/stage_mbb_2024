import os
import sys


# Execute a bedtools getfasta command to extract sequences from a FASTA file
# based on coordinates in a BED file
os.system(
    "bedtools getfasta \
          -fi /Data/genomes/original_genome/centaurea_genome.fasta \
          -bed suspicious_genes.bed -fo no_exon_suspicious_genes.fasta"
)
# The sequences are saved in a new FASTA file named "no_exon_suspicious_genes.fasta"

# Initialize an empty list to store additional information for FASTA headers
nbrs = []

f = open("suspicious_genes.bed")

# If the line in the BED file is a comment, append the rest of the line to the list
for line in f:
    if line[0] == "#":
        nbrs.append(line[1:])

f.close()

o = open("no_exon_suspicious_genes.fasta")
n = open("suspicious_genes.fasta", "w")
i = 0

for line in o:
    # If the line is a header
    if line[0] == ">":
        n.write(line.strip() + ":" + nbrs[i])
        i += 1
        # Add additional information from the list to the header in the new file
    else:
        n.write(line)
        # If not a comment, write the line as is

o.close()
n.close()
