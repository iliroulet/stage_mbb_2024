import sys
import numpy as np

# Initialize an empty list to store gene lengths
lens = []

f = open(sys.argv[1])

for line in f:
    # Skip comments
    if line[0] != "#":
        # Split line into columns
        col = line.split("\t")
        # If the line represents a gene
        if col[2] == "gene":
            # Calculate gene length (subtracting start position from end position)
            len = int(col[4]) - int(col[3])
            # Append length to list of lengths
            lens.append(len)

f.close()

n = open(sys.argv[2]+"/gene_mean_len.txt", "w")
n.write(str(np.mean(lens)))
n.close()