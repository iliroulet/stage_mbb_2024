import sys

f = open(sys.argv[1])
n = open(sys.argv[1] + ".splice", "w")

# Initialize an empty list to store headers
headers = []

for line in f:
    # Check if line is header
    if line[0] == ">":
        headers.append(line[1:-1])
        # Add sequence name to list (header without ">" and newline)
f.close()

# Initialize a variable to keep track of previous gene
prev_gene = ""

for header in headers:
    # Extract gene name from the header
    gene = header.split(".")[0]
    # If current gene matches previous one,
    if gene == prev_gene:
        n.write(";{}".format(header))
        # append gene name to previous line (separate with semicolon)
    # If the gene changed, start a new line of gene names
    if gene != prev_gene:
        # If this is the first gene of file, write the header directly
        if prev_gene == "":
            n.write(header)
        # If this is not the first gene of file, add a newline before writing
        else:
            n.write("\n{}".format(header))
    # Update previous gene for next iteration
    prev_gene = gene

n.close()
