f = open("uniprotkb_swissprot")
n = open("swissprot_map.txt", "w")

for line in f:
    # If line is a header
    if line[0] == ">":
        # Split the line into elements based on spaces
        els = line.split(" ")
        # Extract the sequence identifier (without ">")
        seq = els[0][1:]
        # Iterate over each element in the split line
        for el in els:
            # Check if element starts with "OX=", indicating taxonomy ID
            if el[0:3]=="OX=":
                # Extract taxonomy ID (removing the "OX=")
                taxid = el[3:]
    # Write the sequence identifier and taxonomy ID to new file, separated by a tab
    n.write("{}\t{}\n".format(seq, taxid))

f.close()
n.close()