# Sommaire


- [Lancement](#commandes)

- [Résultats](#fichiers-obtenus)

    - [Statistiques basiques](#fichiers-obtenus-1)
    
    - [Statistiques avec gFACs](#fichiers-obtenus-2)
    
    - [Analyse avec BUSCO](#synthèse-des-résultats)
    
    - [Analyse avec OMArk](#synthèse-des-résultats-1)


# BRAKER3


BRAKER3 a été lancé 4 fois sur les mêmes ensembles de protéines que [BRAKER2](BRAKER2.md#braker2) (Protéines d'espèces proches, clade des Eudicotylédons, combinaison des Eudicotylédons et de <i>Centaurea solstitialis</i> et combinaison des Eudicotylédons et de <i>Cynara cardunculus</i>) avec le meilleur alignement de RNA-Seq obtenu précédemment pour [BRAKER1](BRAKER1.md#alignements) (alignement customisé de <i>Centaurea jacea</i>).

La liste des protéines d'espèces proches disponibles selon les espèces et les plateformes peut être trouvée à la fin du document [BRAKER2](BRAKER2.md#protéines-disponibles).


##### Documentation
https://github.com/Gaius-Augustus/BRAKER


![Schéma complet du pipeline BRAKER3](images/figures_pipelines/braker3-full.png)


##### Commandes

```python
# activer l'environnement contenant les dépendances de braker
mamba activate braker_env
# exporter les outils d'alignement utilisés par GeneMark dans le PATH
export PATH=$PATH:/Data/tools/GeneMark-ETP/tools:/Data/tools/compleasm_kit
# Lancer BRAKER2 sur toutes les protéines et SRA trouvés sur le gène masqué
{ time /Data/tools/BRAKER/scripts/braker.pl \
	--workingdir=/Results/structural_annotation/BRAKER3_$proteinSet_rnaSet \
	--genome=/Data/genomes/masked_genome/centaurea_genome.fasta.masked \
	--bam=/Data/rnaseq/$species/fastq/$SRA_ID".bam" \
	--prot_seq=/Data/proteins/$proteins_fasta \
	--busco_lineage=eudicots_odb10 \
	--threads=15 \
	--AUGUSTUS_CONFIG_PATH=/Data/tools/Augustus/config/ \
	--AUGUSTUS_BIN_PATH=/Data/tools/Augustus/bin/ \
	--AUGUSTUS_SCRIPTS_PATH=/Data/tools/Augustus/scripts/ \
	--COMPLEASM_PATH=/Data/tools/compleasm_kit/ \
	--GENEMARK_PATH=/Data/tools/GeneMark-ETP/bin/ \
	--PROTHINT_PATH=/Data/tools/ProtHint/bin/ \
	--BAMTOOLS_PATH=/Data/tools/bamtools/src/   \
	--TSEBRA_PATH=/Data/tools/TSEBRA/bin/ ; } 2> /Results/times/BRAKER3_$proteinSet_$rnaSet_time.txt
```

```python
# on transforme l'annotation obtenue en format GFF3 pour pouvoir l'utiliser plus tard avec funannotate
cat braker.gtf | perl -ne 'if(m/\tAUGUSTUS\t/ or m/\tGeneMark\.hmm\t/ or m/\tGeneMark\.hmm3\t/ or m/\tgmst\t/) {print $_;}' | gtf2gff.pl --gff3 --out=braker.gff3
```

##### Fichiers obtenus


- Augustus : résultats de l'annotation brute par Augustus
- bbc : résultats des comparaisons BUSCO de compleasm
- best_by_compleasm.log : fichier de log de compleasm
- braker.aa : séquences protéiques de l'annotation
- braker.codingseq : séquences codantes de l'annotation
- braker.gtf : annotation
- braker.log : fichier de log
- braker_original
- errors : sorties d'erreur
- GeneMark-ETP : résultats de l'annotation brute par GeneMark-ETP
- genome_header.map : liste des headers du génome
- hintsfile.gff : fichier GFF contenant les hints
- species : paramètres d'espèces (personnalisés grâce à l'annotation par GeneMark) utilisés par Augustus
- what-to-cite.txt : citations du pipeline et de ses différents outils


##### Hints


Après avoir fait un alignement de protéines pour faire une première annotation avec GeneMark, des hints sont extraits de cette annotation.

| | Protéines d'espèces proches + RNA-Seq de <i>C. jacea</i> | Protéines du clade des Eudicotylédons (OrthoDB 11) + RNA-Seq de <i>C. jacea</i> | Protéines du clade des Eudicotylédons et de <i>C. cardunculus</i> + RNA-Seq de <i>C. jacea</i> | Protéines du clade des Eudicotylédons et de <i>C. solstitialis</i> + RNA-Seq de <i>C. jacea</i> |
| :- | :-: | :-: | :-: | :-: |
Nombre de hints | 1 008 098 | 919 466 | 939 881 | 979 222


## Analyse


### Statistiques de base


##### Commandes

```python
grep -c ">" braker.aa > nb_of_prot_seq.txt
```

```python
# Ploter l'histogramme du nombre d'exons trouvés selon leur taille
python3 /Data/scripts/exons_len_plot.py braker.gtf .
# Ploter l'histogramme du nombre de gènes trouvés selon leur taille
python3 /Data/scripts/gene_len_plot.py braker.gtf .
# Ploter l'histogramme du nombre de transcrits trouvés selon leur taille
python3 /Data/scripts/transcripts_len_plot.py braker.gtf .
# Ploter l'histogramme du nombre de gènes selon leur nombre d'exons (moyenne sur tous les transcrits)
python3 /Data/scripts/exons_mean_nbr_plot_stat.py braker.gtf .
# Ploter le scatter plot de la taille de gènes par leur nombre moyen d'exons
python3 /Data/scripts/gene_len_by_mean_exons.py braker.gtf .
```

```python
mkdir plots
mv *.png plots
mkdir stats
mv *n*.txt stats
```

##### Fichiers obtenus


- exons_length_histogram.png : histogramme du nombre d'exons trouvés selon leur taille
- exons_mean_nbr_histogram.png : histogramme du nombre de gènes selon leur nombre d'exons (moyenne sur tous les transcrits trouvés pour le gène)
- exons_mean_nbr_stat.txt : statistiques sur les exons par gènes (moyenne sur tous les transcrits)
- gene_len_by_mean_exons_scatterplot.png : scatter plot de la taille de gènes par leur nombre moyen d'exons
- gene_length_histogram.png : histogramme du nombre de gènes trouvés selon leur taille
- nb_of_prot_seq.txt : nombre de séquences protéiques extraites de l'annotation
- transcripts_length_histogram.png : histogramme du nombre de transcrits trouvés selon leur taille


Ces résultats ont été synthétisés en tableaux :

| | Protéines d'espèces proches + RNA-Seq de <i>C. jacea</i> | Protéines du clade des Eudicotylédons (OrthoDB 11) + RNA-Seq de <i>C. jacea</i> | Protéines du clade des Eudicotylédons et de <i>C. cardunculus</i> + RNA-Seq de <i>C. jacea</i> | Protéines du clade des Eudicotylédons et de <i>C. solstitialis</i> + RNA-Seq de <i>C. jacea</i> |
| :- | :-: | :-: | :-: | :-: |
Nombre de protéines trouvées | 33 211 | 30 390 | 30 748 | 32 130

<br>

| | Protéines d'espèces proches + RNA-Seq de <i>C. jacea</i> | Protéines du clade des Eudicotylédons (OrthoDB 11) + RNA-Seq de <i>C. jacea</i> | Protéines du clade des Eudicotylédons et de <i>C. cardunculus</i> + RNA-Seq de <i>C. jacea</i> | Protéines du clade des Eudicotylédons et de <i>C. solstitialis</i> + RNA-Seq de <i>C. jacea</i> |
| :- | :-: | :-: | :-: | :-: |
Nombre de gènes | 28 354 | 26 715 | 26 818 | 28 169
Nombre de gènes mono-exoniques | 7 680 | 6 517 | 6 571 | 7 506
Nombre de gènes multi-exoniques | 20 674 | 20 198 | 20 247 | 20 663
Ratio mono/multi | 0.37 | 0.32 | 0.32 | 0.36
Nombre médian d’exons | 3 | 3 | 3 | 3
Nombre maximum d’exons | 79 | 79 | 79 | 79


### gFACs


##### Commandes


gFACs a une option format (-f) pour renseigner le format du fichier GTF donné en entrée. Pour savoir le format à renseigner, on peut utiliser le script de support format_diagnosis.pl (https://gfacs.readthedocs.io/en/latest/Bonus/format_diagnosis.html). Ce script renvoie une suite de valeurs que l'on peut ensuite aller comparer au tableau de formats de la documentation gFACs (https://gfacs.readthedocs.io/en/latest/_images/Table.png) pour déterminer quel format gFACs correspond au nôtre, ou au moins lequel en est le plus proche.

```python
cd /Results/structural_annotation/BRAKER3_$proteinSet_rnaSet
perl /opt/biotools/conda/envs/gFACs/opt/perl-gfacs-1.1.1/support_scripts/format_diagnosis.pl braker.gtf
```

Le format correspondant au fichier gtf obtenu est braker_2.0_gtf.

```python
mkdir gFACs
mamba activate gFACs
gFACs.pl -f braker_2.0_gtf --statistics --fasta /Data/genomes/original_genome/centaurea_genome.fasta --splice-table --nt-content \
	--get-fasta --get-protein-fasta --create-gff3 --distributions exons_lengths intron_lengths CDS_lengths gene_lengths \
	exon_position exon_position_data intron_position intron_position_data -O gFACs braker.gtf
```

##### Fichiers obtenus


- CDS_lengths_distributions.tsv : distribution des longueurs des séquences codantes
- data_exon_position_distributions.tsv : liste des positions des exons ?
- data_intron_position_distributions.tsv : liste des positions des exons ?
- exon_position_distributions.tsv : distribution de la position des exons
- gene_lengths_distributions.tsv : distribution des longueurs des gènes
- genes.fasta : liste des gènes utilisés par gFACs pour ses statistiques
- genes.fasta.faa : liste des gènes utilisés par gFACs pour ses statistiques accompagnés de certaines séquences protéiques
- gene_table.txt : table de gènes crées par gFACs pour ses statistiques
- gFACs_log.txt : fichier de log
- intron_lengths_distributions.tsv : distribution des longueurs des introns
- intron_position_distributions.tsv : distribution de la position des introns
- out.gff3 : fichier contenant les éléments étudiés par gFACs (mRNA, exons, ...)
- statistics.txt : statistiques produites par gFACs


Le résumé des statistiques obtenues se trouve dans deux tableaux de synthèse :

![1er tableau de synthèse de gFACs](images/gFACs/BRAKER3_gFACs_1.svg)

![2ème tableau de synthèse de gFACs](images/gFACs/BRAKER3_gFACs_2.svg)


### BUSCO


L'outil BUSCO permet de faire une recherche des séquences codantes trouvées dans la base de données orthoDB (https://www.orthodb.org/) d'une lignée afin de voir si les gènes prédits y sont retrouvés. La lignée sélectionnée ici est la lignée eudicots_odb10.


##### Documentation
https://busco.ezlab.org/busco_userguide.html


##### Commandes


Pour lancer BUSCO en mode transcriptome, il faut d'abord faire en sorte de ne sélectionner qu'un isoforme (= transcrit) par gène. On utilise donc AGAT pour ne garder que les plus isoformes aux plus longs CDS.

```python
cd /Results/structural_annotation/BRAKER3_$proteinSet_rnaSet
mamba activate agat
mkdir without_isoforms
agat_sp_keep_longest_isoform.pl -gff braker.gtf -o without_isoforms/agat_without_isoforms.gtf
```

Il faut ensuite reformater le fichier produit par AGAT pour qu'il corresponde à un format Augustus, ce qui nous permettra de lancer ensuite le script d'Augustus permettat d'obtenir les séquences codantes et protéiques d'une annotation.

```python
cd without_isoforms
python3 /Data/scripts/reformat_agat_gtf.py agat_without_isoforms.gtf without_isoforms.gtf
/Data/tools/Augustus/scripts/getAnnoFastaFromJoingenes.py -g /Data/genomes/masked_genome/centaurea_genome.fasta.masked \
	-o without_isoforms -f without_isoforms.gtf
```

Le script produit deux fichiers : without_isoforms.aa et without_isoforms.codingseq.

On peut enfin lancer BUSCO sur le fichier .codingseq obtenu.

```python
cd ..
mamba activate busco
busco -m transcriptome -i without_isoforms/without_isoforms.codingseq -o busco_results -l eudicots_odb10 -c 8
```

##### Fichiers obtenus


- busco.log : log de la commande busco
- logs : dossier des logs de tous les processus de busco
- run_eudicots_odb10 : dossier contenant les séquences trouvées par busco en format FASTA et GFF, les identifiants des séquences manquantes, et l'output de hmmer et metaeuk
- short_summary.specific.eudicots_odb10.busco_results.json : résumé du résultat busco
- short_summary.specific.eudicots_odb10.busco_results.txt : résumé du résultat busco


##### Synthèse des résultats


| | Protéines d'espèces proches + RNA-Seq de <i>C. jacea</i> | Protéines du clade des Eudicotylédons (OrthoDB 11) + RNA-Seq de <i>C. jacea</i> | Protéines du clade des Eudicotylédons et de <i>C. cardunculus</i> + RNA-Seq de <i>C. jacea</i> | Protéines du clade des Eudicotylédons et de <i>C. solstitialis</i> + RNA-Seq de <i>C. jacea</i> |
| :- | :-: | :-: | :-: | :-: |
Complet | 2240 (96.3%) | 2239 (96.2%) | 2241 (96.3%) | 2241 (96.3%)
Complet unique | 2086 (89.7%) | 2085 (89.6%) | 2088 (89.7%) | 2087 (89.7%)
Complet dupliqué | 154 (6.6%) | 154 (6.6%) | 153 (6.6%) | 154 (6.6%)
Fragmenté | 8 (0.3%) | 9 (0.4%) | 8 (0.3%) | 8 (0.3%)
Manquant | 78 (3.4%) | 78 (3.4%) | 77 (3.4%) | 77 (3.4%)
Total / 2326 | 2248 (96.6%) | 2248 (96.6%) | 2249 (96.6%) | 2249 (96.6%)


### OMArk


L'outil OMArk permet de faire une recherche des séquences protéiques trouvées dans la base de données OMA (https://omabrowser.org/oma/home/) d'une lignée afin de voir si les gènes prédits y sont retrouvés. La lignée sélectionnée ici est la lignée Asteranae (clade). Cette lignée comprend 11 espèces :
- <i>Actinidia chinensis var. chinensis</i>
- <i>Capsicum annuum</i>
- <i>Coffea canephora</i>
- <i>Cynara cardunculus var. scolymus</i>
- <i>Daucus carota subsp. sativus</i>
- <i>Erythranthe guttata</i>
- <i>Helianthus annuus</i>
- <i>Nicotiana attenuata</i>
- <i>Nicotiana tabacum</i>
- <i>Solanum lycopersicum</i>
- <i>Solanum tuberosum</i>


##### Documentation
https://github.com/DessimozLab/OMArk/tree/main

https://github.com/DessimozLab/omamer


##### Commandes


Il faut d'abord faire une recherche dans la base OMA (ici <i>Viridiplantae</i>) avec l'outil OMAmer, puis soumettre le résultats de cette recherche à OMArk.

```python
cd /Results/structural_annotation/BRAKER3_$proteinSet_rnaSet
mamba activate OMArk
mkdir OMAmer-OMArk
omamer search -d /Data/dbs/OMA/Viridiplantae.h5 -q braker.aa -o OMAmer-OMArk/omamer.out -t 8
```

OMArk utilise les protéines générées depuis l'annotation et n'en sélectionne qu'une par gène, il faut donc lui soumettre la liste des transcrits pour chaque gène pour qu'il choisisse l'un de ces isoformes.

```python
# On crée un fichier .splice contenant les isoformes
python3 /Data/scripts/make_splice_file.py braker.aa
```

```python
cd OMAmer-OMArk
# On donne à omark le rang taxonomique le plus proche (parmi la liste des rangs acceptés) avec l'option -r,
# et l'identifiant taxonomique de ce rang pour notre espèce avec l'option -t
omark -f omamer.out -d /Data/dbs/OMA/Viridiplantae.h5 -o omark -r family -t 4210 -of ../braker.aa --isoform_file ../braker.aa.splice
```

##### Fichiers obtenus


- omamer_Cynara_cardunculus_var_scolymus.fasta : liste des protéines ayant été retrouvées dans la base de <i>Cynara cardunculus</i>
- omamer_detailed_summary.txt : résumé détaillé des résultats
- omamer_mapped.fasta : liste des protéines retrouvées dans la lignée donnée
- omamer_misplaced.fasta : liste des protéines retrouvées dans d'autres lignées que la lignée donnée
- omamer_no_hits.fasta :  : liste des protéines non-retrouvées dans la base de données
- omamer.omq : liste des protéines uniques retrouvées
- omamer.pdf : histogramme des pourcentages de protéines selon leur catégorie
- omamer.png : histogramme des pourcentages de protéines selon leur catégorie
- omamer_selected_isoforms.txt : liste des isoformes (= transcrits) choisis pour chaque gène par omark
- omamer.sum : résumé simplifié des résultats
- omamer.tax : rappel de la lignée et clade sélectionnés
- omamer.ump : liste des protéines complètes retrouvés dans la lignée


Pour rappel, OMArk ne sélectionne qu'un seul isoforme par gène : il ne prend donc pas en compte toutes les protéines fournies par nos annotations.

Le nombre de protéines prises en compte dans nos annotations par OMArk est :

| | Protéines d'espèces proches + RNA-Seq de <i>C. jacea</i> | Protéines du clade des Eudicotylédons (OrthoDB 11) + RNA-Seq de <i>C. jacea</i> | Protéines du clade des Eudicotylédons et de <i>C. cardunculus</i> + RNA-Seq de <i>C. jacea</i> | Protéines du clade des Eudicotylédons et de <i>C. solstitialis</i> + RNA-Seq de <i>C. jacea</i> |
| :- | :-: | :-: | :-: | :-: |
Nombre total de protéines | 28 354 | 26 715 | 26 818 | 28 169


Le premier tableau permet de voir les gènes trouvés dans la lignée sélectionnée, et en combien de copies, similairement à BUSCO :

| | Protéines d'espèces proches + RNA-Seq de <i>C. jacea</i> | Protéines du clade des Eudicotylédons (OrthoDB 11) + RNA-Seq de <i>C. jacea</i> | Protéines du clade des Eudicotylédons et de <i>C. cardunculus</i> + RNA-Seq de <i>C. jacea</i> | Protéines du clade des Eudicotylédons et de <i>C. solstitialis</i> + RNA-Seq de <i>C. jacea</i> |
| :- | :-: | :-: | :-: | :-: |
Unique | 7 885 (70.41%) | 7 899 (70.54%) | 7 906 (70.6%) | 7 897 (70.52%)
Dupliqué | 2 800 (25%) | 2 784 (24.86%) | 2 779 (24.82%) | 2 798 (24.99%)
Dupliqué attendu | 2 057 (18.37%) | 2 067 (18.46%) | 2 060 (18.4%) | 2 066 (18.45%)
Dupliqué inattendu | 743 (6.64%) | 717 (6.4%) | 719 (6.42%) | 732 (6.54%)
Manquant | 513 (4.58%) | 515 (4.6%) | 513 (4.58%) | 503 (4.49%)
Total / 11 198 | 10 685 (95.42%) | 10 683 (95.4%) | 10 685 (95.42%) | 10 695 (95.51%)


Le deuxième tableau obtenu permet de voir les gènes retrouvés dans des espèces appartenant à la lignée sélectionnée (Consistent), ceux retrouvés dans des espèces non incluses dans la lignée (Inconsistent), et enfin ceux n'ayant pas été trouvé dans la base de données OMA (Inconnu) :

| | Protéines d'espèces proches + RNA-Seq de <i>C. jacea</i> | Protéines du clade des Eudicotylédons (OrthoDB 11) + RNA-Seq de <i>C. jacea</i> | Protéines du clade des Eudicotylédons et de <i>C. cardunculus</i> + RNA-Seq de <i>C. jacea</i> | Protéines du clade des Eudicotylédons et de <i>C. solstitialis</i> + RNA-Seq de <i>C. jacea</i> |
| :- | :-: | :-: | :-: | :-: |
Consistent | 25 384 (89.53%) | 24 973 (93.48%) | 25 024 (93.31%) | 25 381 (90.1%)
Consistent partiel | 1 931 (6.81%) | 1 676 (6.27%) | 1 712 (6.38%) | 1 911 (6.78%)
Consistent fragmenté | 844 (2.98%) | 806 (3.02%) | 811 (3.02%) | 838 (2.97%)
Inconsistent | 749 (2.64%) | 669 (2.5%) | 684 (2.55%) | 740 (2.63%)
Inconsistent partiel | 358 (1.26%) | 301 (1.13%) | 320 (1.19%) | 350 (1.24%)
Inconsistent fragmenté | 86 (0.3%) | 89 (0.33%) | 87 (0.32%) | 85 (0.3%)
Inconnu | 2 221 (7.83%) | 1 073 (4.02%) | 1110 (4.14%) | 2 048 (7.27%)

<!-- #region -->
L'espèce chez laquelle le plus de gènes ont été retrouvés est <b><i>Cynara cardunculus</i></b> pour toutes les annotations :

| | Protéines d'espèces proches + RNA-Seq de <i>C. jacea</i> | Protéines du clade des Eudicotylédons (OrthoDB 11) + RNA-Seq de <i>C. jacea</i> | Protéines du clade des Eudicotylédons et de <i>C. cardunculus</i> + RNA-Seq de <i>C. jacea</i> | Protéines du clade des Eudicotylédons et de <i>C. solstitialis</i> + RNA-Seq de <i>C. jacea</i> |
| :- | :-: | :-: | :-: | :-: |
Espèce détectée : <i>Cynara cardunculus</i> | 26 133 (92.17%) | 25 642 (95.98%) | 25 708 (95.86%) | 26 121 (92.73%)


Cela est certainement dû au fait qu'OMA ne contient que 11 espèces dans son clade des Astéridées, et <i>Cynara cardunculus</i> est la seule de ces espèces à être assez proche de notre Centaurée.
<!-- #endregion -->
