# Sommaire


- [Données](#données)

- [Statistiques basiques](#fichiers-obtenus)
    
- [Statistiques avec gFACs](#fichiers-obtenus-1)
    
- [Analyse avec BUSCO](#synthèse-des-résultats)
    
- [Analyse avec OMArk](#synthèse-des-résultats-1)


Huit espèces plus ou moins proches de <i>Centaurea corymbosa</i> possèdent des données génomiques exploitables sur différentes plateformes :

![Phylogénie rudimentaires des espèces proches](images/phylogénie.png)

Parmi ces espèces, trois ont été annotées : <i>Arctium lappa</i>, <i>Centaurea solstitialis</i> et <i>Cynara cardunculus</i>. Des informations basiques ont été extraites de ces annotations pour permettre une éventuelle comparaison avec notre future annotation (l'espèce <i>Solanum lycopersicum</i> a été rajoutée car ce sera l'espèce utilisée en paramètre pour l'annotation par Augustus en <i>ab initio</i>) :


# Données


Les données récupérées pour chaque espèce proviennent de NCBI. Les fichiers obtenus et renommés pour chaque espèce sont :
- annotation.gff : annotation au format GFF
- annotation.gtf : annotation au format GTF
- genome.codingseq : séquences codantes issues de l'annotation
- genome.fa : génome de l'espèce
- proteins.fa : protéines issues de l'annotation
- rna.fa : séquences ARN issues de l'annotation (disponibles uniquement pour <i>Cynara cardunculus</i> et <i>Solanum lycopersicum</i>, espèces modèles donc les plus documentées)


# Statistiques de base


##### Commandes

```python
cd /Data/other_species/$SPECIES_NAME
```

```python
grep -c ">" proteins.fa > nb_of_prot_seq.txt
```

```python
# Ploter l'histogramme du nombre d'exons trouvés selon leur taille
python3 /Data/scripts/exons_len_plot.py annotation.gtf .
# Ploter l'histogramme du nombre de gènes trouvés selon leur taille
python3 /Data/scripts/gene_len_plot.py annotation.gtf .
# Ploter l'histogramme du nombre de transcrits trouvés selon leur taille
python3 /Data/scripts/transcripts_len_plot.py annotation.gtf .
# Ploter l'histogramme du nombre de gènes selon leur nombre d'exons (moyenne sur tous les transcrits)
python3 /Data/scripts/exons_mean_nbr_plot_stat_for_NCBI.py annotation.gtf .
# Ploter le scatter plot de la taille de gènes par leur nombre moyen d'exons
python3 /Data/scripts/gene_len_by_mean_exons_for_NCBI.py annotation.gtf .
```

```python
mkdir plots
mv *.png plots
mkdir stats
mv *n*.txt stats
```

##### Fichiers obtenus


- exons_length_histogram.png : histogramme du nombre d'exons trouvés selon leur taille
- exons_mean_nbr_histogram.png : histogramme du nombre de gènes selon leur nombre d'exons (moyenne sur tous les transcrits trouvés pour le gène)
- exons_mean_nbr_stat.txt : statistiques sur les exons par gènes (moyenne sur tous les transcrits)
- gene_len_by_mean_exons_scatterplot.png : scatter plot de la taille de gènes par leur nombre moyen d'exons
- gene_length_histogram.png : histogramme du nombre de gènes trouvés selon leur taille
- nb_of_prot_seq.txt : nombre de séquences protéiques extraites de l'annotation
- transcripts_length_histogram.png : histogramme du nombre de transcrits trouvés selon leur taille


Ces résultats ont été synthétisés en tableaux :

| | <i>Arctium lappa</i> | <i>Centaurea solstitialis</i> | <i>Cynara cardunculus</i> | | <i>Solanum lycopersicum</i>
| :- | :-: | :-: | :-: | :-: | :-: |
Nombre de protéines | 40 570 | 33 537 | 38 393 | | 37 602
Longueur des séquences codantes | 50 139 011 | 58 825 353 | 56 958 776 | | 53 503 707

<br>

| | <i>Arctium lappa</i> | <i>Centaurea solstitialis</i> | <i>Cynara cardunculus</i> | | <i>Solanum lycopersicum</i>
| :- | :-: | :-: | :-: | :-: | :-: |
Nombre de gènes | 46 935 | 34 242 | 30 275 | | 31 221
Nombre de gènes mono-ex. | 7 290 | 3 229 | 6 478 | | 5 481
Nombre de gènes multi-ex. | 39 645 | 31 013 | 22 713 | | 24 513
Ratio mono/multi | 0.18 | 0.1 | 0.29 | | 0.22
Nombre médian d’exons | 3 | 4 | 3 | | 3
Nombre maximum d’exons | 79 | 79 | 76 | | 77


### gFACs


##### Commandes


gFACs a une option format (-f) pour renseigner le format du fichier GTF donné en entrée. Pour savoir le format à renseigner, on peut utiliser le script de support format_diagnosis.pl (https://gfacs.readthedocs.io/en/latest/Bonus/format_diagnosis.html). Ce script renvoie une suite de valeurs que l'on peut ensuite aller comparer au tableau de formats de la documentation gFACs (https://gfacs.readthedocs.io/en/latest/_images/Table.png) pour déterminer quel format gFACs correspond au nôtre, ou au moins lequel en est le plus proche.

```python
cd /Results/structural_annotation/BRAKER1_$alignment/BRAKER1_$species
perl /opt/biotools/conda/envs/gFACs/opt/perl-gfacs-1.1.1/support_scripts/format_diagnosis.pl annotation.gtf
```

Le format correspondant au fichier gtf obtenu est xxxxx.

```python
mkdir gFACs
mamba activate gFACs
gFACs.pl -f braker_2.0_gtf --statistics --fasta /Data/genomes/original_genome/centaurea_genome.fasta --splice-table --nt-content \
	--get-fasta --get-protein-fasta --create-gff3 --distributions exons_lengths intron_lengths CDS_lengths gene_lengths \
	exon_position exon_position_data intron_position intron_position_data -O gFACs annotation.gtf
```

##### Fichiers obtenus


- CDS_lengths_distributions.tsv : distribution des longueurs des séquences codantes
- data_exon_position_distributions.tsv : liste des positions des exons ?
- data_intron_position_distributions.tsv : liste des positions des exons ?
- exon_position_distributions.tsv : distribution de la position des exons
- gene_lengths_distributions.tsv : distribution des longueurs des gènes
- genes.fasta : liste des gènes utilisés par gFACs pour ses statistiques
- genes.fasta.faa : liste des gènes utilisés par gFACs pour ses statistiques accompagnés de certaines séquences protéiques
- gene_table.txt : table de gènes crées par gFACs pour ses statistiques
- gFACs_log.txt : fichier de log
- intron_lengths_distributions.tsv : distribution des longueurs des introns
- intron_position_distributions.tsv : distribution de la position des introns
- out.gff3 : fichier contenant les éléments étudiés par gFACs (mRNA, exons, ...)
- statistics.txt : statistiques produites par gFACs


Le résumé des statistiques obtenues se trouve dans deux tableaux de synthèse :


### BUSCO


L'outil BUSCO permet de faire une recherche des séquences codantes trouvées dans la base de données orthoDB (https://www.orthodb.org/) d'une lignée taxonomique. La lignée sélectionnée ici est la lignée eudicots_odb10.

On lance ici BUSCO sur les séquences codantes extraites depuis l'annotation.


##### Documentation
https://busco.ezlab.org/busco_userguide.html


##### Commandes


Pour lancer BUSCO en mode transcriptome, il faut d'abord faire en sorte de ne sélectionner qu'un isoforme (= transcrit) par gène. Les annotations NCBI n'ont qu'un seul isoforme par gène, donc il n'est pas nécessaire de s'en soucier ici.

```python
cd /Data/other_species/$SPECIES_NAME
mamba activate busco
busco -m transcriptome -i genome.codingseq -o busco_cds -l eudicots_odb10 -c 8
```

##### Fichiers obtenus


- busco.log : log de la commande busco
- logs : dossier des logs de tous les processus de busco
- run_eudicots_odb10 : dossier contenant les séquences trouvées par busco en format FASTA et GFF, les identifiants des séquences manquantes, et l'output de hmmer et metaeuk
- short_summary.specific.eudicots_odb10.busco_results.json : résumé du résultat busco
- short_summary.specific.eudicots_odb10.busco_results.txt : résumé du résultat busco


##### Synthèse des résultats


| | <i>Arctium lappa</i> | <i>Centaurea solstitialis</i> | <i>Cynara cardunculus</i> | | <i>Solanum lycopersicum</i>
| :- | :-: | :-: | :-: | :-: | :-: |
Complet | 2 237 (96.1%) | 1 604 (69%) | 2 284 (98.2%) | | 2 305 (99.1%)
Complet unique | 2 101 (90.3%) | 1 514 (65.1%) | 1 439 (61.9%) | | 1 560 (67.1%)
Complet dupliqué | 136 (5.8%) | 90 (3.9%) | 845 (36.3%) | | 745 (32%)
Fragmenté | 21 (0.9%) | 78 (3.4%) | 13 (0.6%) | | 5 (0.2%)
Manquant | 68 (3%) | 644 (27.6%) | 29 (1.2%) | | 16 (0.7%)
Total / 2 326 | 2 258 (97%) | 1 682 (72.4%) | 2 297 (98.8%) | | 2 310 (99.3%)


### OMArk


L'outil OMArk permet de faire une recherche des séquences protéiques trouvées dans la base de données OMA (https://omabrowser.org/oma/home/) d'une lignée afin de voir si les gènes prédits y sont retrouvés. La lignée sélectionnée ici est la lignée Asteranae (clade). Cette lignée comprend 11 espèces :
- <i>Actinidia chinensis var. chinensis</i>
- <i>Capsicum annuum</i>
- <i>Coffea canephora</i>
- <i>Cynara cardunculus var. scolymus</i>
- <i>Daucus carota subsp. sativus</i>
- <i>Erythranthe guttata</i>
- <i>Helianthus annuus</i>
- <i>Nicotiana attenuata</i>
- <i>Nicotiana tabacum</i>
- <i>Solanum lycopersicum</i>
- <i>Solanum tuberosum</i>


##### Documentation
https://github.com/DessimozLab/OMArk/tree/main

https://github.com/DessimozLab/omamer


##### Commandes


Il faut d'abord faire une recherche dans la base OMA (ici <i>Viridiplantae</i>) avec l'outil OMAmer, puis soumettre le résultats de cette recherche à OMArk.

```python
cd /Results/structural_annotation/BRAKER1_$alignment/BRAKER1_$species
mamba activate OMArk
mkdir OMAmer-OMArk
omamer search -d /Data/dbs/OMA/Viridiplantae.h5 -q braker.aa -o OMAmer-OMArk/omamer.out -t 8
```

OMArk utilise les protéines générées depuis l'annotation et n'en sélectionne qu'une par gène, il faut donc lui soumettre la liste des transcrits pour chaque gène pour qu'il choisisse l'un de ces isoformes.

```python
# On crée un fichier .splice contenant les isoformes
python3 /Data/scripts/make_splice_file.py braker.aa
```

```python
cd OMAmer-OMArk
# On donne à omark le rang taxonomique le plus proche (parmi la liste des rangs acceptés) avec l'option -r,
# et l'identifiant taxonomique de ce rang pour notre espèce avec l'option -t
omark -f omamer.out -d /Data/dbs/OMA/Viridiplantae.h5 -o omark -r family -t 4210 -of ../braker.aa --isoform_file ../braker.aa.splice
```

##### Fichiers obtenus


- omamer_Cynara_cardunculus_var_scolymus.fasta : liste des protéines ayant été retrouvées dans la base de <i>Cynara cardunculus</i>
- omamer_detailed_summary.txt : résumé détaillé des résultats
- omamer_mapped.fasta : liste des protéines retrouvées dans la lignée donnée
- omamer_misplaced.fasta : liste des protéines retrouvées dans d'autres lignées que la lignée donnée
- omamer_no_hits.fasta :  : liste des protéines non-retrouvées dans la base de données
- omamer.omq : liste des protéines uniques retrouvées
- omamer.pdf : histogramme des pourcentages de protéines selon leur catégorie
- omamer.png : histogramme des pourcentages de protéines selon leur catégorie
- omamer_selected_isoforms.txt : liste des isoformes (= transcrits) choisis pour chaque gène par omark
- omamer.sum : résumé simplifié des résultats
- omamer.tax : rappel de la lignée et clade sélectionnés
- omamer.ump : liste des protéines complètes retrouvés dans la lignée


##### Synthèse des résultats


Pour rappel, OMArk ne sélectionne qu'un seul isoforme par gène : il ne prend donc pas en compte toutes les protéines fournies par nos annotations.

Le nombre de protéines prises en compte dans nos annotations par OMArk est :

| | <i>Arctium lappa</i> | <i>Centaurea solstitialis</i> | <i>Cynara cardunculus</i> | | <i>Solanum lycopersicum</i>
| :- | :-: | :-: | :-: | :-: | :-: |
Nombre total de protéines | 40 570 | 33 537 | 38 393 | | 37 602


Le premier tableau permet de voir les gènes trouvés dans la lignée sélectionnée, et en combien de copies, similairement à BUSCO :

| | <i>Arctium lappa</i> | <i>Centaurea solstitialis</i> | <i>Cynara cardunculus</i> | | <i>Solanum lycopersicum</i>
| :- | :-: | :-: | :-: | :-: | :-: |
Unique | 7 061 (63.06%) | 6 559 (58.57%) | 6 090 (54.38%) | | 6 229 (55.63%)
Dupliqué | 3 069 (27.41%) | 1 751 (15.64%) | 4 747 (42.39%) | | 4 793 (42.80%)
Dupliqué attendu | 1 927 (17.21%) | 1 019 (9.1%) | 2 073 (18.51%) | | 1 983 (17.71%)
Dupliqué inattendu | 1 142 (10.20%) | 732 (6.54%) | 2 674 (23.88%) | | 2 810 (25.09%)
Manquant | 1 068 (9.54%) | 2 888 (25.79%) | 361 (3.22%) | | 176 (1.57%)
Total / 11 198 | 10 130 (90.46%) | 8 310 (74.21%) | 10 837 (96.78%) | | 11 022 (98.43%)


Le deuxième tableau obtenu permet de voir les gènes retrouvés dans des espèces appartenant à la lignée sélectionnée (Consistent), ceux retrouvés dans des espèces non incluses dans la lignée (Inconsistent), et enfin ceux n'ayant pas été trouvé dans la base de données OMA (Inconnu) :

| | <i>Arctium lappa</i> | <i>Centaurea solstitialis</i> | <i>Cynara cardunculus</i> | | <i>Solanum lycopersicum</i>
| :- | :-: | :-: | :-: | :-: | :-: |
Consistent | 28 958 (71.38%) | 27 525 (82.07%) | 37 058 (96.52%) | | 36 973 (98.33%)
Consistent partiel | 6 639 (16.36%) | 7 208 (21.49%) | 2 140 (5.57%) | | 1 736 (4.62%)
Consistent fragmenté | 2 058 (5.07%) | 1 634 (4.87%) | 1 111 (2.89%) | | 1 040 (2.77%)
Inconsistent | 1 416 (3.49%) | 2 211 (6.59%) | 627 (1.63%) | | 149 (0.40%)
Inconsistent partiel | 1 016 (2.50%) | 1 425 (4.25%) | 328 (0.85%) | | 90 (0.24%)
Inconsistent fragmenté | 174 (0.43%) | 136 (0.41%) | 65 (0.17%) | | 9 (0.02%)
Inconnu | 10 196 (25.13%) | 3 801 (11.33%) | 708 (1.84%) | | 480 (1.28%)


L'espèce chez laquelle le plus de gènes ont été retrouvés est <b><i>Cynara cardunculus</i></b> pour les annotations des 3 espèces proches :

| | <i>Arctium lappa</i> | <i>Centaurea solstitialis</i> | <i>Cynara cardunculus</i>
| :- | :-: | :-: | :-: |
Espèce détectée : <i>Cynara cardunculus</i> | 30 374 (74.87%) | 29 736 (88.67%) | 37 685 (98.16%)

Cela est certainement dû au fait qu'OMA ne contient que 11 espèces dans son clade des Astéridées, et <i>Cynara cardunculus</i> est la seule de ces espèces à être assez proche de notre Centaurée.

<br>

Pour <i>Solanum lycopersicum</i>, l'espèce détectée est, sans surprise, sa propre espèce (qui fait partie des 11 espèces du clade) :

| | <i>Solanum lycopersicum</i>
| :- | :-: |
Espèce détectée : <i>Solanum lycopersicum</i> | 37 122 (98.72%)
