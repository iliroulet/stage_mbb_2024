# Sommaire


- [Lancement](#commandes)

- [Résultats](#fichiers-obtenus)

    - [Statistiques basiques](#fichiers-obtenus-1)
    
    - [Statistiques avec gFACs](#fichiers-obtenus-2)
    
    - [Analyse avec BUSCO](#synthèse-des-résultats)
    
    - [Analyse avec OMArk](#synthèse-des-résultats-1)


# BRAKER1


BRAKER1 a été lancé 4 fois avec différentes données RNA-Seq de 4 espèces proches de la Centaurée : <i>Centaurea jacea</i>, <i>Centaurea solstitialis</i>, <i>Centaurea cyanus</i> (qui a été récemment reclassifiée dans un autre genre et renommée en <i>Cyanus segetum</i>) et <i>Cynara cardunculus</i>.

Le pipeline BRAKER est initialement fait pour faire de l'annotation par alignement ARN avec des données provenant de la même espèce que le génome, il a donc fallu ici modifier les paramètres de l'aligneur de BRAKER, hisat2 (https://github.com/DaehwanKimLab/hisat2), pour autoriser plus de mismatchs. Il a donc fallu aligner les SRA manuellement au lieu de laisser BRAKER le faire, car il nétait pas possible de modifier les paramètres d'hisat2 depuis la commande BRAKER.

Les paramètres modifiés sont les pénalités de mismatch minimum et maximum (option --mp), c'est-à-dire les pénalités soustraites au score d'alignement pour chaque caractère non-alignable (voir documentation).

Il est aussi possible de modifier la fonction calculant le score minimum d'alignement requis pour qu'un alignement soit considéré valide (option --score-min), mais cela n'a pas été fait ici.


##### Documentation

https://github.com/Gaius-Augustus/BRAKER

![Schéma du pipeline BRAKER1](images/figures_pipelines/braker1.png)

https://daehwankimlab.github.io/hisat2/manual/

http://www.htslib.org/doc/ (samtools)

```python
# ID RNA-Seq pour Centaurea jacea
SRA_IDs=SRR12838531
# ID RNA-Seq pour Centaurea solstitialis
SRA_IDs=SRR12838530
# ID RNA-Seq pour Centaurea cyanus
SRA_IDs=SRR12917349
# IDs RNA-Seq pour Cynara cardunculus
SRA_IDs=SRR16295431,SRR16295432,SRR16295433,SRR16295434,SRR16295435,SRR16295436,SRR16295437,SRR16295438,SRR16295439,SRR16295440,SRR16295441,SRR16295442
```

##### Commandes

```python
cd /Data/genomes/masked_genome/
# On indexe d'abord le génome (masqué)
hisat2-build -p 8 /Data/genomes/masked_genome/centaurea_genome.fasta.masked masked_centaurea 1> hisat2.out 2> hisat2.err
```

```python
cd /Data/rnaseq/$species/fastq
# On aligne ensuite les reads sur le génome grâce à l'index produit
hisat2 -x /Data/genomes/masked_genome/hisat2/masked_centaurea -1 $SRA_ID"_1.fastq" -2 $SRA_ID"_2.fastq" --dta -p 8 -S $SRA_ID".sam" --mp 3,1 1> hisat2.out 2> hisat2.err
```

```python
# On convertit les fichiers SAM obtenus en fichiers BAM (version binarisée) avec samtools
samtools sort -o $SRA_ID".bam" -@ 3 $SRA_ID".sam"
```

On peut ensuite lancer BRAKER avec les fichiers BAM obtenus.

```python
# activer l'environnement contenant les dépendances de braker
mamba activate braker_env
# exporter les outils d'alignement utilisés par GeneMark dans le PATH
export PATH=$PATH:/Data/tools/GeneMark-ETP/tools
# Lancer BRAKER1 sur tous les SRA trouvés sur NCBI sur le gène masqué
{ time /Data/tools/BRAKER/scripts/braker.pl \
        --workingdir=/Results/structural_annotation/BRAKER1_custom_align/BRAKER1_$species \
        --genome=/Data/genomes/masked_genome/centaurea_genome.fasta.masked \
        --bam=/Data/rnaseq/$species/fastq/$SRA_ID".bam" \
        --threads=15 \
        --AUGUSTUS_CONFIG_PATH=/Data/tools/Augustus/config/ \
        --AUGUSTUS_BIN_PATH=/Data/tools/Augustus/bin/ \
        --AUGUSTUS_SCRIPTS_PATH=/Data/tools/Augustus/scripts/ \
        --COMPLEASM_PATH=/Data/tools/compleasm_kit/ \
        --GENEMARK_PATH=/Data/tools/GeneMark-ETP/bin/ \
        --PROTHINT_PATH=/Data/tools/ProtHint/bin/ \
        --BAMTOOLS_PATH=/Data/tools/bamtools/src/ \
        --TSEBRA_PATH=/Data/tools/TSEBRA/bin/ ; } 2> /Results/times/BRAKER1_custom_align/BRAKER1_$species_time.txt
```

Lancement avec la liste des IDs des SRA (donc alignement standard de BRAKER1) :

```python
# activer l'environnement contenant les dépendances de braker
mamba activate braker_env
# exporter les outils d'alignement utilisés par GeneMark dans le PATH
export PATH=$PATH:/Data/tools/GeneMark-ETP/tools
# Lancer BRAKER1 sur tous les SRA trouvés sur NCBI sur le gène masqué
{ time /Data/tools/BRAKER/scripts/braker.pl \
	--workingdir=/Results/structural_annotation/BRAKER1_standard_align/BRAKER1_$species/ \
	--genome=/Data/genomes/masked_genome/centaurea_genome.fasta.masked \
	--rnaseq_sets_ids=$SRA_IDs \
	--threads=15 \
	--AUGUSTUS_CONFIG_PATH=/Data/tools/Augustus/config/ \
	--AUGUSTUS_BIN_PATH=/Data/tools/Augustus/bin/ \
	--AUGUSTUS_SCRIPTS_PATH=/Data/tools/Augustus/scripts/ \
	--COMPLEASM_PATH=/Data/tools/compleasm_kit/ \
	--GENEMARK_PATH=/Data/tools/GeneMark-ETP/bin/ \
	--PROTHINT_PATH=/Data/tools/ProtHint/bin/ \
	--BAMTOOLS_PATH=/Data/tools/bamtools/src/ \
	--SRATOOLS_PATH=/Data/tools/sratoolkit/bin/ \
	--TSEBRA_PATH=/Data/tools/TSEBRA/bin/ ; } 2> /Results/times/BRAKER1_standard_align/BRAKER1_$species_time.txt
```

 

```python
# on transforme l'annotation obtenue en format GFF3 pour pouvoir l'utiliser plus tard avec funannotate
cat braker.gtf | perl -ne 'if(m/\tAUGUSTUS\t/ or m/\tGeneMark\.hmm\t/ or m/\tGeneMark\.hmm3\t/ or m/\tgmst\t/) {print $_;}' | gtf2gff.pl --gff3 --out=braker.gff3
```

##### Fichiers obtenus


- Augustus : résultats de l'annotation brute par Augustus
- bam_header.map : liste des headers des fichiers bam
- braker.aa : séquences protéiques de l'annotation
- braker.codingseq : séquences codantes de l'annotation
- braker.gtf : annotation
- braker.log : fichier de log
- errors : sorties d'erreur
- GeneMark-ET : résultats de l'annotation brute par GeneMark-ET
- genome_header.map : liste des headers du génome
- hintsfile.gff : fichier GFF contenant les hints
- species : paramètres d'espèces (personnalisés grâce à l'annotation par GeneMark) utilisés par Augustus
- what-to-cite.txt : citations du pipeline et de ses différents outils


##### Alignements


- Alignement standard :

    - <i>Centaurea jacea</i> :
        - SRR12838531: 76.57% overall alignment rate

    - <i>Centaurea solstitialis</i> :
        - SRR12838530 : 43.49% overall alignment rate

    - <i>Centaurea cyanus</i> :
        - SRR12917349 : 12.19% overall alignment rate

    - <i>Cynara cardunculus</i> :
        - SRR16295431 : 6.91% overall alignment rate
        - SRR16295432 : 7.41% overall alignment rate
        - SRR16295433 : 4.81% overall alignment rate
        - SRR16295434 : 4.76% overall alignment rate
        - SRR16295435 : 4.25% overall alignment rate
        - SRR16295436 : 5.08% overall alignment rate
        - SRR16295437 : 4.32% overall alignment rate
        - SRR16295438 : 4.10% overall alignment rate
        - SRR16295439 : 3.95% overall alignment rate
        - SRR16295440 : 4.82% overall alignment rate
        - SRR16295441 : 4.24% overall alignment rate
        - SRR16295442 : 6.24% overall alignment rate


- Alignement customisé :

    - <i>Centaurea jacea</i> :
        - SRR12838531: 84.64% overall alignment rate

    - <i>Centaurea solstitialis</i> :
        - SRR12838530 : 66.22% overall alignment rate

    - <i>Centaurea cyanus</i> :
        - SRR12917349 : 30.28% overall alignment rate

    - <i>Cynara cardunculus</i> :
        - SRR16295431 : 21.62% overall alignment rate
        - SRR16295432 : 21.83% overall alignment rate
        - SRR16295433 : 20.80% overall alignment rate
        - SRR16295434 : 21.01% overall alignment rate
        - SRR16295435 : 19.87% overall alignment rate
        - SRR16295436 : 20.30% overall alignment rate
        - SRR16295437 : 19.66% overall alignment rate
        - SRR16295438 : 17.58% overall alignment rate
        - SRR16295439 : 18.64% overall alignment rate
        - SRR16295440 : 20.21% overall alignment rate
        - SRR16295441 : 20.02% overall alignment rate
        - SRR16295442 : 20.70% overall alignment rate
    
On peut observer ici que le fait de relâcher les paramètres d'alignement permet en effet de faire augmenter le pourcentage, de 10 à 20% environ.


##### Hints

<!-- #region -->
Après avoir utilisé ces alignements pour faire une première annotation avec GeneMark, des hints sont extraits de cette annotation avec l'outil bam2hints, qui n'utilisera que les annotations d'introns en tant que hints (option --introns-only).


| | C. solstitialis (alignement standard) | C. solstitialis (alignement customisé) | C. cyanus (alignement standard) | C. cyanus (alignement customisé) | C. cardunculus (alignement standard) | C. cardunculus (alignement customisé) | C. jacea (alignement standard) | C. jacea (alignement customisé)
| :- | :-: | :-: | :-: | :-: | :-: | :-: | :-: | :-: |
Nombre de hints | 103 375 | 124 536 | 38 397 | 92 065 | 34 950 | 86 425 | 151 579 | 160 735
<!-- #endregion -->

## Analyse


### Statistiques de base


##### Commandes

```python
grep -c ">" braker.aa > nb_of_prot_seq.txt
```

```python
# Ploter l'histogramme du nombre d'exons trouvés selon leur taille
python3 /Data/scripts/exons_len_plot.py braker.gtf .
# Ploter l'histogramme du nombre de gènes trouvés selon leur taille
python3 /Data/scripts/gene_len_plot.py braker.gtf .
# Ploter l'histogramme du nombre de transcrits trouvés selon leur taille
python3 /Data/scripts/transcripts_len_plot.py braker.gtf .
# Ploter l'histogramme du nombre de gènes selon leur nombre d'exons (moyenne sur tous les transcrits)
python3 /Data/scripts/exons_mean_nbr_plot_stat.py braker.gtf .
# Ploter le scatter plot de la taille de gènes par leur nombre moyen d'exons
python3 /Data/scripts/gene_len_by_mean_exons.py braker.gtf .
```

```python
mkdir plots
mv *.png plots
mkdir stats
mv *n*.txt stats
```

##### Fichiers obtenus


- exons_length_histogram.png : histogramme du nombre d'exons trouvés selon leur taille
- exons_mean_nbr_histogram.png : histogramme du nombre de gènes selon leur nombre d'exons (moyenne sur tous les transcrits trouvés pour le gène)
- exons_mean_nbr_stat.txt : statistiques sur les exons par gènes (moyenne sur tous les transcrits)
- gene_len_by_mean_exons_scatterplot.png : scatter plot de la taille de gènes par leur nombre moyen d'exons
- gene_length_histogram.png : histogramme du nombre de gènes trouvés selon leur taille
- nb_of_prot_seq.txt : nombre de séquences protéiques extraites de l'annotation
- transcripts_length_histogram.png : histogramme du nombre de transcrits trouvés selon leur taille


Ces résultats ont été synthétisés en tableaux :

| | C. solstitialis (standard) | C. solstitialis (customisé) | C. cyanus (standard) | C. cyanus (customisé) | C. cardunculus (standard) | C. cardunculus (customisé) | C. jacea (standard) | C. jacea (customisé)
| :- | :-: | :-: | :-: | :-: | :-: | :-: | :-: | :-: |
Nombre de protéines trouvées | 46 710 | 46 190 | 40 782 | 44 754 | 40 205 | 41 219 | 49 148 | 51 022

<br>

| | C. solstitialis (standard) | C. solstitialis (customisé) | C. cyanus (standard) | C. cyanus (customisé) | C. cardunculus (standard) | C. cardunculus (customisé) | C. jacea (standard) | C. jacea (customisé)
| :- | :-: | :-: | :-: | :-: | :-: | :-: | :-: | :-: |
Nombre de gènes | 44 957 | 44 051 | 40 318 | 43 400 | 39 883 | 40 206 | 45 525 | 47 046
Nombre de gènes mono-ex. | 13 006 | 11 132 | 10 115 | 11 844 | 9 443 | 10 351 | 11 180 | 12 498
Nombre de gènes multi-ex. | 31 951 | 32 919 | 30 203 | 31 556 | 30 440 | 29 855 | 34 345 | 34 548
Ratio mono/multi | 0.41 | 0.34 | 0.33 | 0.38 | 0.31 | 0.35 | 0.33 | 0.36
Nombre médian d’exons | 2 | 3 | 3 | 2 | 3 | 3 | 3 | 3
Nombre maximum d’exons | 455 | 305 | 277 | 192 | 332 | 209 | 333 | 270


### gFACs


##### Commandes


gFACs a une option format (-f) pour renseigner le format du fichier GTF donné en entrée. Pour savoir le format à renseigner, on peut utiliser le script de support format_diagnosis.pl (https://gfacs.readthedocs.io/en/latest/Bonus/format_diagnosis.html). Ce script renvoie une suite de valeurs que l'on peut ensuite aller comparer au tableau de formats de la documentation gFACs (https://gfacs.readthedocs.io/en/latest/_images/Table.png) pour déterminer quel format gFACs correspond au nôtre, ou au moins lequel en est le plus proche.

```python
cd /Results/structural_annotation/BRAKER1_$alignment/BRAKER1_$species
perl /opt/biotools/conda/envs/gFACs/opt/perl-gfacs-1.1.1/support_scripts/format_diagnosis.pl braker.gtf
```

Le format correspondant au fichier gtf obtenu est braker_2.0_gtf.

```python
mkdir gFACs
mamba activate gFACs
gFACs.pl -f braker_2.0_gtf --statistics --fasta /Data/genomes/original_genome/centaurea_genome.fasta --splice-table --nt-content \
	--get-fasta --get-protein-fasta --create-gff3 --distributions exons_lengths intron_lengths CDS_lengths gene_lengths \
	exon_position exon_position_data intron_position intron_position_data -O gFACs braker.gtf
```

##### Fichiers obtenus


- CDS_lengths_distributions.tsv : distribution des longueurs des séquences codantes
- data_exon_position_distributions.tsv : liste des positions des exons ?
- data_intron_position_distributions.tsv : liste des positions des exons ?
- exon_position_distributions.tsv : distribution de la position des exons
- gene_lengths_distributions.tsv : distribution des longueurs des gènes
- genes.fasta : liste des gènes utilisés par gFACs pour ses statistiques
- genes.fasta.faa : liste des gènes utilisés par gFACs pour ses statistiques accompagnés de certaines séquences protéiques
- gene_table.txt : table de gènes crées par gFACs pour ses statistiques
- gFACs_log.txt : fichier de log
- intron_lengths_distributions.tsv : distribution des longueurs des introns
- intron_position_distributions.tsv : distribution de la position des introns
- out.gff3 : fichier contenant les éléments étudiés par gFACs (mRNA, exons, ...)
- statistics.txt : statistiques produites par gFACs


Le résumé des statistiques obtenues se trouve dans deux tableaux de synthèse :

![1er tableau de synthèse de gFACs](images/gFACs/BRAKER1_gFACs_1.svg)

![2ème tableau de synthèse de gFACs](images/gFACs/BRAKER1_gFACs_2.svg)


### BUSCO


L'outil BUSCO permet de faire une recherche des séquences codantes trouvées dans la base de données orthoDB (https://www.orthodb.org/) d'une lignée afin de voir si les gènes prédits y sont retrouvés. La lignée sélectionnée ici est la lignée eudicots_odb10.


##### Documentation
https://busco.ezlab.org/busco_userguide.html


##### Commandes


Pour lancer BUSCO en mode transcriptome, il faut d'abord faire en sorte de ne sélectionner qu'un isoforme (= transcrit) par gène. On utilise donc AGAT pour ne garder que les plus isoformes aux plus longs CDS.

```python
cd /Results/structural_annotation/BRAKER1_$alignment/BRAKER1_$species
mamba activate agat
mkdir without_isoforms
agat_sp_keep_longest_isoform.pl -gff braker.gtf -o without_isoforms/agat_without_isoforms.gtf
```

Il faut ensuite reformater le fichier produit par AGAT pour qu'il corresponde à un format Augustus, ce qui nous permettra de lancer ensuite le script d'Augustus permettat d'obtenir les séquences codantes et protéiques d'une annotation.

```python
cd without_isoforms
python3 /Data/scripts/reformat_agat_gtf.py agat_without_isoforms.gtf without_isoforms.gtf
/Data/tools/Augustus/scripts/getAnnoFastaFromJoingenes.py -g /Data/genomes/masked_genome/centaurea_genome.fasta.masked \
	-o without_isoforms -f without_isoforms.gtf
```

Le script produit deux fichiers : without_isoforms.aa et without_isoforms.codingseq.

On peut enfin lancer BUSCO sur le fichier .codingseq obtenu.

```python
cd ..
mamba activate busco
busco -m transcriptome -i without_isoforms/without_isoforms.codingseq -o busco_results -l eudicots_odb10 -c 8
```

##### Fichiers obtenus


- busco.log : log de la commande busco
- logs : dossier des logs de tous les processus de busco
- run_eudicots_odb10 : dossier contenant les séquences trouvées par busco en format FASTA et GFF, les identifiants des séquences manquantes, et l'output de hmmer et metaeuk
- short_summary.specific.eudicots_odb10.busco_results.json : résumé du résultat busco
- short_summary.specific.eudicots_odb10.busco_results.txt : résumé du résultat busco


##### Synthèse des résultats


| | C. solstitialis (standard) | C. solstitialis (customisé) | C. cyanus (standard) | C. cyanus (customisé) | C. cardunculus (standard) | C. cardunculus (customisé) | C. jacea (standard) | C. jacea (customisé)
| :- | :-: | :-: | :-: | :-: | :-: | :-: | :-: | :-: |
Complet | 2 160 (92.9%) | 2 167 (93.1%) | 2 091 (89.8%) | 2 146 (92.3%) | 2 066 (88.8%) | 2 105 (90.5%) | 2 174 (93.4%) | 2 175 (93.5%)
Complet unique | 2 021 (86.9%) | 2 034 (87.4%) | 1 955 (84%) | 2 005 (86.2%) | 1 931 (83%) | 1 969 (84.7%) | 2 036 (87.5%) | 2 029 (87.2%)
Complet dupliqué | 139 (6%) | 133 (5.7%) | 136 (5.8%) | 141 (6.1%) | 135 (5.8%) | 136 (5.8%) | 138 (5.9%) | 146 (6.3%)
Fragmenté | 38 (1.6%) | 31 (1.3%) | 67 (2.9%) | 50 (2.1%) | 74 (3.2%) | 61 (2.6%) | 30 (1.3%) | 34 (1.5%)
Manquant | 128 (5.5%) | 128 (5.5%) | 168 (7.3%) | 130 (5.6%) | 186 (8%) | 160 (6.9%) | 122 (5.3%) | 117 (5%)
Total / 2 326 | 2 198 (94.5%) | 2 198 (94.5%) | 2 158 (92.7%) | 2 196 (94.4%) | 2 140 (92%) | 2 166 (93.1%) | 2 204 (94.7%) | 2 209 (95%)


### OMArk


L'outil OMArk permet de faire une recherche des séquences protéiques trouvées dans la base de données OMA (https://omabrowser.org/oma/home/) d'une lignée afin de voir si les gènes prédits y sont retrouvés. La lignée sélectionnée ici est la lignée Asteranae (clade). Cette lignée comprend 11 espèces :
- <i>Actinidia chinensis var. chinensis</i>
- <i>Capsicum annuum</i>
- <i>Coffea canephora</i>
- <i>Cynara cardunculus var. scolymus</i>
- <i>Daucus carota subsp. sativus</i>
- <i>Erythranthe guttata</i>
- <i>Helianthus annuus</i>
- <i>Nicotiana attenuata</i>
- <i>Nicotiana tabacum</i>
- <i>Solanum lycopersicum</i>
- <i>Solanum tuberosum</i>


##### Documentation
https://github.com/DessimozLab/OMArk/tree/main

https://github.com/DessimozLab/omamer


##### Commandes


Il faut d'abord faire une recherche dans la base OMA (ici <i>Viridiplantae</i>) avec l'outil OMAmer, puis soumettre le résultats de cette recherche à OMArk.

```python
cd /Results/structural_annotation/BRAKER1_$alignment/BRAKER1_$species
mamba activate OMArk
mkdir OMAmer-OMArk
omamer search -d /Data/dbs/OMA/Viridiplantae.h5 -q braker.aa -o OMAmer-OMArk/omamer.out -t 8
```

OMArk utilise les protéines générées depuis l'annotation et n'en sélectionne qu'une par gène, il faut donc lui soumettre la liste des transcrits pour chaque gène pour qu'il choisisse l'un de ces isoformes.

```python
# On crée un fichier .splice contenant les isoformes
python3 /Data/scripts/make_splice_file.py braker.aa
```

```python
cd OMAmer-OMArk
# On donne à omark le rang taxonomique le plus proche (parmi la liste des rangs acceptés) avec l'option -r,
# et l'identifiant taxonomique de ce rang pour notre espèce avec l'option -t
omark -f omamer.out -d /Data/dbs/OMA/Viridiplantae.h5 -o omark -r family -t 4210 -of ../braker.aa --isoform_file ../braker.aa.splice
```

##### Fichiers obtenus


- omamer_Cynara_cardunculus_var_scolymus.fasta : liste des protéines ayant été retrouvées dans la base de <i>Cynara cardunculus</i>
- omamer_detailed_summary.txt : résumé détaillé des résultats
- omamer_mapped.fasta : liste des protéines retrouvées dans la lignée donnée
- omamer_misplaced.fasta : liste des protéines retrouvées dans d'autres lignées que la lignée donnée
- omamer_no_hits.fasta :  : liste des protéines non-retrouvées dans la base de données
- omamer.omq : liste des protéines uniques retrouvées
- omamer.pdf : histogramme des pourcentages de protéines selon leur catégorie
- omamer.png : histogramme des pourcentages de protéines selon leur catégorie
- omamer_selected_isoforms.txt : liste des isoformes (= transcrits) choisis pour chaque gène par omark
- omamer.sum : résumé simplifié des résultats
- omamer.tax : rappel de la lignée et clade sélectionnés
- omamer.ump : liste des protéines complètes retrouvés dans la lignée


##### Synthèse des résultats


Pour rappel, OMArk ne sélectionne qu'un seul isoforme par gène : il ne prend donc pas en compte toutes les protéines fournies par nos annotations.

Le nombre de protéines prises en compte dans nos annotations par OMArk est :

| | C. solstitialis (standard) | C. solstitialis (customisé) | C. cyanus (standard) | C. cyanus (customisé) | C. cardunculus (standard) | C. cardunculus (customisé) | C. jacea (standard) | C. jacea (customisé)
| :- | :-: | :-: | :-: | :-: | :-: | :-: | :-: | :-: |
Nombre total de protéines | 44 957 | 44 051 | 40 318 | 43 400 | 39 883 | 40 206 | 45 525 | 47 046


Le premier tableau permet de voir les gènes trouvés dans la lignée sélectionnée, et en combien de copies, similairement à BUSCO :

| | C. solstitialis (standard) | C. solstitialis (customisé) | C. cyanus (standard) | C. cyanus (customisé) | C. cardunculus (standard) | C. cardunculus (customisé) | C. jacea (standard) | C. jacea (customisé)
| :- | :-: | :-: | :-: | :-: | :-: | :-: | :-: | :-: |
Unique | 7 628 (68.12%) | 7 611 (67.97%) | 7 613 (67.99%) | 7 596 (67.83%) | 7 561 (67.52%) | 7 567 (67.57%) | 7 619 (68%) | 7 618 (68.03%)
Dupliqué | 3 073 (27.44%) | 3 105 (27.73%) | 2 851 (25.46%) | 3 090 (27.59%) | 2 824 (25.22%) | 2 882 (25.74%) | 3 102 (27.7%) | 3 083 (27.53%)
Dupliqué attendu | 1 170 (10.45%) | 1 179 (10.53%) | 1 078 (9.63%) | 1 171 (10.46%) | 1 087 (9.71%) | 1 083 (9.67%) | 1 916 (17.11%) | 1 915 (17.1%)
Dupliqué inattendu | 1 903 (16.99%) | 1 926 (17.2%) | 1 773 (15.83%) | 1 919 (17.14%) | 1 737 (15.51%) | 1 799 (16.07%) | 1 186 (10.59%) | 1 168 (10.43%)
Manquant | 497 (4.44%) | 482 (4.3%) | 734 (6.55%) | 512 (4.57%) | 813 (7.26%) | 749 (6.69%) | 477 (4.26%) | 497 (4.44%)
Total / 11 198 | 10 701 (95.56%) | 10 716 (95.7%) | 10 464 (93.45%) | 10 685 (95.43%) | 10 385 (92.74%) | 10 449 (93.31%) | 10 721 (95.74%) | 10 701 (95.56%)


Le deuxième tableau obtenu permet de voir les gènes retrouvés dans des espèces appartenant à la lignée sélectionnée (Consistent), ceux retrouvés dans des espèces non incluses dans la lignée (Inconsistent), et enfin ceux n'ayant pas été trouvé dans la base de données OMA (Inconnu) :

| | C. solstitialis (standard) | C. solstitialis (customisé) | C. cyanus (standard) | C. cyanus (customisé) | C. cardunculus (standard) | C. cardunculus (customisé) | C. jacea (standard) | C. jacea (customisé)
| :- | :-: | :-: | :-: | :-: | :-: | :-: | :-: | :-: |
Consistent | 30 298 (67.39%) | 30 271 (68.72%) | 28 290 (70.17%) | 30 027 (69.19%) | 28 107 (70.47%) | 28 167 (70.06%) | 30 297 (66.55%) | 30 381 (64.58%)
Consistent partiel | 5 697 (12.67%) | 5 632 (12.79%) | 6 167 (15.3%) | 5 581 (12.86%) | 6 562 (16.45%) | 5 966 (14.84%) | 5 730 (12.59%) | 5 824 (12.38%)
Consistent fragmenté | 2 132 (4.74%) | 2 189 (4.97%) | 1 602 (3.97%) | 2 181 (5.03%) | 1 465 (3.67%) | 1 569 (3.9%) | 1 854 (4.07%) | 1 940 (4.12%)
Inconsistent | 1 556 (3.46%) | 1 563 (3.55%) | 1 336 (3.31%) | 1 508 (3.47%) | 1 186 (2.97%) | 1 419 (3.53%) | 1 593 (3.5%) | 1 624 (3.45%)
Inconsistent partiel | 1 083 (2.41%) | 1 092 (2.48%) | 957 (2.37%) | 1 088 (2.51%) | 829 (2.08%) | 1 038 (2.58%) | 1 139 (2.5%) | 1 141 (2.43%)
Inconsistent fragmenté | 172 (0.38%) | 165 (0.37%) | 131 (0.32%) | 139 (0.32%) | 122 (0.31%) | 121 (0.3%) | 154 (0.34%) | 168 (0.36%)
Inconnu | 13 103 (29.15%) | 12 217 (27.73%) | 10 692 (26.52%) | 11 865 (27.34%) | 10 590 (26.55%) | 10 620 (26.41%) | 13 635 (29.95%) | 15 041 (31.97%)


L'espèce chez laquelle le plus de gènes ont été retrouvés est <b><i>Cynara cardunculus</i></b> pour toutes les annotations :

| | C. solstitialis (standard) | C. solstitialis (customisé) | C. cyanus (standard) | C. cyanus (customisé) | C. cardunculus (standard) | C. cardunculus (customisé) | C. jacea (standard) | C. jacea (customisé)
| :- | :-: | :-: | :-: | :-: | :-: | :-: | :-: | :-: |
Espèce détectée : <i>Cynara cardunculus</i> | 31 854 (70.85%) | 31 834 (72.27%) | 29 626 (73.48%) | 31 535 (72.66%) | 29 293 (73.45%) | 29 586 (73.59%) | 31 890 (70.05%) | 32 005 (68.03%)

Cela est certainement dû au fait qu'OMA ne contient que 11 espèces dans son clade des Astéridées, et <i>Cynara cardunculus</i> est la seule de ces espèces à être assez proche de notre Centaurée.
