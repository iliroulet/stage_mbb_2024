# Sommaire


- [RepeatModeler](#1ère-étape--repeatmodeler)

- [RepeatMasker](#2ème-étape--repeatmasker)


# Masking


Le masking consiste à masquer les séquences répétées présentes dans le génome (éléments transposables, etc.), car ces séquences peuvent facilement induire les outils de prédiction de gènes en erreur. Les outils de masking vont donc masquer ces séquences, en les remplançant par des X ou des N ou bien en les passant en lettres minuscules au lieu de majuscules, ce qui permettra aux outil d'annotation de pouvoir détecter ces séquences et les ignorer si besoin.

L'outil de masking utilisé ici est RepeatMasker (accompagné de RepeatModeler). Une autre approche avec l'outil earlGrey (https://github.com/TobyBaril/EarlGrey) a aussi été essayée, mais a ensuite été abandonnée car elle ne donnait pas de résultats significativement meilleurs.


## 1ère étape : RepeatModeler


RepeatModeler permet de créer une base de données contenant des familles de séquences répétées trouvées dans le génome.


##### Documentation
https://blaxter-lab-documentation.readthedocs.io/en/latest/repeatmodeler.html


##### Commandes

```python
# Créer une base de données
cd /Results/masking
BuildDatabase -name db_centaurea /Data/genomes/original_genome/centaurea_genome.fasta
# Remplir cette base de données (ajout de fichiers fasta et stk)
RepeatModeler -database /Results/masking/db_centaurea -threads 32 -LTRStruct
```

L'option -LTRStruct permet de masquer les LTR (<i>Long Terminal Repeat</i>). Les LTR sont des paires de séquences répétées de plusieurs centaines de pb, qui sont retrouvées chez les eukaryotes aux extrémités d'une série de gènes ou pseudogènes qui forment un transposon.


##### Fichiers obtenus


- db_centaurea-families.fa : produit par RepeatModeler, contient les familles de séquences répétées
- db_centaurea-families.stk : produit par RepeatModeler
- db_centaurea.nhr : produit par la commande BuildDatabase
- db_centaurea.nin : produit par la commande BuildDatabase
- db_centaurea.njs : produit par la commande BuildDatabase
- db_centaurea.nnd : produit par la commande BuildDatabase
- db_centaurea.nni : produit par la commande BuildDatabase
- db_centaurea.nog : produit par la commande BuildDatabase
- db_centaurea.nsq : produit par la commande BuildDatabase
- db_centaurea-rmod.log : log de RepeatModeler
- db_centaurea.translation : produit par la commande BuildDatabase


## 2ème étape : RepeatMasker


RepeatMasker peut ensuite utiliser cette librairie pour masquer le génome.


##### Documentation
https://blaxter-lab-documentation.readthedocs.io/en/latest/repeatmasker.html


##### Commandes

```python
cd /Data/genomes/masked_genome
RepeatMasker -pa 16 -lib /Results/masking/db_centaurea -cutoff 300 \ -xsmall /Data/genomes/original_genome/centaurea_genome.fasta
	-a -source -html -gff
```

Les options html et gff permettent d'obtenir un tableau sous format HTML et un fichier GFF de toutes les séquences masquées sur le génome.


##### Fichiers obtenus


- centaurea_genome.fasta.align : fichier d'alignements des séquences répétées sur le génome
- centaurea_genome.fasta.cat : fichier contenant les alignements avec des informations additionnelles
- centaurea_genome.fasta.cat.gz : version compressée du fichier précédent
- centaurea_genome.fasta.masked : génome masqué
- centaurea_genome.fasta.masked.idx : fichier d'index du génome masqué
- centaurea_genome.fasta.out : fichier contenant des informations précises pour chaque séquence masquée (scaffold, position, classe, ID...)
- centaurea_genome.fasta.out.gff : fichier GFF contenant les séquences masquées
- centaurea_genome.fasta.out.html : tableau HTML
- centaurea_genome.fasta.tbl : tableau récapitulatif du processus de masking


On peut rapidement extraire le nombre de bases masquées du fichier centaurea_genome.fasta.tbl grâce à la commande :

```python
grep "bases masked:" centaurea_genome.fasta.tbl
```

Sortie : <b>bases masked:  450301573 bp (60.06 %)</b>


À titre de comparaison, les espèces proches qui ont été annotées ont comme pourcentages de bases masquées (l'espèce <i>Solanum lycopersicum</i> a été rajoutée car ce sera l'espèce utilisée en paramètre pour l'annotation par Augustus en <i>ab initio</i>) :

| Espèce | <i>Arctium lappa</i> | <i>Centaurea solstitialis</i> | <i>Cynara cardunculus</i> | | <i>Solanum lycopersicum</i>
| :- | :-: | :-: | :-: | :-: | :-: |
Pourcentage masqué | 68.4% | 82.77% | 60.44 à 78.44% | | ~60%
